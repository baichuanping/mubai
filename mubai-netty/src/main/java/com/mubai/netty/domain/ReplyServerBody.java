package com.mubai.netty.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author baichuanping
 * @create 2018-08-30
 */
@Setter@Getter
public class ReplyServerBody extends ReplyBody {
    private String severInfo;

    public ReplyServerBody(String severInfo) {
        this.severInfo = severInfo;
    }
}
