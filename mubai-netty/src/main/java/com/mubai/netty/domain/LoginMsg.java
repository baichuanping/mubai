package com.mubai.netty.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 登陆验证类型的消息
 * @author baichuanping
 * @create 2018-08-30
 */
@Setter
@Getter
public class LoginMsg extends BaseMsg{
    private String userName;
    private String password;

    public LoginMsg() {
        super();
        setType(MsgType.LOGIN);
    }
}
