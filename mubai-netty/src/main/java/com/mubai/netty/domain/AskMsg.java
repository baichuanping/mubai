package com.mubai.netty.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * 请求类型的消息
 * @author baichuanping
 * @create 2018-08-30
 */
@Getter
@Setter
public class AskMsg extends BaseMsg{
    private AskParams params;

    public AskMsg() {
        super();
        setType(MsgType.ASK);
    }
}
