package com.mubai.netty.domain;

/**
 * @author baichuanping
 * @create 2018-08-30
 */
public class Constants {
    private static String clientId;

    public static String getClientId() {
        return clientId;
    }

    public static void setClientId(String clientId) {
        Constants.clientId = clientId;
    }
}
