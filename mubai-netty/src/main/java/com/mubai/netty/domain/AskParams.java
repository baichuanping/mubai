package com.mubai.netty.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * 请求参数
 * @author baichuanping
 * @create 2018-08-30
 */
@Setter
@Getter
public class AskParams implements Serializable{
    private static final long serialVersionUID = 7742227860420387378L;
    private String auth;
    private String content;
}
