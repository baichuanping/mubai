package com.mubai.netty.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author baichuanping
 * @create 2018-08-30
 */
@Setter@Getter
public class ReplyMsg extends BaseMsg {
    private ReplyBody body;

    public ReplyMsg() {
       super();
       setType(MsgType.REPLY);
    }
}
