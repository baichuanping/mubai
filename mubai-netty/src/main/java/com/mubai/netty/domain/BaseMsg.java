package com.mubai.netty.domain;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author baichuanping
 * @create 2018-08-30
 */
@Getter
@Setter
public class BaseMsg implements Serializable{
    private static final long serialVersionUID = -1535623973609613027L;

    private MsgType type;
    private String clientId;

    public BaseMsg() {
        this.clientId = Constants.getClientId();
    }
}
