package com.mubai.netty.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author baichuanping
 * @create 2018-08-30
 */
@Setter@Getter
public class ReplyClientBody extends ReplyBody {
    private String clientInfo;

    public ReplyClientBody(String clientInfo) {
        this.clientInfo = clientInfo;
    }
}
