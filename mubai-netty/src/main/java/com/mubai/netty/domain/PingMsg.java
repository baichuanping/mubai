package com.mubai.netty.domain;

/**
 * 心跳检测的消息类型
 * @author baichuanping
 * @create 2018-08-30
 */
public class PingMsg extends BaseMsg{
    public PingMsg() {
        super();
        setType(MsgType.PING);
    }
}
