package com.mubai.oa.service.impl;

import java.util.List;

import com.mubai.common.constant.OAConstants;
import com.mubai.common.support.Convert;
import com.mubai.oa.domain.NotifyRecord;
import com.mubai.oa.mapper.NotifyRecordMapper;
import com.mubai.oa.service.NotifyRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 通知通告发送记录 服务层实现
 *
 * @author baichuanping
 * @date 2018-12-18
 */
@Service
public class NotifyRecordServiceImpl implements NotifyRecordService
{
	@Autowired
	private NotifyRecordMapper notifyRecordMapper;

	/**
	 * 查询通知通告发送记录信息
	 *
	 * @param id 通知通告发送记录ID
	 * @return 通知通告发送记录信息
	 */
	@Override
	public NotifyRecord selectNotifyRecordById(Long id)
	{
		return notifyRecordMapper.selectNotifyRecordById(id);
	}

	/**
	 * 查询通知通告发送记录列表
	 *
	 * @param notifyRecord 通知通告发送记录信息
	 * @return 通知通告发送记录集合
	 */
	@Override
	public List<NotifyRecord> selectNotifyRecordList(NotifyRecord notifyRecord)
	{
		return notifyRecordMapper.selectNotifyRecordList(notifyRecord);
	}

	/**
	 * 新增通知通告发送记录
	 *
	 * @param notifyRecord 通知通告发送记录信息
	 * @return 结果
	 */
	@Override
	public int insertNotifyRecord(NotifyRecord notifyRecord)
	{
		return notifyRecordMapper.insertNotifyRecord(notifyRecord);
	}

	/**
	 * 修改通知通告发送记录
	 *
	 * @param notifyRecord 通知通告发送记录信息
	 * @return 结果
	 */
	@Override
	public int updateNotifyRecord(NotifyRecord notifyRecord)
	{
		return notifyRecordMapper.updateNotifyRecord(notifyRecord);
	}

	/**
	 * 删除通知通告发送记录对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteNotifyRecordByIds(String ids)
	{
		return notifyRecordMapper.deleteNotifyRecordByIds(Convert.toStrArray(ids));
	}

	/**
	 * 修改阅读状态
	 * @param userId
	 * @param notifyId
	 * @return
	 */
	@Override
	public int changeRead(Long userId,Long notifyId) {
		NotifyRecord notifyRecord = new NotifyRecord();
		notifyRecord.setNotifyId(notifyId);
		notifyRecord.setUserId(userId);
		notifyRecord.setIsRead(OAConstants.NOTIFY_READ_YES);
		return notifyRecordMapper.changeRead(notifyRecord);
	}

}