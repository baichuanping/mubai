package com.mubai.oa.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.mubai.oa.domain.DayRecord;
import com.mubai.oa.mapper.DayRecordMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.oa.mapper.DayManageMapper;
import com.mubai.oa.domain.DayManage;
import com.mubai.oa.service.DayManageService;
import com.mubai.common.support.Convert;

/**
 * 日程 服务层实现
 *
 * @author baichuanping
 * @date 2019-01-04
 */
@Service
public class DayManageServiceImpl implements DayManageService
{
	@Autowired
	private DayManageMapper dayManageMapper;
	@Autowired
	private DayRecordMapper dayRecordMapper;

	/**
	 * 查询日程信息
	 *
	 * @param dayId 日程ID
	 * @return 日程信息
	 */
	@Override
	public DayManage selectDayManageById(Long dayId)
	{
		return dayManageMapper.selectDayManageById(dayId);
	}

	/**
	 * 查询日程列表
	 *
	 * @param dayManage 日程信息
	 * @return 日程集合
	 */
	@Override
	public List<DayManage> selectDayManageList(DayManage dayManage)
	{
		return dayManageMapper.selectDayManageList(dayManage);
	}

	/**
	 * 新增日程
	 *
	 * @param dayManage 日程信息
	 * @return 结果
	 */
	@Override
	public int insertDayManage(DayManage dayManage)
	{
		int r = dayManageMapper.insertDayManage(dayManage);
		// 保存到接收表列表中
		Long dayId = dayManage.getDayId();
		List<DayRecord> dayRecords = new ArrayList<DayRecord>();
		for (Long userId: dayManage.getUserIds()) {
			DayRecord dayRecord = new DayRecord();
			dayRecord.setDayId(dayId);
			dayRecord.setUserId(userId);
			dayRecords.add(dayRecord);
		}
		dayRecordMapper.batchSave(dayRecords);
		return r;
	}

	/**
	 * 修改日程
	 *
	 * @param dayManage 日程信息
	 * @return 结果
	 */
	@Override
	public int updateDayManage(DayManage dayManage)
	{
		return dayManageMapper.updateDayManage(dayManage);
	}

	/**
	 * 删除日程对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteDayManageByIds(String ids)
	{
		return dayManageMapper.deleteDayManageByIds(Convert.toStrArray(ids));
	}

	/**
	 * 获取日程信息根据用户Id
	 *
	 * @param userId
	 * @return
	 */
	@Override
	public List<DayManage> selectDayByUserId(Long userId) {
		return dayManageMapper.selectDayListByUserId(userId);
	}

}