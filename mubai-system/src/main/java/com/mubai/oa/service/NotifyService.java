package com.mubai.oa.service;

import com.mubai.oa.domain.Notify;
import com.mubai.oa.domain.NotifyDto;
import com.mubai.system.domain.SysUser;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * 通知通告 服务层
 * 
 * @author baichuanping
 * @date 2018-12-18
 */
public interface NotifyService
{
	/**
     * 查询通知通告信息
     * 
     * @param id 通知通告ID
     * @return 通知通告信息
     */
	public Notify selectNotifyById(Long id);
	
	/**
     * 查询通知通告列表
     * 
     * @param notify 通知通告信息
     * @return 通知通告集合
     */
	public List<Notify> selectNotifyList(Notify notify);
	
	/**
     * 新增通知通告
     * 
     * @param notify 通知通告信息
     * @return 结果
     */
	public int insertNotify(Notify notify);

	/**
     * 修改通知通告
     * 
     * @param notify 通知通告信息
     * @return 结果
     */
	public int updateNotify(Notify notify);
		
	/**
     * 删除通知通告信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNotifyByIds(String ids);

	/**
	 * 获取自己的消息
	 * @param map
	 * @return
	 */
	public List<NotifyDto> selfList(Map<String, Object> map);
}
