package com.mubai.oa.service;

import com.mubai.oa.domain.DayManage;
import java.util.List;

/**
 * 日程 服务层
 * 
 * @author baichuanping
 * @date 2019-01-04
 */
public interface DayManageService
{
	/**
     * 查询日程信息
     * 
     * @param dayId 日程ID
     * @return 日程信息
     */
	public DayManage selectDayManageById(Long dayId);
	
	/**
     * 查询日程列表
     * 
     * @param dayManage 日程信息
     * @return 日程集合
     */
	public List<DayManage> selectDayManageList(DayManage dayManage);
	
	/**
     * 新增日程
     * 
     * @param dayManage 日程信息
     * @return 结果
     */
	public int insertDayManage(DayManage dayManage);
	
	/**
     * 修改日程
     * 
     * @param dayManage 日程信息
     * @return 结果
     */
	public int updateDayManage(DayManage dayManage);
		
	/**
     * 删除日程信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteDayManageByIds(String ids);

	/**
	 * 获取日程信息根据用户Id
	 * @param userId
	 * @return
	 */
	public List<DayManage> selectDayByUserId(Long userId);

}
