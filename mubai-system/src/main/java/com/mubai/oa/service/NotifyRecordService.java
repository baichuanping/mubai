package com.mubai.oa.service;

import com.mubai.oa.domain.NotifyRecord;

import java.util.List;

/**
 * 通知通告发送记录 服务层
 *
 * @author baichuanping
 * @date 2018-12-18
 */
public interface NotifyRecordService
{
	/**
	 * 查询通知通告发送记录信息
	 *
	 * @param id 通知通告发送记录ID
	 * @return 通知通告发送记录信息
	 */
	public NotifyRecord selectNotifyRecordById(Long id);

	/**
	 * 查询通知通告发送记录列表
	 *
	 * @param notifyRecord 通知通告发送记录信息
	 * @return 通知通告发送记录集合
	 */
	public List<NotifyRecord> selectNotifyRecordList(NotifyRecord notifyRecord);

	/**
	 * 新增通知通告发送记录
	 *
	 * @param notifyRecord 通知通告发送记录信息
	 * @return 结果
	 */
	public int insertNotifyRecord(NotifyRecord notifyRecord);

	/**
	 * 修改通知通告发送记录
	 *
	 * @param notifyRecord 通知通告发送记录信息
	 * @return 结果
	 */
	public int updateNotifyRecord(NotifyRecord notifyRecord);

	/**
	 * 删除通知通告发送记录信息
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	public int deleteNotifyRecordByIds(String ids);

	/**
	 * 修改阅读状态
	 * @param userId
	 * @param notifyId
	 * @return
	 */
	public int changeRead(Long userId,Long notifyId);
}
