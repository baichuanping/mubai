package com.mubai.oa.mapper;

import com.mubai.oa.domain.Notify;
import com.mubai.oa.domain.NotifyDto;

import java.util.List;
import java.util.Map;

/**
 * 通知通告 数据层
 * 
 * @author baichuanping
 * @date 2018-12-18
 */
public interface NotifyMapper 
{
	/**
     * 查询通知通告信息
     * 
     * @param id 通知通告ID
     * @return 通知通告信息
     */
	public Notify selectNotifyById(Long id);
	
	/**
     * 查询通知通告列表
     * 
     * @param notify 通知通告信息
     * @return 通知通告集合
     */
	public List<Notify> selectNotifyList(Notify notify);
	
	/**
     * 新增通知通告
     * 
     * @param notify 通知通告信息
     * @return 结果
     */
	public int insertNotify(Notify notify);
	
	/**
     * 修改通知通告
     * 
     * @param notify 通知通告信息
     * @return 结果
     */
	public int updateNotify(Notify notify);
	
	/**
     * 删除通知通告
     * 
     * @param id 通知通告ID
     * @return 结果
     */
	public int deleteNotifyById(Long id);
	
	/**
     * 批量删除通知通告
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNotifyByIds(String[] ids);

	/**
	 * 获取我的消息
	 * @param map
	 * @return
	 */
	List<NotifyDto> selectNotifyDtoList(Map<String, Object> map);
	
}