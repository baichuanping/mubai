package com.mubai.oa.mapper;

import com.mubai.oa.domain.DayManage;
import java.util.List;	

/**
 * 日程 数据层
 * 
 * @author baichuanping
 * @date 2019-01-04
 */
public interface DayManageMapper 
{
	/**
     * 查询日程信息
     * 
     * @param dayId 日程ID
     * @return 日程信息
     */
	public DayManage selectDayManageById(Long dayId);
	
	/**
     * 查询日程列表
     * 
     * @param dayManage 日程信息
     * @return 日程集合
     */
	public List<DayManage> selectDayManageList(DayManage dayManage);
	
	/**
     * 新增日程
     * 
     * @param dayManage 日程信息
     * @return 结果
     */
	public int insertDayManage(DayManage dayManage);
	
	/**
     * 修改日程
     * 
     * @param dayManage 日程信息
     * @return 结果
     */
	public int updateDayManage(DayManage dayManage);
	
	/**
     * 删除日程
     * 
     * @param dayId 日程ID
     * @return 结果
     */
	public int deleteDayManageById(Long dayId);
	
	/**
     * 批量删除日程
     * 
     * @param dayIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteDayManageByIds(String[] dayIds);

	/**
	 * 查询我的日程信息
	 * @param userId
	 * @return
	 */
	public List<DayManage> selectDayListByUserId(Long userId);
}