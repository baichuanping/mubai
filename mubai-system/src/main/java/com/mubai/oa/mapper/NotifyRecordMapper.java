package com.mubai.oa.mapper;

import com.mubai.oa.domain.NotifyRecord;

import java.util.List;

/**
 * 通知通告发送记录 数据层
 * 
 * @author baichuanping
 * @date 2018-12-18
 */
public interface NotifyRecordMapper 
{
	/**
     * 查询通知通告发送记录信息
     * 
     * @param id 通知通告发送记录ID
     * @return 通知通告发送记录信息
     */
	public NotifyRecord selectNotifyRecordById(Long id);
	
	/**
     * 查询通知通告发送记录列表
     * 
     * @param notifyRecord 通知通告发送记录信息
     * @return 通知通告发送记录集合
     */
	public List<NotifyRecord> selectNotifyRecordList(NotifyRecord notifyRecord);
	
	/**
     * 新增通知通告发送记录
     * 
     * @param notifyRecord 通知通告发送记录信息
     * @return 结果
     */
	public int insertNotifyRecord(NotifyRecord notifyRecord);
	
	/**
     * 修改通知通告发送记录
     * 
     * @param notifyRecord 通知通告发送记录信息
     * @return 结果
     */
	public int updateNotifyRecord(NotifyRecord notifyRecord);
	
	/**
     * 删除通知通告发送记录
     * 
     * @param id 通知通告发送记录ID
     * @return 结果
     */
	public int deleteNotifyRecordById(Long id);
	
	/**
     * 批量删除通知通告发送记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteNotifyRecordByIds(String[] ids);

	/**
	 * 修改阅读状态
	 * @param notifyRecord
	 * @return
	 */
	public int changeRead(NotifyRecord notifyRecord);

	/**
	 * 批量新增通知记录
	 * @param notifyRecords
	 * @return
	 */
	public int batchSave(List<NotifyRecord> notifyRecords);

	/**
	 * 根据通知id删除通知记录信息
	 * @param notifyIds
	 * @return
	 */
	public int batchRemoveByNotifbyId(String[] notifyIds);
}