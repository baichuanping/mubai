package com.mubai.oa.mapper;

import com.mubai.oa.domain.DayRecord;
import java.util.List;	

/**
 * 日程相关人员记录 数据层
 * 
 * @author baichuanping
 * @date 2019-01-04
 */
public interface DayRecordMapper 
{
	/**
     * 查询日程相关人员记录信息
     * 
     * @param dayId 日程相关人员记录ID
     * @return 日程相关人员记录信息
     */
	public DayRecord selectDayRecordById(Long dayId);
	
	/**
     * 查询日程相关人员记录列表
     * 
     * @param dayRecord 日程相关人员记录信息
     * @return 日程相关人员记录集合
     */
	public List<DayRecord> selectDayRecordList(DayRecord dayRecord);
	
	/**
     * 新增日程相关人员记录
     * 
     * @param dayRecord 日程相关人员记录信息
     * @return 结果
     */
	public int insertDayRecord(DayRecord dayRecord);
	
	/**
     * 修改日程相关人员记录
     * 
     * @param dayRecord 日程相关人员记录信息
     * @return 结果
     */
	public int updateDayRecord(DayRecord dayRecord);
	
	/**
     * 删除日程相关人员记录
     * 
     * @param dayId 日程相关人员记录ID
     * @return 结果
     */
	public int deleteDayRecordById(Long dayId);
	
	/**
     * 批量删除日程相关人员记录
     * 
     * @param dayIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteDayRecordByIds(String[] dayIds);

	/**
	 * 批量新增日程记录
	 * @param dayRecords
	 * @return
	 */
	public int batchSave(List<DayRecord> dayRecords);

	/**
	 * 根据日程Id删除日程记录信息
	 * @param dayIds
	 * @return
	 */
	public int batchRemoveByDaybyId(String[] dayIds);
}