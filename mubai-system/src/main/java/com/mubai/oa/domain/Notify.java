package com.mubai.oa.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;

import java.util.Arrays;
import java.util.Date;

/**
 * 通知通告表 t_notify
 * 
 * @author baichuanping
 * @date 2018-12-18
 */
public class Notify extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 编号 */
	private Long id;
	/** 类型 */
	private String type;
	/** 标题 */
	private String title;
	/** 内容 */
	private String content;
	/** 附件 */
	private String files;
	/** 公告状态（0正常 1关闭） */
	private String status;

	/**
	 * 接收人Id
	 */
	private Long[] userIds;
	/**
	 * 接收人名字
	 */
	private String[] userNames;

	/**
	 * 登录名称
	 */
	private String[] loginNames;

	public void setId(Long id) 
	{
		this.id = id;
	}

	public Long getId() 
	{
		return id;
	}
	public void setType(String type) 
	{
		this.type = type;
	}

	public String getType() 
	{
		return type;
	}
	public void setTitle(String title) 
	{
		this.title = title;
	}

	public String getTitle() 
	{
		return title;
	}
	public void setContent(String content) 
	{
		this.content = content;
	}

	public String getContent() 
	{
		return content;
	}
	public void setFiles(String files) 
	{
		this.files = files;
	}

	public String getFiles() 
	{
		return files;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}

	public Long[] getUserIds() {
		return userIds;
	}

	public void setUserIds(Long[] userIds) {
		this.userIds = userIds;
	}

	public String[] getUserNames() {
		return userNames;
	}

	public void setUserNames(String[] userNames) {
		this.userNames = userNames;
	}

	public String[] getLoginNames() {
		return loginNames;
	}

	public void setLoginNames(String[] loginNames) {
		this.loginNames = loginNames;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("type", getType())
            .append("title", getTitle())
            .append("content", getContent())
            .append("files", getFiles())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
