package com.mubai.oa.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;

import java.io.Serializable;

/**
 * 日程相关人员记录表 oa_day_record
 * 
 * @author baichuanping
 * @date 2019-01-04
 */
public class DayRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/** 日程ID */
	private Long dayId;
	/** 用户Id */
	private Long userId;

	public void setDayId(Long dayId) 
	{
		this.dayId = dayId;
	}

	public Long getDayId() 
	{
		return dayId;
	}
	public void setUserId(Long userId) 
	{
		this.userId = userId;
	}

	public Long getUserId() 
	{
		return userId;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dayId", getDayId())
            .append("userId", getUserId())
            .toString();
    }
}
