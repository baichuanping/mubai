package com.mubai.oa.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * 通知通告发送记录表 t_notify_record
 * 
 * @author baichuanping
 * @date 2018-12-18
 */
public class NotifyRecord implements Serializable {
	private static final long serialVersionUID = 1L;
	
	/** 编号 */
	private Long id;
	/** 通知通告ID */
	private Long notifyId;
	/** 接受人 */
	private Long userId;
	/** 阅读标记 */
	private Integer isRead;
	/** 阅读时间 */
	private Date readDate;

	public void setId(Long id) 
	{
		this.id = id;
	}

	public Long getId() 
	{
		return id;
	}
	public void setNotifyId(Long notifyId) 
	{
		this.notifyId = notifyId;
	}

	public Long getNotifyId() 
	{
		return notifyId;
	}
	public void setUserId(Long userId) 
	{
		this.userId = userId;
	}

	public Long getUserId() 
	{
		return userId;
	}
	public void setIsRead(Integer isRead) 
	{
		this.isRead = isRead;
	}

	public Integer getIsRead() 
	{
		return isRead;
	}
	public void setReadDate(Date readDate) 
	{
		this.readDate = readDate;
	}

	public Date getReadDate() 
	{
		return readDate;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("notifyId", getNotifyId())
            .append("userId", getUserId())
            .append("isRead", getIsRead())
            .append("readDate", getReadDate())
            .toString();
    }
}
