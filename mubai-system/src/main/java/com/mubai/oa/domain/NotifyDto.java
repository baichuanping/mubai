package com.mubai.oa.domain;

/**
 * @author baichuanping
 * @create 2018-12-18
 */
public class NotifyDto extends Notify{
	public static final long serialVersionUID = 1L;
	/**
	 * 阅读标志
	 */
	private String isRead;

	private String before;

	private String sender;

	public String getIsRead() {
		return isRead;
	}

	public void setIsRead(String isRead) {
		this.isRead = isRead;
	}

	public String getBefore() {
		return before;
	}

	public void setBefore(String before) {
		this.before = before;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}
}
