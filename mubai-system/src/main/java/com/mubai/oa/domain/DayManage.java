package com.mubai.oa.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;
import java.util.Date;

/**
 * 日程表 oa_day_manage
 * 
 * @author baichuanping
 * @date 2019-01-04
 */
public class DayManage extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 日程ID */
	private Long dayId;
	/** 日程标题 */
	private String dayTitle;
	/** 开始时间 */
	private Date startTime;
	/** 结束时间 */
	private Date endTime;
	/** 日程类型（1日程安排 2假日提醒） */
	private String dayType;
	/** 日程状态（0一般 1重要 2紧急） */
	private String status;
	/** 是否提醒(0否1是) */
	private String remind;
	/**
	 * 接收人Id
	 */
	private Long[] userIds;
	/**
	 * 接收人名字
	 */
	private String[] userNames;

	/**
	 * 登录名称
	 */
	private String[] loginNames;
	public void setDayId(Long dayId) 
	{
		this.dayId = dayId;
	}

	public Long getDayId() 
	{
		return dayId;
	}
	public void setDayTitle(String dayTitle) 
	{
		this.dayTitle = dayTitle;
	}

	public String getDayTitle() 
	{
		return dayTitle;
	}
	public void setStartTime(Date startTime) 
	{
		this.startTime = startTime;
	}

	public Date getStartTime() 
	{
		return startTime;
	}
	public void setEndTime(Date endTime) 
	{
		this.endTime = endTime;
	}

	public Date getEndTime() 
	{
		return endTime;
	}
	public void setDayType(String dayType) 
	{
		this.dayType = dayType;
	}

	public String getDayType() 
	{
		return dayType;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}
	public void setRemind(String remind) 
	{
		this.remind = remind;
	}

	public String getRemind() 
	{
		return remind;
	}

	public Long[] getUserIds() {
		return userIds;
	}

	public void setUserIds(Long[] userIds) {
		this.userIds = userIds;
	}

	public String[] getUserNames() {
		return userNames;
	}

	public void setUserNames(String[] userNames) {
		this.userNames = userNames;
	}

	public String[] getLoginNames() {
		return loginNames;
	}

	public void setLoginNames(String[] loginNames) {
		this.loginNames = loginNames;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dayId", getDayId())
            .append("dayTitle", getDayTitle())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("dayType", getDayType())
            .append("status", getStatus())
            .append("remind", getRemind())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
