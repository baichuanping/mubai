package com.mubai.files.service;

import com.mubai.files.domain.Files;
import java.util.List;

/**
 * 文件上传 服务层
 * 
 * @author baichuanping
 * @date 2019-01-19
 */
public interface FilesService
{
	/**
     * 查询文件上传信息
     * 
     * @param fileId 文件上传ID
     * @return 文件上传信息
     */
	public Files selectFilesById(Long fileId);
	
	/**
     * 查询文件上传列表
     * 
     * @param files 文件上传信息
     * @return 文件上传集合
     */
	public List<Files> selectFilesList(Files files);
	
	/**
     * 新增文件上传
     * 
     * @param files 文件上传信息
     * @return 结果
     */
	public int insertFiles(Files files);

	/**
	 * 修改文件上传
	 *
	 * @param files 文件上传信息
	 * @return 结果
	 */
	public int updateFiles(Files files,Boolean isFile);
		
	/**
     * 删除文件上传信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteFilesByIds(String ids);

	/**
	 * 检验文件名是否唯一
	 * @param files
	 * @return
	 */
	public String checkFileNameUnqiue(Files files);

	/**
	 * 查询文件上传列表(不带显示的)
	 * @param files
	 * @return
	 */
	public List<Files> selectFilesListNoSave(Files files);
	
}
