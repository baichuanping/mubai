package com.mubai.files.service;

import com.mubai.files.domain.FilePlay;
import java.util.List;

/**
 * 文件播放顺序 服务层
 * 
 * @author baichuanping
 * @date 2019-01-19
 */
public interface FilePlayService
{
	/**
     * 查询文件播放顺序信息
     * 
     * @param playId 文件播放顺序ID
     * @return 文件播放顺序信息
     */
	public FilePlay selectFilePlayById(Long playId);
	
	/**
     * 查询文件播放顺序列表
     * 
     * @param filePlay 文件播放顺序信息
     * @return 文件播放顺序集合
     */
	public List<FilePlay> selectFilePlayList(FilePlay filePlay);
	
	/**
     * 新增文件播放顺序
     * 
     * @param filePlay 文件播放顺序信息
     * @return 结果
     */
	public int insertFilePlay(FilePlay filePlay);
	
	/**
     * 修改文件播放顺序
     * 
     * @param filePlay 文件播放顺序信息
     * @return 结果
     */
	public int updateFilePlay(FilePlay filePlay);
		
	/**
     * 删除文件播放顺序信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteFilePlayByIds(String ids);

	/**
	 * 更新播放顺序
	 * @param channelCode
	 * @param mark
	 * @param orderNum
	 * @return
	 */
	public int updatePayLimit(Long channelCode, Integer mark, Integer orderNum);

	/**
	 * 根据通道获取播放文件
	 * @param channelCode
	 * @return
	 */
	public FilePlay selectMaxLimit(Long channelCode);


}
