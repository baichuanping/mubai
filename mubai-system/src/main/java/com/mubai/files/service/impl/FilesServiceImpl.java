package com.mubai.files.service.impl;

import java.io.File;
import java.util.List;

import com.mubai.common.constant.UserConstants;
import com.mubai.common.utils.file.FileUploadUtils;
import com.mubai.common.utils.file.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.files.mapper.FilesMapper;
import com.mubai.files.domain.Files;
import com.mubai.files.service.FilesService;
import com.mubai.common.support.Convert;

/**
 * 文件上传 服务层实现
 *
 * @author baichuanping
 * @date 2019-01-19
 */
@Service
public class FilesServiceImpl implements FilesService
{
	@Autowired
	private FilesMapper filesMapper;

	/**
	 * 查询文件上传信息
	 *
	 * @param fileId 文件上传ID
	 * @return 文件上传信息
	 */
	@Override
	public Files selectFilesById(Long fileId)
	{
		return filesMapper.selectFilesById(fileId);
	}

	/**
	 * 查询文件上传列表
	 *
	 * @param files 文件上传信息
	 * @return 文件上传集合
	 */
	@Override
	public List<Files> selectFilesList(Files files)
	{
		return filesMapper.selectFilesList(files);
	}

	/**
	 * 新增文件上传
	 *
	 * @param files 文件上传信息
	 * @return 结果
	 */
	@Override
	public int insertFiles(Files files)
	{
		return filesMapper.insertFiles(files);
	}

	/**
	 * 修改文件上传
	 *
	 * @param files  文件上传信息
	 * @param isFile
	 * @return 结果
	 */
	@Override
	public int updateFiles(Files files, Boolean isFile) {
		int result = 0;
		//修改新数据
		result = filesMapper.updateFiles(files);
		if (result > 0) {
			//先判断有没有文件
			Files oldFiles = filesMapper.selectFilesById(files.getFileId());
			File file = new File(oldFiles.getUrl());
			if (file.isFile()) {
				if (files != null && isFile) {
					file.delete();
				} else {
					String url = FileUploadUtils.getDefaultBaseDir() + files.getFileName() + "." + oldFiles.getSuffix();
					file.renameTo(new File(url));
				}
			}
		}
		return result;
	}


	/**
	 * 删除文件上传对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteFilesByIds(String ids)
	{
		int result = 0;
		if (ids.length() > 0) {
			for (Long id:Convert.toLongArray(ids)) {
				Files files = filesMapper.selectFilesById(id);
				if (files != null) {
					result = filesMapper.deleteFilesById(id);
					//删除成功后要移除文件
					if (result > 0) {
						FileUtils.deleteFile(files.getUrl());
					}
				}
			}
		}
		return result;
	}

	/**
	 * 检验文件名是否唯一
	 *
	 * @param files
	 * @return
	 */
	@Override
	public String checkFileNameUnqiue(Files files) {
		List<Files> filesList = filesMapper.selectFilesList(files);
		if (filesList != null && filesList.size() > 0) {
			return UserConstants.DEPT_NAME_NOT_UNIQUE;
		}
		return UserConstants.DEPT_NAME_UNIQUE;
	}

	/**
	 * 查询文件上传列表(不带显示的)
	 *
	 * @param files
	 * @return
	 */
	@Override
	public List<Files> selectFilesListNoSave(Files files) {
		return filesMapper.selectFilesListNoSave(files);
	}

}