package com.mubai.files.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.files.mapper.FilePlayMapper;
import com.mubai.files.domain.FilePlay;
import com.mubai.files.service.FilePlayService;
import com.mubai.common.support.Convert;

/**
 * 文件播放顺序 服务层实现
 *
 * @author baichuanping
 * @date 2019-01-19
 */
@Service
public class FilePlayServiceImpl implements FilePlayService
{
	@Autowired
	private FilePlayMapper filePlayMapper;

	/**
	 * 查询文件播放顺序信息
	 *
	 * @param playId 文件播放顺序ID
	 * @return 文件播放顺序信息
	 */
	@Override
	public FilePlay selectFilePlayById(Long playId)
	{
		return filePlayMapper.selectFilePlayById(playId);
	}

	/**
	 * 查询文件播放顺序列表
	 *
	 * @param filePlay 文件播放顺序信息
	 * @return 文件播放顺序集合
	 */
	@Override
	public List<FilePlay> selectFilePlayList(FilePlay filePlay)
	{
		return filePlayMapper.selectFilePlayList(filePlay);
	}

	/**
	 * 新增文件播放顺序
	 *
	 * @param filePlay 文件播放顺序信息
	 * @return 结果
	 */
	@Override
	public int insertFilePlay(FilePlay filePlay)
	{
		return filePlayMapper.insertFilePlay(filePlay);
	}

	/**
	 * 修改文件播放顺序
	 *
	 * @param filePlay 文件播放顺序信息
	 * @return 结果
	 */
	@Override
	public int updateFilePlay(FilePlay filePlay)
	{
		return filePlayMapper.updateFilePlay(filePlay);
	}

	/**
	 * 删除文件播放顺序对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteFilePlayByIds(String ids)
	{
		return filePlayMapper.deleteFilePlayByIds(Convert.toStrArray(ids));
	}

	/**
	 * 更新播放顺序
	 *
	 * @param channelCode
	 * @param mark
	 * @param orderNum
	 * @return
	 */
	@Override
	public int updatePayLimit(Long channelCode, Integer mark, Integer orderNum) {
		return filePlayMapper.updatePayLimit(channelCode,mark,orderNum);
	}

	/**
	 * 根据通道获取播放文件
	 *
	 * @param channelCode
	 * @return
	 */
	@Override
	public FilePlay selectMaxLimit(Long channelCode) {
		return filePlayMapper.selectMaxLimit(channelCode);
	}

}