package com.mubai.files.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;

/**
 * 文件通道表 sys_file_channel
 * 
 * @author baichuanping
 * @date 2019-01-19
 */
public class FileChannel extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 文件通道ID */
	private Long channelId;
	/** 通道名称 */
	private String channelName;
	/** 通道编码 */
	private String channelCode;
	/** 显示内容 */
	private String showFile;

	public void setChannelId(Long channelId)
	{
		this.channelId = channelId;
	}

	public Long getChannelId()
	{
		return channelId;
	}
	public void setChannelName(String channelName) 
	{
		this.channelName = channelName;
	}

	public String getChannelName() 
	{
		return channelName;
	}
	public void setChannelCode(String channelCode) 
	{
		this.channelCode = channelCode;
	}

	public String getChannelCode() 
	{
		return channelCode;
	}
	public void setShowFile(String showFile) 
	{
		this.showFile = showFile;
	}

	public String getShowFile() 
	{
		return showFile;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("channelId", getChannelId())
            .append("channelName", getChannelName())
            .append("channelCode", getChannelCode())
            .append("showFile", getShowFile())
            .toString();
    }
}
