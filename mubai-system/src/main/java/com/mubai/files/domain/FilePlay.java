package com.mubai.files.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;

/**
 * 文件播放顺序表 sys_file_play
 * 
 * @author baichuanping
 * @date 2019-01-19
 */
public class FilePlay extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 文件播放ID */
	private Long playId;
	/** 通道编码 */
	private Long channelCode;
	/**  */
	private Long fileId;
	private String fileName;
	/** 0 图片 1 视频 2 文字 */
	private String types;
	/** 后缀 */
	private String suffix;
	/**  */
	private String url;
	/**  */
	private String content;
	/** 播放顺序 */
	private Integer orderNum;

	public Long getPlayId() {
		return playId;
	}

	public void setPlayId(Long playId) {
		this.playId = playId;
	}

	public Long getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(Long channelCode) {
		this.channelCode = channelCode;
	}

	public Long getFileId() {
		return fileId;
	}

	public void setFileId(Long fileId) {
		this.fileId = fileId;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public String getFileName() 
	{
		return fileName;
	}
	public void setTypes(String types) 
	{
		this.types = types;
	}

	public String getTypes() 
	{
		return types;
	}
	public void setSuffix(String suffix) 
	{
		this.suffix = suffix;
	}

	public String getSuffix() 
	{
		return suffix;
	}
	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getUrl() 
	{
		return url;
	}
	public void setContent(String content) 
	{
		this.content = content;
	}

	public String getContent() 
	{
		return content;
	}
	public void setOrderNum(Integer orderNum) 
	{
		this.orderNum = orderNum;
	}

	public Integer getOrderNum() 
	{
		return orderNum;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("playId", getPlayId())
            .append("channelCode", getChannelCode())
            .append("fileName", getFileName())
            .append("types", getTypes())
            .append("suffix", getSuffix())
            .append("url", getUrl())
            .append("content", getContent())
            .append("orderNum", getOrderNum())
            .append("remark", getRemark())
            .toString();
    }
}
