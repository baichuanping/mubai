package com.mubai.files.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;
import java.util.Date;

/**
 * 文件上传表 sys_files
 * 
 * @author baichuanping
 * @date 2019-01-19
 */
public class Files extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 文件存储ID */
	private Long fileId;
	/**  */
	private String fileName;
	/** url路径 */
	private String url;
	/** 文字内容 */
	private String content;
	/** 后缀 */
	private String suffix;
	/** 类型（0代表图片 1代表视频） */
	private String type;
	/** 删除标志（0代表存在 2代表删除） */
	private String delFlag;
	/** 创建人姓名 */
	private String createByName;
	/** 创建者 */
	private String createBy;
	/** 创建时间 */
	private Date createTime;
	/** 修改人名称 */
	private String updateByName;
	/** 更新者 */
	private String updateBy;
	/** 更新时间 */
	private Date updateTime;
	/** 备注 */
	private String remark;
	/**修改标识 0 新增 1 修改**/
	private int updateFlag;
	/**通道编码**/
	private Long channelCode;
	public void setFileId(Long fileId)
	{
		this.fileId = fileId;
	}

	public Long getFileId()
	{
		return fileId;
	}
	public void setFileName(String fileName) 
	{
		this.fileName = fileName;
	}

	public String getFileName() 
	{
		return fileName;
	}
	public void setUrl(String url) 
	{
		this.url = url;
	}

	public String getUrl() 
	{
		return url;
	}
	public void setContent(String content)
	{
		this.content = content;
	}

	public String getContent() 
	{
		return content;
	}
	public void setSuffix(String suffix) 
	{
		this.suffix = suffix;
	}

	public String getSuffix() 
	{
		return suffix;
	}
	public void setType(String type) 
	{
		this.type = type;
	}

	public String getType() 
	{
		return type;
	}
	public void setDelFlag(String delFlag) 
	{
		this.delFlag = delFlag;
	}

	public String getDelFlag() 
	{
		return delFlag;
	}
	public void setCreateByName(String createByName) 
	{
		this.createByName = createByName;
	}

	public String getCreateByName() 
	{
		return createByName;
	}
	public void setCreateBy(String createBy) 
	{
		this.createBy = createBy;
	}

	public String getCreateBy() 
	{
		return createBy;
	}
	public void setCreateTime(Date createTime) 
	{
		this.createTime = createTime;
	}

	public Date getCreateTime() 
	{
		return createTime;
	}
	public void setUpdateByName(String updateByName) 
	{
		this.updateByName = updateByName;
	}

	public String getUpdateByName() 
	{
		return updateByName;
	}
	public void setUpdateBy(String updateBy) 
	{
		this.updateBy = updateBy;
	}

	public String getUpdateBy() 
	{
		return updateBy;
	}
	public void setUpdateTime(Date updateTime) 
	{
		this.updateTime = updateTime;
	}

	public Date getUpdateTime() 
	{
		return updateTime;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}

	public int getUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(int updateFlag) {
		this.updateFlag = updateFlag;
	}

	public Long getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(Long channelCode) {
		this.channelCode = channelCode;
	}

	public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("fileId", getFileId())
            .append("fileName", getFileName())
            .append("url", getUrl())
            .append("content", getContent())
            .append("suffix", getSuffix())
            .append("type", getType())
            .append("delFlag", getDelFlag())
            .append("createByName", getCreateByName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateByName", getUpdateByName())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
