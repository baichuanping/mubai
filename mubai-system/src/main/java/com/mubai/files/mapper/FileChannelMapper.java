package com.mubai.files.mapper;

import com.mubai.files.domain.FileChannel;
import java.util.List;	

/**
 * 文件通道 数据层
 * 
 * @author baichuanping
 * @date 2019-01-19
 */
public interface FileChannelMapper 
{
	/**
     * 查询文件通道信息
     * 
     * @param channelId 文件通道ID
     * @return 文件通道信息
     */
	public FileChannel selectFileChannelById(Long channelId);
	
	/**
     * 查询文件通道列表
     * 
     * @param fileChannel 文件通道信息
     * @return 文件通道集合
     */
	public List<FileChannel> selectFileChannelList(FileChannel fileChannel);
	
	/**
     * 新增文件通道
     * 
     * @param fileChannel 文件通道信息
     * @return 结果
     */
	public int insertFileChannel(FileChannel fileChannel);
	
	/**
     * 修改文件通道
     * 
     * @param fileChannel 文件通道信息
     * @return 结果
     */
	public int updateFileChannel(FileChannel fileChannel);
	
	/**
     * 删除文件通道
     * 
     * @param channelId 文件通道ID
     * @return 结果
     */
	public int deleteFileChannelById(Integer channelId);
	
	/**
     * 批量删除文件通道
     * 
     * @param channelIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteFileChannelByIds(String[] channelIds);
	
}