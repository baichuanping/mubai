package com.mubai.files.mapper;

import com.mubai.files.domain.Files;
import java.util.List;	

/**
 * 文件上传 数据层
 * 
 * @author baichuanping
 * @date 2019-01-19
 */
public interface FilesMapper 
{
	/**
     * s
     * 
     * @param fileId 文件上传ID
     * @return 文件上传信息
     */
	public Files selectFilesById(Long fileId);
	
	/**
     * 查询文件上传列表
     * 
     * @param files 文件上传信息
     * @return 文件上传集合
     */
	public List<Files> selectFilesList(Files files);
	
	/**
     * 新增文件上传
     * 
     * @param files 文件上传信息
     * @return 结果
     */
	public int insertFiles(Files files);
	
	/**
     * 修改文件上传
     * 
     * @param files 文件上传信息
     * @return 结果
     */
	public int updateFiles(Files files);
	
	/**
     * 删除文件上传
     * 
     * @param fileId 文件上传ID
     * @return 结果
     */
	public int deleteFilesById(Long fileId);
	
	/**
     * 批量删除文件上传
     * 
     * @param fileIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteFilesByIds(String[] fileIds);

	/**
	 * 查询文件上传列表(不带显示的)
	 * @param files
	 * @return
	 */
	public List<Files> selectFilesListNoSave(Files files);
}