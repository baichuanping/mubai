package com.mubai.capital.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;
import java.util.Date;

/**
 * 读写器表 ca_reader
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public class Reader extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 读写器ID */
	private String readerId;
	/** 读写器NO */
	private String readerNo;
	/** 读写器类型,4通道，8通道 */
	private String type;
	/** 读写器IP */
	private String ip;
	/** 端口 */
	private Integer port;
	/** 发射功率 */
	private String power;
	/** 读写器天线 */
	private String wire;
	/** 连接状态 0断开，1连接 */
	private String status;
	/** 创建者 */
	private String createBy;
	/** 创建时间 */
	private Date createTime;
	/** 更新者 */
	private String updateBy;
	/** 更新时间 */
	private Date updateTime;
	/** 备注 */
	private String remark;

	public void setReaderId(String readerId) 
	{
		this.readerId = readerId;
	}

	public String getReaderId() 
	{
		return readerId;
	}
	public void setReaderNo(String readerNo) 
	{
		this.readerNo = readerNo;
	}

	public String getReaderNo() 
	{
		return readerNo;
	}
	public void setType(String type) 
	{
		this.type = type;
	}

	public String getType() 
	{
		return type;
	}
	public void setIp(String ip) 
	{
		this.ip = ip;
	}

	public String getIp() 
	{
		return ip;
	}
	public void setPort(Integer port) 
	{
		this.port = port;
	}

	public Integer getPort() 
	{
		return port;
	}
	public void setPower(String power) 
	{
		this.power = power;
	}

	public String getPower() 
	{
		return power;
	}
	public void setWire(String wire) 
	{
		this.wire = wire;
	}

	public String getWire() 
	{
		return wire;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}
	public void setCreateBy(String createBy) 
	{
		this.createBy = createBy;
	}

	public String getCreateBy() 
	{
		return createBy;
	}
	public void setCreateTime(Date createTime) 
	{
		this.createTime = createTime;
	}

	public Date getCreateTime() 
	{
		return createTime;
	}
	public void setUpdateBy(String updateBy) 
	{
		this.updateBy = updateBy;
	}

	public String getUpdateBy() 
	{
		return updateBy;
	}
	public void setUpdateTime(Date updateTime) 
	{
		this.updateTime = updateTime;
	}

	public Date getUpdateTime() 
	{
		return updateTime;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("readerId", getReaderId())
            .append("readerNo", getReaderNo())
            .append("type", getType())
            .append("ip", getIp())
            .append("port", getPort())
            .append("power", getPower())
            .append("wire", getWire())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
