package com.mubai.capital.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;
import java.util.Date;

/**
 * 供应商表 ca_supplier
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public class Supplier extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 供应商ID */
	private Integer supplierId;
	/** 供应商名称 */
	private String supplierName;
	/** 联系人 */
	private String contact;
	/** 电话号码 */
	private String tel;
	/** 手机号码 */
	private String phone;
	/** 邮箱 */
	private String mail;
	/** 供应商地址 */
	private String address;

	public void setSupplierId(Integer supplierId) 
	{
		this.supplierId = supplierId;
	}

	public Integer getSupplierId() 
	{
		return supplierId;
	}
	public void setSupplierName(String supplierName) 
	{
		this.supplierName = supplierName;
	}

	public String getSupplierName() 
	{
		return supplierName;
	}
	public void setContact(String contact) 
	{
		this.contact = contact;
	}

	public String getContact() 
	{
		return contact;
	}
	public void setTel(String tel) 
	{
		this.tel = tel;
	}

	public String getTel() 
	{
		return tel;
	}
	public void setPhone(String phone) 
	{
		this.phone = phone;
	}

	public String getPhone() 
	{
		return phone;
	}
	public void setMail(String mail) 
	{
		this.mail = mail;
	}

	public String getMail() 
	{
		return mail;
	}
	public void setAddress(String address) 
	{
		this.address = address;
	}

	public String getAddress() 
	{
		return address;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("supplierId", getSupplierId())
            .append("supplierName", getSupplierName())
            .append("contact", getContact())
            .append("tel", getTel())
            .append("phone", getPhone())
            .append("mail", getMail())
            .append("address", getAddress())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
