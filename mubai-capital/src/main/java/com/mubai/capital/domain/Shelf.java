package com.mubai.capital.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;
import java.util.Date;

/**
 * 货架表 ca_shelf
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public class Shelf extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 货架ID */
	private Integer shelfId;
	/** 货架名称 */
	private String shelfName;
	/** 货架编号 */
	private String shelfNo;
	/** 库区Id */
	private Integer zoneId;
	/** rfid编号 */
	private String rfid;
	/** 货架的层数 */
	private Integer layer;
	/** 货架状态（0 在用 1空闲 2报废 3停用） */
	private String status;

	public void setShelfId(Integer shelfId) 
	{
		this.shelfId = shelfId;
	}

	public Integer getShelfId() 
	{
		return shelfId;
	}
	public void setShelfName(String shelfName) 
	{
		this.shelfName = shelfName;
	}

	public String getShelfName() 
	{
		return shelfName;
	}
	public void setShelfNo(String shelfNo) 
	{
		this.shelfNo = shelfNo;
	}

	public String getShelfNo() 
	{
		return shelfNo;
	}
	public void setZoneId(Integer zoneId) 
	{
		this.zoneId = zoneId;
	}

	public Integer getZoneId() 
	{
		return zoneId;
	}
	public void setRfid(String rfid) 
	{
		this.rfid = rfid;
	}

	public String getRfid() 
	{
		return rfid;
	}
	public void setLayer(Integer layer) 
	{
		this.layer = layer;
	}

	public Integer getLayer() 
	{
		return layer;
	}
	public void setStatus(String status) 
	{
		this.status = status;
	}

	public String getStatus() 
	{
		return status;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("shelfId", getShelfId())
            .append("shelfName", getShelfName())
            .append("shelfNo", getShelfNo())
            .append("zoneId", getZoneId())
            .append("rfid", getRfid())
            .append("layer", getLayer())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
