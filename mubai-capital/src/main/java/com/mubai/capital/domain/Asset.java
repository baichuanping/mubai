package com.mubai.capital.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;
import java.util.Date;

/**
 * 资产表 ca_asset
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public class Asset extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 物品Id */
	private Integer assetId;
	/** 物品编号 */
	private String assetNo;
	/** 物品名称 */
	private String assetName;
	/** 货架编号 */
	private String shelfNo;
	/** 货架位置 （组成结构：层号/列号） */
	private String location;
	/** rfid */
	private String rfId;
	/** 物品大小 */
	private String size;
	/** 物品重量 */
	private String weight;
	/** 图片Id多个图片以,分隔 */
	private String imgId;
	/** 物品类型 10书画 20石器 30铜器 */
	private String type;
	/** 状态  0入库 1出库 2报废 */
	private Integer status;
	/** 创建者 */
	private String createBy;
	/** 创建时间 */
	private Date createTime;
	/** 更新者 */
	private String updateBy;
	/** 更新时间 */
	private Date updateTime;
	/** 备注 */
	private String remark;

	public void setAssetId(Integer assetId) 
	{
		this.assetId = assetId;
	}

	public Integer getAssetId() 
	{
		return assetId;
	}
	public void setAssetNo(String assetNo) 
	{
		this.assetNo = assetNo;
	}

	public String getAssetNo() 
	{
		return assetNo;
	}
	public void setAssetName(String assetName) 
	{
		this.assetName = assetName;
	}

	public String getAssetName() 
	{
		return assetName;
	}
	public void setShelfNo(String shelfNo) 
	{
		this.shelfNo = shelfNo;
	}

	public String getShelfNo() 
	{
		return shelfNo;
	}
	public void setLocation(String location) 
	{
		this.location = location;
	}

	public String getLocation() 
	{
		return location;
	}
	public void setRfId(String rfId) 
	{
		this.rfId = rfId;
	}

	public String getRfId() 
	{
		return rfId;
	}
	public void setSize(String size) 
	{
		this.size = size;
	}

	public String getSize() 
	{
		return size;
	}
	public void setWeight(String weight) 
	{
		this.weight = weight;
	}

	public String getWeight() 
	{
		return weight;
	}
	public void setImgId(String imgId) 
	{
		this.imgId = imgId;
	}

	public String getImgId() 
	{
		return imgId;
	}
	public void setType(String type) 
	{
		this.type = type;
	}

	public String getType() 
	{
		return type;
	}
	public void setStatus(Integer status) 
	{
		this.status = status;
	}

	public Integer getStatus() 
	{
		return status;
	}
	public void setCreateBy(String createBy) 
	{
		this.createBy = createBy;
	}

	public String getCreateBy() 
	{
		return createBy;
	}
	public void setCreateTime(Date createTime) 
	{
		this.createTime = createTime;
	}

	public Date getCreateTime() 
	{
		return createTime;
	}
	public void setUpdateBy(String updateBy) 
	{
		this.updateBy = updateBy;
	}

	public String getUpdateBy() 
	{
		return updateBy;
	}
	public void setUpdateTime(Date updateTime) 
	{
		this.updateTime = updateTime;
	}

	public Date getUpdateTime() 
	{
		return updateTime;
	}
	public void setRemark(String remark) 
	{
		this.remark = remark;
	}

	public String getRemark() 
	{
		return remark;
	}

    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("assetId", getAssetId())
            .append("assetNo", getAssetNo())
            .append("assetName", getAssetName())
            .append("shelfNo", getShelfNo())
            .append("location", getLocation())
            .append("rfId", getRfId())
            .append("size", getSize())
            .append("weight", getWeight())
            .append("imgId", getImgId())
            .append("type", getType())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
