package com.mubai.capital.mapper;

import com.mubai.capital.domain.Zone;
import java.util.List;	

/**
 * 货区 数据层
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public interface ZoneMapper 
{
	/**
     * 查询货区信息
     * 
     * @param zoneId 货区ID
     * @return 货区信息
     */
	public Zone selectZoneById(Integer zoneId);
	
	/**
     * 查询货区列表
     * 
     * @param zone 货区信息
     * @return 货区集合
     */
	public List<Zone> selectZoneList(Zone zone);
	
	/**
     * 新增货区
     * 
     * @param zone 货区信息
     * @return 结果
     */
	public int insertZone(Zone zone);
	
	/**
     * 修改货区
     * 
     * @param zone 货区信息
     * @return 结果
     */
	public int updateZone(Zone zone);
	
	/**
     * 删除货区
     * 
     * @param zoneId 货区ID
     * @return 结果
     */
	public int deleteZoneById(Integer zoneId);
	
	/**
     * 批量删除货区
     * 
     * @param zoneIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteZoneByIds(String[] zoneIds);
	
}