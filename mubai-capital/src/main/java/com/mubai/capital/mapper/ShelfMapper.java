package com.mubai.capital.mapper;

import com.mubai.capital.domain.Shelf;
import java.util.List;	

/**
 * 货架 数据层
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public interface ShelfMapper 
{
	/**
     * 查询货架信息
     * 
     * @param shelfId 货架ID
     * @return 货架信息
     */
	public Shelf selectShelfById(Integer shelfId);
	
	/**
     * 查询货架列表
     * 
     * @param shelf 货架信息
     * @return 货架集合
     */
	public List<Shelf> selectShelfList(Shelf shelf);
	
	/**
     * 新增货架
     * 
     * @param shelf 货架信息
     * @return 结果
     */
	public int insertShelf(Shelf shelf);
	
	/**
     * 修改货架
     * 
     * @param shelf 货架信息
     * @return 结果
     */
	public int updateShelf(Shelf shelf);
	
	/**
     * 删除货架
     * 
     * @param shelfId 货架ID
     * @return 结果
     */
	public int deleteShelfById(Integer shelfId);
	
	/**
     * 批量删除货架
     * 
     * @param shelfIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteShelfByIds(String[] shelfIds);
	
}