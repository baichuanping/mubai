package com.mubai.capital.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.capital.mapper.ReaderMapper;
import com.mubai.capital.domain.Reader;
import com.mubai.capital.service.ReaderService;
import com.mubai.common.support.Convert;

/**
 * 读写器 服务层实现
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Service
public class ReaderServiceImpl implements ReaderService
{
	@Autowired
	private ReaderMapper readerMapper;

	/**
	 * 查询读写器信息
	 *
	 * @param readerId 读写器ID
	 * @return 读写器信息
	 */
	@Override
	public Reader selectReaderById(String readerId)
	{
		return readerMapper.selectReaderById(readerId);
	}

	/**
	 * 查询读写器列表
	 *
	 * @param reader 读写器信息
	 * @return 读写器集合
	 */
	@Override
	public List<Reader> selectReaderList(Reader reader)
	{
		return readerMapper.selectReaderList(reader);
	}

	/**
	 * 新增读写器
	 *
	 * @param reader 读写器信息
	 * @return 结果
	 */
	@Override
	public int insertReader(Reader reader)
	{
		return readerMapper.insertReader(reader);
	}

	/**
	 * 修改读写器
	 *
	 * @param reader 读写器信息
	 * @return 结果
	 */
	@Override
	public int updateReader(Reader reader)
	{
		return readerMapper.updateReader(reader);
	}

	/**
	 * 删除读写器对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteReaderByIds(String ids)
	{
		return readerMapper.deleteReaderByIds(Convert.toStrArray(ids));
	}

}