package com.mubai.capital.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.capital.mapper.ZoneMapper;
import com.mubai.capital.domain.Zone;
import com.mubai.capital.service.ZoneService;
import com.mubai.common.support.Convert;

/**
 * 货区 服务层实现
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Service
public class ZoneServiceImpl implements ZoneService
{
	@Autowired
	private ZoneMapper zoneMapper;

	/**
	 * 查询货区信息
	 *
	 * @param zoneId 货区ID
	 * @return 货区信息
	 */
	@Override
	public Zone selectZoneById(Integer zoneId)
	{
		return zoneMapper.selectZoneById(zoneId);
	}

	/**
	 * 查询货区列表
	 *
	 * @param zone 货区信息
	 * @return 货区集合
	 */
	@Override
	public List<Zone> selectZoneList(Zone zone)
	{
		return zoneMapper.selectZoneList(zone);
	}

	/**
	 * 新增货区
	 *
	 * @param zone 货区信息
	 * @return 结果
	 */
	@Override
	public int insertZone(Zone zone)
	{
		return zoneMapper.insertZone(zone);
	}

	/**
	 * 修改货区
	 *
	 * @param zone 货区信息
	 * @return 结果
	 */
	@Override
	public int updateZone(Zone zone)
	{
		return zoneMapper.updateZone(zone);
	}

	/**
	 * 删除货区对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteZoneByIds(String ids)
	{
		return zoneMapper.deleteZoneByIds(Convert.toStrArray(ids));
	}

}