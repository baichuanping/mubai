package com.mubai.capital.service;

import com.mubai.capital.domain.Shelf;
import java.util.List;

/**
 * 货架 服务层
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public interface ShelfService
{
	/**
     * 查询货架信息
     * 
     * @param shelfId 货架ID
     * @return 货架信息
     */
	public Shelf selectShelfById(Integer shelfId);
	
	/**
     * 查询货架列表
     * 
     * @param shelf 货架信息
     * @return 货架集合
     */
	public List<Shelf> selectShelfList(Shelf shelf);
	
	/**
     * 新增货架
     * 
     * @param shelf 货架信息
     * @return 结果
     */
	public int insertShelf(Shelf shelf);
	
	/**
     * 修改货架
     * 
     * @param shelf 货架信息
     * @return 结果
     */
	public int updateShelf(Shelf shelf);
		
	/**
     * 删除货架信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteShelfByIds(String ids);
	
}
