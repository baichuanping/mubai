package com.mubai.capital.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.capital.mapper.ShelfMapper;
import com.mubai.capital.domain.Shelf;
import com.mubai.capital.service.ShelfService;
import com.mubai.common.support.Convert;

/**
 * 货架 服务层实现
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Service
public class ShelfServiceImpl implements ShelfService
{
	@Autowired
	private ShelfMapper shelfMapper;

	/**
	 * 查询货架信息
	 *
	 * @param shelfId 货架ID
	 * @return 货架信息
	 */
	@Override
	public Shelf selectShelfById(Integer shelfId)
	{
		return shelfMapper.selectShelfById(shelfId);
	}

	/**
	 * 查询货架列表
	 *
	 * @param shelf 货架信息
	 * @return 货架集合
	 */
	@Override
	public List<Shelf> selectShelfList(Shelf shelf)
	{
		return shelfMapper.selectShelfList(shelf);
	}

	/**
	 * 新增货架
	 *
	 * @param shelf 货架信息
	 * @return 结果
	 */
	@Override
	public int insertShelf(Shelf shelf)
	{
		return shelfMapper.insertShelf(shelf);
	}

	/**
	 * 修改货架
	 *
	 * @param shelf 货架信息
	 * @return 结果
	 */
	@Override
	public int updateShelf(Shelf shelf)
	{
		return shelfMapper.updateShelf(shelf);
	}

	/**
	 * 删除货架对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteShelfByIds(String ids)
	{
		return shelfMapper.deleteShelfByIds(Convert.toStrArray(ids));
	}

}