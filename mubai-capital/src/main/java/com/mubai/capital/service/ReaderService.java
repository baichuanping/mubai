package com.mubai.capital.service;

import com.mubai.capital.domain.Reader;
import java.util.List;

/**
 * 读写器 服务层
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public interface ReaderService
{
	/**
     * 查询读写器信息
     * 
     * @param readerId 读写器ID
     * @return 读写器信息
     */
	public Reader selectReaderById(String readerId);
	
	/**
     * 查询读写器列表
     * 
     * @param reader 读写器信息
     * @return 读写器集合
     */
	public List<Reader> selectReaderList(Reader reader);
	
	/**
     * 新增读写器
     * 
     * @param reader 读写器信息
     * @return 结果
     */
	public int insertReader(Reader reader);
	
	/**
     * 修改读写器
     * 
     * @param reader 读写器信息
     * @return 结果
     */
	public int updateReader(Reader reader);
		
	/**
     * 删除读写器信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteReaderByIds(String ids);
	
}
