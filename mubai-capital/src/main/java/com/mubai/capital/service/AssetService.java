package com.mubai.capital.service;

import com.mubai.capital.domain.Asset;
import java.util.List;

/**
 * 资产 服务层
 * 
 * @author baichuanping
 * @date 2019-01-08
 */
public interface AssetService
{
	/**
     * 查询资产信息
     * 
     * @param assetId 资产ID
     * @return 资产信息
     */
	public Asset selectAssetById(Integer assetId);
	
	/**
     * 查询资产列表
     * 
     * @param asset 资产信息
     * @return 资产集合
     */
	public List<Asset> selectAssetList(Asset asset);
	
	/**
     * 新增资产
     * 
     * @param asset 资产信息
     * @return 结果
     */
	public int insertAsset(Asset asset);
	
	/**
     * 修改资产
     * 
     * @param asset 资产信息
     * @return 结果
     */
	public int updateAsset(Asset asset);
		
	/**
     * 删除资产信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteAssetByIds(String ids);
	
}
