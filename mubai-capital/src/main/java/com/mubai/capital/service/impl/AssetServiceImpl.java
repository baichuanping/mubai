package com.mubai.capital.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.capital.mapper.AssetMapper;
import com.mubai.capital.domain.Asset;
import com.mubai.capital.service.AssetService;
import com.mubai.common.support.Convert;

/**
 * 资产 服务层实现
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Service
public class AssetServiceImpl implements AssetService
{
	@Autowired
	private AssetMapper assetMapper;

	/**
	 * 查询资产信息
	 *
	 * @param assetId 资产ID
	 * @return 资产信息
	 */
	@Override
	public Asset selectAssetById(Integer assetId)
	{
		return assetMapper.selectAssetById(assetId);
	}

	/**
	 * 查询资产列表
	 *
	 * @param asset 资产信息
	 * @return 资产集合
	 */
	@Override
	public List<Asset> selectAssetList(Asset asset)
	{
		return assetMapper.selectAssetList(asset);
	}

	/**
	 * 新增资产
	 *
	 * @param asset 资产信息
	 * @return 结果
	 */
	@Override
	public int insertAsset(Asset asset)
	{
		return assetMapper.insertAsset(asset);
	}

	/**
	 * 修改资产
	 *
	 * @param asset 资产信息
	 * @return 结果
	 */
	@Override
	public int updateAsset(Asset asset)
	{
		return assetMapper.updateAsset(asset);
	}

	/**
	 * 删除资产对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteAssetByIds(String ids)
	{
		return assetMapper.deleteAssetByIds(Convert.toStrArray(ids));
	}

}