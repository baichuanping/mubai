import com.fasterxml.jackson.core.JsonProcessingException;
import com.mubai.web.controller.activiti.ActProcessController;
import org.activiti.engine.*;
import org.activiti.engine.repository.ProcessDefinition;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

/**
 * 测试流程
 * @author baichuanping
 * @create 2018-12-13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class ActivitiTest {
	private static final Logger Log = LoggerFactory.getLogger(ProcessController.class);

	@Autowired
	private ProcessEngineConfiguration processEngineConfiguration;
	@Autowired
	RepositoryService repositoryService;
	@Autowired
	FormService formService;
	@Autowired
	TaskService taskService;
	// @Autowired
	// ActTaskService actTaskService;

	@Autowired
	RuntimeService runtimeService;

	@Autowired
	ManagementService managementService;

	@Autowired
	IdentityService identityService;

	@Autowired
	HistoryService historyService;

	@Autowired
	DynamicBpmnService dynamicBpmnService;

	@Test
	@Ignore
	public void load() throws JsonProcessingException {
		Log.info("===流程定义数========>>>>" + repositoryService.createProcessDefinitionQuery().count());

		for (ProcessDefinition p : repositoryService.createProcessDefinitionQuery().list()) {
			Log.info(p.getId() + "," + p.getDeploymentId()+","+p.getName()+","+p.getCategory()+","+p.getResourceName()+",version:"+p.getVersion()+","+ p.getKey());
		}

		// Log.info("根据key启动一个流程======>>");
		// ProcessInstance onboarding = runtimeService.startProcessInstanceByKey("onboarding");
		// Log.info("businessKey:"+onboarding.getBusinessKey()+",dep"+onboarding.getDeploymentId());
		// long count = runtimeService.createProcessInstanceQuery().count();
		// System.out.println("===流程实例========>>>>" + count);
		// ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().list().get(0);

		//
		// List<Task> list = taskService.createTaskQuery().list();
		// System.out.println("=====任务========>>>>"+list.size());
		// List<Task> tasks = taskService.createTaskQuery().list();
		// for (Task task : tasks) {
		//     Log.info("Task available: " +task.getId()+","+ task.getName()+","+task.getAssignee()+","+task.getOwner()+","+task.getDueDate()+","+ task.getAssignee());
		//     FormData formData = formService.getTaskFormData(task.getId());
		//     Log.info(formData.getFormKey()+","+formData.getDeploymentId()+",");
		//     List<FormProperty> formProperties = formData.getFormProperties();
		//     Log.info(new ObjectMapper().writeValueAsString(formProperties));
		// }
		// Map<String, Object> variables = new HashMap<>();
		// Task task = tasks.get(0);
		// variables.put("fullName","matosiki");
		// variables.put("yearsOfExperience",10L);
		// taskService.complete(task.getId(), variables);
		//
		// List<HistoricActivityInstance> activities =
		//         historyService.createHistoricActivityInstanceQuery()
		//                 .processInstanceId(processInstance.getId()).finished()
		//                 .orderByHistoricActivityInstanceEndTime().asc()
		//                 .list();
		//
		// for (HistoricActivityInstance activity : activities) {
		//     if (activity.getActivityType() == "startEvent") {
		//         Log.info("BEGIN " + activity.getActivityId()
		//                 + " [" + processInstance.getProcessDefinitionKey()
		//                 + "] " + activity.getStartTime());
		//     }
		// }
	}

	/**
	 * 直接获取某张表数据
	 */
	@Test
	public void managerTableQuery() {
		List<Map<String, Object>> rows = managementService.createTablePageQuery().tableName("sys_job").listPage(1, 10).getRows();

		for (Map<String, Object> row : rows) {
			for (String k : row.keySet()) {
				System.out.println("k===>"+k+",value===>"+row.get(k));
			}
		}
	}
}
