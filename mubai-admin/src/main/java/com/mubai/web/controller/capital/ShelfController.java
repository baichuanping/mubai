package com.mubai.web.controller.capital;

import com.mubai.capital.domain.Shelf;
import com.mubai.capital.service.ShelfService;
import com.mubai.common.annotation.Log;
import com.mubai.common.base.Return;
import com.mubai.common.enums.BusinessType;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.web.core.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 货架 信息操作处理
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Controller
@RequestMapping("/capital/shelf")
public class ShelfController extends BaseController
{
	private String prefix = "capital/shelf";

	@Autowired
	private ShelfService shelfService;

	@RequiresPermissions("capital:shelf:view")
	@GetMapping()
	public String shelf()
	{
		return prefix + "/shelf";
	}

	/**
	 * 查询货架列表
	 */
	@RequiresPermissions("capital:shelf:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Shelf shelf)
	{
		startPage();
		List<Shelf> list = shelfService.selectShelfList(shelf);
		return getDataTable(list);
	}


	/**
	 * 导出货架列表
	 */
	@RequiresPermissions("capital:shelf:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(Shelf shelf)
	{
		List<Shelf> list = shelfService.selectShelfList(shelf);
		ExcelUtil<Shelf> util = new ExcelUtil<Shelf>(Shelf.class);
		return util.exportExcel(list, "shelf");
	}

	/**
	 * 新增货架
	 */
	@GetMapping("/add")
	public String add()
	{
		return prefix + "/add";
	}

	/**
	 * 新增保存货架
	 */
	@RequiresPermissions("capital:shelf:add")
	@Log(title = "货架", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(Shelf shelf)
	{
		return toReturn(shelfService.insertShelf(shelf));
	}

	/**
	 * 修改货架
	 */
	@GetMapping("/edit/{shelfId}")
	public String edit(@PathVariable("shelfId") Integer shelfId, ModelMap mmap)
	{
		Shelf shelf = shelfService.selectShelfById(shelfId);
		mmap.put("shelf", shelf);
		return prefix + "/edit";
	}

	/**
	 * 修改保存货架
	 */
	@RequiresPermissions("capital:shelf:edit")
	@Log(title = "货架", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(Shelf shelf)
	{
		return toReturn(shelfService.updateShelf(shelf));
	}

	/**
	 * 删除货架
	 */
	@RequiresPermissions("capital:shelf:remove")
	@Log(title = "货架", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(shelfService.deleteShelfByIds(ids));
	}

}