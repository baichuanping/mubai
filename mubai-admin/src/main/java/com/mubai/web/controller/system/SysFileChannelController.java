package com.mubai.web.controller.system;

import com.mubai.common.annotation.Log;
import com.mubai.common.base.Return;
import com.mubai.common.enums.BusinessType;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.files.domain.FileChannel;
import com.mubai.files.service.FileChannelService;
import com.mubai.web.core.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 文件通道 信息操作处理
 *
 * @author baichuanping
 * @date 2019-01-19
 */
@Controller
@RequestMapping("/system/fileChannel")
public class SysFileChannelController extends BaseController
{
	private String prefix = "system/fileChannel";

	@Autowired
	private FileChannelService fileChannelService;

	@RequiresPermissions("system:fileChannel:view")
	@GetMapping()
	public String fileChannel()
	{
		return prefix + "/fileChannel";
	}

	/**
	 * 查询文件通道列表
	 */
	@RequiresPermissions("system:fileChannel:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(FileChannel fileChannel)
	{
		startPage();
		List<FileChannel> list = fileChannelService.selectFileChannelList(fileChannel);
		return getDataTable(list);
	}


	/**
	 * 导出文件通道列表
	 */
	@RequiresPermissions("system:fileChannel:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(FileChannel fileChannel)
	{
		List<FileChannel> list = fileChannelService.selectFileChannelList(fileChannel);
		ExcelUtil<FileChannel> util = new ExcelUtil<FileChannel>(FileChannel.class);
		return util.exportExcel(list, "fileChannel");
	}

	/**
	 * 新增通道文件
	 * @param channelCode
	 * @param mmap
	 * @return
	 */
	@GetMapping("/addfile")
	public String addFile(Long channelCode, ModelMap mmap) {
		mmap.put("channelCode",channelCode);
		return prefix + "/addfile";
	}
	/**
	 * 新增文件通道
	 */
	@GetMapping("/add")
	public String add()
	{
		return prefix + "/add";
	}

	/**
	 * 新增保存文件通道
	 */
	@RequiresPermissions("system:fileChannel:add")
	@Log(title = "文件通道", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(FileChannel fileChannel)
	{
		return toReturn(fileChannelService.insertFileChannel(fileChannel));
	}

	/**
	 * 修改文件通道
	 */
	@GetMapping("/edit/{channelId}")
	public String edit(@PathVariable("channelId") Long channelId, ModelMap mmap)
	{
		FileChannel fileChannel = fileChannelService.selectFileChannelById(channelId);
		mmap.put("fileChannel", fileChannel);
		return prefix + "/edit";
	}

	/**
	 * 修改保存文件通道
	 */
	@RequiresPermissions("system:fileChannel:edit")
	@Log(title = "文件通道", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(FileChannel fileChannel)
	{
		return toReturn(fileChannelService.updateFileChannel(fileChannel));
	}

	/**
	 * 删除文件通道
	 */
	@RequiresPermissions("system:fileChannel:remove")
	@Log(title = "文件通道", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(fileChannelService.deleteFileChannelByIds(ids));
	}

	/**
	 * 删除对应的播放文件
	 * @param ids
	 * @return
	 */
	@PostMapping( "/removeFile")
	@ResponseBody
	public Return removeFile(String ids)
	{
		return toReturn(fileChannelService.removeFile(ids));
	}

	@PostMapping( "/saveLimit")
	@ResponseBody
	public Return saveLimit(String ids)
	{
		return toReturn(fileChannelService.saveLimit(ids));
	}

}