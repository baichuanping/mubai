package com.mubai.web.controller.capital;

import java.util.List;

import com.mubai.common.base.Return;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mubai.common.annotation.Log;
import com.mubai.common.enums.BusinessType;
import com.mubai.capital.domain.Zone;
import com.mubai.capital.service.ZoneService;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.web.core.base.BaseController;

/**
 * 货区 信息操作处理
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Controller
@RequestMapping("/capital/zone")
public class ZoneController extends BaseController
{
	private String prefix = "capital/zone";

	@Autowired
	private ZoneService zoneService;

	@RequiresPermissions("capital:zone:view")
	@GetMapping()
	public String zone()
	{
		return prefix + "/zone";
	}

	/**
	 * 查询货区列表
	 */
	@RequiresPermissions("capital:zone:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Zone zone)
	{
		startPage();
		List<Zone> list = zoneService.selectZoneList(zone);
		return getDataTable(list);
	}


	/**
	 * 导出货区列表
	 */
	@RequiresPermissions("capital:zone:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(Zone zone)
	{
		List<Zone> list = zoneService.selectZoneList(zone);
		ExcelUtil<Zone> util = new ExcelUtil<Zone>(Zone.class);
		return util.exportExcel(list, "zone");
	}

	/**
	 * 新增货区
	 */
	@GetMapping("/add")
	public String add()
	{
		return prefix + "/add";
	}

	/**
	 * 新增保存货区
	 */
	@RequiresPermissions("capital:zone:add")
	@Log(title = "货区", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(Zone zone)
	{
		return toReturn(zoneService.insertZone(zone));
	}

	/**
	 * 修改货区
	 */
	@GetMapping("/edit/{zoneId}")
	public String edit(@PathVariable("zoneId") Integer zoneId, ModelMap mmap)
	{
		Zone zone = zoneService.selectZoneById(zoneId);
		mmap.put("zone", zone);
		return prefix + "/edit";
	}

	/**
	 * 修改保存货区
	 */
	@RequiresPermissions("capital:zone:edit")
	@Log(title = "货区", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(Zone zone)
	{
		return toReturn(zoneService.updateZone(zone));
	}

	/**
	 * 删除货区
	 */
	@RequiresPermissions("capital:zone:remove")
	@Log(title = "货区", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(zoneService.deleteZoneByIds(ids));
	}

}