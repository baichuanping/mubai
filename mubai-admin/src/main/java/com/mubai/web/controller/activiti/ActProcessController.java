package com.mubai.web.controller.activiti;

import com.mubai.activiti.dto.ProcessDefinitionDto;
import com.mubai.activiti.service.ActProcessService;
import com.mubai.common.annotation.Log;
import com.mubai.common.base.Return;
import com.mubai.common.enums.BusinessType;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.StringUtils;
import com.mubai.web.core.base.BaseController;
import org.activiti.engine.repository.Model;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * 流程管理 操作处理
 * @author baichuanping
 * @create 2018-12-13
 */
@Controller
@RequestMapping("/activiti/process")
public class ActProcessController extends BaseController {
	private String prefix = "activiti/process";

	@Autowired
	private ActProcessService actProcessService;

	@RequiresPermissions("activiti:process:view")
	@GetMapping
	public String process() {
		return prefix + "/process";
	}

	@RequiresPermissions("activiti:process:list")
	@PostMapping("list")
	@ResponseBody
	public TableDataInfo list(ProcessDefinitionDto processDefinitionDto) {
		return actProcessService.selectProcessDefinitionList(processDefinitionDto);
	}

	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	@RequiresPermissions("activiti:process:add")
	@Log(title = "流程管理", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(@RequestParam String category, @RequestParam("file") MultipartFile file)
			throws IOException {
		InputStream fileInputStream = file.getInputStream();
		String fileName = file.getOriginalFilename();
		return actProcessService.saveNameDeplove(fileInputStream, fileName, category);
	}

	/**
	 * 将部署的流程转换为模型
	 * @param processId
	 * @return
	 */
	@RequiresPermissions("activiti:process:model")
	@GetMapping(value = "/convertToModel/{processId}")
	@ResponseBody
	public Return convertToModel(@PathVariable("processId") String processId) {
		try {
			Model model = actProcessService.convertToModel(processId);
			return success(StringUtils.format("转换模型成功，模型编号[{}]", model.getId()));
		} catch (Exception e) {
			return error("转换模型失败" + e.getMessage());
		}
	}

	/**
	 * 使用部署对象ID查看流程图
	 * @param imageName
	 * @param deploymentId
	 * @param response
	 */
	@GetMapping(value = "/resource/{imageName}/{deploymentId}")
	public void viewImage(@PathVariable("imageName") String imageName,
						  @PathVariable("deploymentId") String deploymentId, HttpServletResponse response) {
		try {
			InputStream in = actProcessService.findImageStream(deploymentId, imageName);
			for (int bit = -1; (bit = in.read()) != -1; ) {
				response.getOutputStream().write(bit);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequiresPermissions("activiti:process:remove")
	@PostMapping("/remove")
	@ResponseBody
	public Return remove(String ids) {
		return toReturn(actProcessService.deleteProcessDefinitionByDeploymentIds(ids));
	}
}