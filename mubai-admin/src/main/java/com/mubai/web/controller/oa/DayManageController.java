package com.mubai.web.controller.oa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.mubai.common.base.Return;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mubai.common.annotation.Log;
import com.mubai.common.enums.BusinessType;
import com.mubai.oa.domain.DayManage;
import com.mubai.oa.service.DayManageService;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.web.core.base.BaseController;

/**
 * 日程 信息操作处理
 *
 * @author baichuanping
 * @date 2019-01-04
 */
@Controller
@RequestMapping("/oa/daymanage")
public class DayManageController extends BaseController
{
	private String prefix = "oa/daymanage";

	@Autowired
	private DayManageService dayManageService;

	@RequiresPermissions("oa:daymanage:view")
	@GetMapping()
	public String dayManage()
	{
		return prefix + "/daymanage";
	}

	/**
	 * 查询日程列表
	 */
	@RequiresPermissions("oa:daymanage:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(DayManage dayManage)
	{
		startPage();
		List<DayManage> list = dayManageService.selectDayManageList(dayManage);
		return getDataTable(list);
	}


	/**
	 * 导出日程列表
	 */
	@RequiresPermissions("oa:daymanage:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(DayManage dayManage)
	{
		List<DayManage> list = dayManageService.selectDayManageList(dayManage);
		ExcelUtil<DayManage> util = new ExcelUtil<DayManage>(DayManage.class);
		return util.exportExcel(list, "daymanage");
	}

	/**
	 * 新增日程
	 */
	@GetMapping("/add")
	public String add()
	{
		return prefix + "/add";
	}

	/**
	 * 新增保存日程
	 */
	@RequiresPermissions("oa:daymanage:add")
	@Log(title = "日程", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(DayManage dayManage)
	{
		Long[] userIds = dayManage.getUserIds();
		List<Long> list = new ArrayList(Arrays.asList(userIds));
		list.add(getUserId());
		dayManage.setUserIds(list.toArray(new Long[0]));
		return toReturn(dayManageService.insertDayManage(dayManage));
	}

	/**
	 * 修改日程
	 */
	@GetMapping("/edit/{dayId}")
	public String edit(@PathVariable("dayId") Long dayId, ModelMap mmap)
	{
		DayManage dayManage = dayManageService.selectDayManageById(dayId);
		mmap.put("dayManage", dayManage);
		return prefix + "/edit";
	}

	/**
	 * 修改保存日程
	 */
	@RequiresPermissions("oa:daymanage:edit")
	@Log(title = "日程", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(DayManage dayManage)
	{
		return toReturn(dayManageService.updateDayManage(dayManage));
	}

	/**
	 * 删除日程
	 */
	@RequiresPermissions("oa:daymanage:remove")
	@Log(title = "日程", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(dayManageService.deleteDayManageByIds(ids));
	}

	/**
	 * 获取我的日程信息
	 * @return
	 */
	@RequestMapping("mycalendarload")
	@ResponseBody
	public List<DayManage> mycalendarload() {
		List<DayManage> dayManages = dayManageService.selectDayByUserId(getUserId());
		return dayManages;
	}
}