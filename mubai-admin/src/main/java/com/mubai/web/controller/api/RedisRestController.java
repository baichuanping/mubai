package com.mubai.web.controller.api;

import com.mubai.common.base.Return;
import com.mubai.common.config.Global;
import com.mubai.common.enums.CODE;
import com.mubai.framework.util.RedisUtils;
import com.mubai.web.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author baichuanping
 * @create 2019-01-09
 */
@RestController
@RequestMapping("/rest/redis")
@Api(value = "Redis云数据库缓存接口",description = "Redis云数据缓存接口")
public class RedisRestController extends BaseController {

	@GetMapping(value = "/test")
	@ApiOperation(value="Redis Test信息", notes=" 测试Redis是否正常")
	public String test(){
		RedisUtils.set("Redis Test", "Redis Test");
		String string= RedisUtils.get("Redis Test").toString();
		return string;
	}

	/**
	 * 批量删除对应的key
	 *
	 * @param keys
	 */
	@PostMapping(value ="/removeList")
	@ApiOperation(value="Redis remove接口", notes="批量删除对应的key. keys=xx,xx,xx")
	public Return removeList(@RequestParam(required=false) String keys) {
		String [] keysList=keys.split(",");
		for(String key:keysList) {
			RedisUtils.remove(key);
		}
		return success("移除成功");
	}

	/**
	 * 批量删除key
	 * @param pattern
	 */
	@PostMapping(value ="/removePattern")
	@ApiOperation(value="Redis remove pattern接口", notes="批量删除key. pattern匹配")
	public Return removePattern(@RequestParam(required=false) String pattern) {
		RedisUtils.removePattern(pattern);
		return success("移除成功！");
	}


	/**
	 * 判断缓存中是否有对应的key
	 *
	 * @param key
	 * @return
	 */
	@GetMapping(value ="/exists")
	@ApiOperation(value="Redis exists接口", notes="判断缓存中是否有对应的key=?")
	public Return exists(@RequestParam(required=false) String key) {
		if(RedisUtils.exists(key)) {
			return success("存在！");
		} else {
			return success("不存在！");
		}
	}

	/**
	 * 读取缓存
	 *
	 * @param key
	 * @return
	 */
	@GetMapping(value ="/get")
	@ApiOperation(value="Redis get接口", notes="读取缓存 key=?")
	public Return get(@RequestParam(required=false) String key) {
		if(RedisUtils.exists(key)){
			Object obj=RedisUtils.get(key);
			return Return.SUCCESS(CODE.success).add("data",obj);
		}else{
			return error("不存在！");
		}
	}

	/**
	 * 写入缓存
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	@PostMapping(value ="/set")
	@ApiOperation(value="Redis set接口", notes="写入缓存 key=?&value=?")
	public Return set(@RequestParam(required=false) String key, @RequestParam(required=false)  String value) {
		if(RedisUtils.set(key,value)){
			Return result = success("添加/更新成功！");
			return result;
		}else{
			return error("添加/更新失败！");
		}
	}

	/**
	 * 写入缓存
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	@PostMapping(value ="/setExpireTime")
	@ApiOperation(value="Redis set expireTime接口", notes="写入缓存 key=?&value=?&expireTime=123")
	public Return set(@RequestParam(required=false) String key, @RequestParam(required=false) String value, @RequestParam(required=false) Long expireTime) {
		if(RedisUtils.set(key,value,expireTime)){
			Return result = success("添加/更新成功！");
			return result;
		}else{
			return error("添加/更新失败！");
		}
	}

	/**
	 * 获取keys
	 *
	 * @param pattern
	 */
	@GetMapping(value ="/getKeys")
	@ApiOperation(value="Redis get keys接口", notes="获取keys pattern=?")
	public Return getKeys(@RequestParam(required=false) String pattern) {
		Set<String> keys = RedisUtils.getKeys(pattern);
		Return result = success("获取Keys成功！");
		result.put("note",keys);
		return result;
	}

	@GetMapping(value ="/getKeysAll")
	@ApiOperation(value="Redis get keys all接口", notes="获取所有keys")
	public Return getKeysAll() {
		Set<String> keys = RedisUtils.getKeysAll();
		Return result = success("获取Keys成功！");
		result.put("note",keys);
		return result;
	}

	/**
	 * 获取keys 数量
	 *
	 * @param pattern
	 */
	@GetMapping(value ="/getCount")
	@ApiOperation(value="Redis get count接口", notes="获取keys 数量")
	public Return getCount(@RequestParam(required=false) String pattern) {
		Return result = success("获取数量成功！");
		result.put("note",RedisUtils.getCount());
		return result;
	}

	@GetMapping(value ="/getCountShiro")
	@ApiOperation(value="Redis get count shiro接口", notes="获取shiro缓存数量")
	public Return getCountShiro(@RequestParam(required=false) String pattern) {
		Return result = success("获取数量成功！");
		result.put("note",RedisUtils.getCountShiro());
		return result;
	}

	/**
	 * 获取key的有效期（秒）
	 * @param key
	 */
	@GetMapping(value ="/getExpireTime")
	@ApiOperation(value="Redis get ExpireTime接口", notes="获取key的有效期（秒）key=?")
	public Return getExpireTime(@RequestParam(required=false) String key){
		Return result = success("获取token的有效期（秒）成功！");
		result.put("note",RedisUtils.getExpireTime(key));
		return result;
	}

	/**
	 * 获取缓存有效期成功
	 */
	@GetMapping(value ="/getExpire")
	@ApiOperation(value="Redis get getExpire", notes="获取配置的缓存有效期")
	public Return getExpire(){
		Return result = success("获取缓存有效期成功！");
		result.put("note",RedisUtils.getExpire());
		return result;
	}

	/**
	 * 获取单点登录缓存有效期成功
	 */
	@GetMapping(value ="/getExpireShiro")
	@ApiOperation(value="Redis get getExpireShiro", notes="获取配置的shiro缓存有效期")
	public Return getExpireShiro(){
		Return result = success("获取单点登录缓存有效期成功！");
		result.put("note", Global.getConfig("shiro.redis.expireTimeShiro"));
		return result;
	}
}
