package com.mubai.web.controller.capital;

import java.util.List;

import com.mubai.common.base.Return;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mubai.common.annotation.Log;
import com.mubai.common.enums.BusinessType;
import com.mubai.capital.domain.Asset;
import com.mubai.capital.service.AssetService;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.web.core.base.BaseController;

/**
 * 资产 信息操作处理
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Controller
@RequestMapping("/capital/asset")
public class AssetController extends BaseController
{
	private String prefix = "capital/asset";

	@Autowired
	private AssetService assetService;

	@RequiresPermissions("capital:asset:view")
	@GetMapping()
	public String asset()
	{
		return prefix + "/asset";
	}

	/**
	 * 查询资产列表
	 */
	@RequiresPermissions("capital:asset:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Asset asset)
	{
		startPage();
		List<Asset> list = assetService.selectAssetList(asset);
		return getDataTable(list);
	}


	/**
	 * 导出资产列表
	 */
	@RequiresPermissions("capital:asset:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(Asset asset)
	{
		List<Asset> list = assetService.selectAssetList(asset);
		ExcelUtil<Asset> util = new ExcelUtil<Asset>(Asset.class);
		return util.exportExcel(list, "asset");
	}

	/**
	 * 新增资产
	 */
	@GetMapping("/add")
	public String add()
	{
		return prefix + "/add";
	}

	/**
	 * 新增保存资产
	 */
	@RequiresPermissions("capital:asset:add")
	@Log(title = "资产", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(Asset asset)
	{
		return toReturn(assetService.insertAsset(asset));
	}

	/**
	 * 修改资产
	 */
	@GetMapping("/edit/{assetId}")
	public String edit(@PathVariable("assetId") Integer assetId, ModelMap mmap)
	{
		Asset asset = assetService.selectAssetById(assetId);
		mmap.put("asset", asset);
		return prefix + "/edit";
	}

	/**
	 * 修改保存资产
	 */
	@RequiresPermissions("capital:asset:edit")
	@Log(title = "资产", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(Asset asset)
	{
		return toReturn(assetService.updateAsset(asset));
	}

	/**
	 * 删除资产
	 */
	@RequiresPermissions("capital:asset:remove")
	@Log(title = "资产", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(assetService.deleteAssetByIds(ids));
	}

}