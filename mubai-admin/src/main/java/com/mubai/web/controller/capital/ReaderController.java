package com.mubai.web.controller.capital;

import java.util.List;

import com.mubai.common.base.Return;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mubai.common.annotation.Log;
import com.mubai.common.enums.BusinessType;
import com.mubai.capital.domain.Reader;
import com.mubai.capital.service.ReaderService;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.web.core.base.BaseController;

/**
 * 读写器 信息操作处理
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Controller
@RequestMapping("/capital/reader")
public class ReaderController extends BaseController
{
	private String prefix = "capital/reader";

	@Autowired
	private ReaderService readerService;

	@RequiresPermissions("capital:reader:view")
	@GetMapping()
	public String reader()
	{
		return prefix + "/reader";
	}

	/**
	 * 查询读写器列表
	 */
	@RequiresPermissions("capital:reader:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Reader reader)
	{
		startPage();
		List<Reader> list = readerService.selectReaderList(reader);
		return getDataTable(list);
	}


	/**
	 * 导出读写器列表
	 */
	@RequiresPermissions("capital:reader:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(Reader reader)
	{
		List<Reader> list = readerService.selectReaderList(reader);
		ExcelUtil<Reader> util = new ExcelUtil<Reader>(Reader.class);
		return util.exportExcel(list, "reader");
	}

	/**
	 * 新增读写器
	 */
	@GetMapping("/add")
	public String add()
	{
		return prefix + "/add";
	}

	/**
	 * 新增保存读写器
	 */
	@RequiresPermissions("capital:reader:add")
	@Log(title = "读写器", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(Reader reader)
	{
		return toReturn(readerService.insertReader(reader));
	}

	/**
	 * 修改读写器
	 */
	@GetMapping("/edit/{readerId}")
	public String edit(@PathVariable("readerId") String readerId, ModelMap mmap)
	{
		Reader reader = readerService.selectReaderById(readerId);
		mmap.put("reader", reader);
		return prefix + "/edit";
	}

	/**
	 * 修改保存读写器
	 */
	@RequiresPermissions("capital:reader:edit")
	@Log(title = "读写器", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(Reader reader)
	{
		return toReturn(readerService.updateReader(reader));
	}

	/**
	 * 删除读写器
	 */
	@RequiresPermissions("capital:reader:remove")
	@Log(title = "读写器", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(readerService.deleteReaderByIds(ids));
	}

}