package com.mubai.web.controller.oa;

import com.mubai.web.core.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 我的邮箱处理
 * @author baichuanping
 * @create 2019-01-04
 */
@Controller
@RequestMapping("oa/mail")
public class MailController extends BaseController {

	private String prefix = "oa/mail";

	@RequiresPermissions("oa:mail:view")
	@GetMapping()
	public String mailView() {return prefix + "/mail";}
}
