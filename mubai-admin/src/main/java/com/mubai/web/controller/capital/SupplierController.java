package com.mubai.web.controller.capital;

import com.mubai.capital.domain.Supplier;
import com.mubai.capital.service.SupplierService;
import com.mubai.common.annotation.Log;
import com.mubai.common.base.Return;
import com.mubai.common.enums.BusinessType;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.web.core.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 供应商 信息操作处理
 *
 * @author baichuanping
 * @date 2019-01-08
 */
@Controller
@RequestMapping("/capital/supplier")
public class SupplierController extends BaseController
{
	private String prefix = "capital/supplier";

	@Autowired
	private SupplierService supplierService;

	@RequiresPermissions("capital:supplier:view")
	@GetMapping()
	public String supplier()
	{
		return prefix + "/supplier";
	}

	/**
	 * 查询供应商列表
	 */
	@RequiresPermissions("capital:supplier:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Supplier supplier)
	{
		startPage();
		List<Supplier> list = supplierService.selectSupplierList(supplier);
		return getDataTable(list);
	}


	/**
	 * 导出供应商列表
	 */
	@RequiresPermissions("capital:supplier:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(Supplier supplier)
	{
		List<Supplier> list = supplierService.selectSupplierList(supplier);
		ExcelUtil<Supplier> util = new ExcelUtil<Supplier>(Supplier.class);
		return util.exportExcel(list, "supplier");
	}

	/**
	 * 新增供应商
	 */
	@GetMapping("/add")
	public String add()
	{
		return prefix + "/add";
	}

	/**
	 * 新增保存供应商
	 */
	@RequiresPermissions("capital:supplier:add")
	@Log(title = "供应商", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(Supplier supplier)
	{
		return toReturn(supplierService.insertSupplier(supplier));
	}

	/**
	 * 修改供应商
	 */
	@GetMapping("/edit/{supplierId}")
	public String edit(@PathVariable("supplierId") Integer supplierId, ModelMap mmap)
	{
		Supplier supplier = supplierService.selectSupplierById(supplierId);
		mmap.put("supplier", supplier);
		return prefix + "/edit";
	}

	/**
	 * 修改保存供应商
	 */
	@RequiresPermissions("capital:supplier:edit")
	@Log(title = "供应商", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(Supplier supplier)
	{
		return toReturn(supplierService.updateSupplier(supplier));
	}

	/**
	 * 删除供应商
	 */
	@RequiresPermissions("capital:supplier:remove")
	@Log(title = "供应商", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(supplierService.deleteSupplierByIds(ids));
	}

}