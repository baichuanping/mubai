package com.mubai.web.controller.activiti;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mubai.activiti.dto.ModelEntityDto;
import com.mubai.activiti.service.ActModelService;
import com.mubai.common.annotation.Log;
import com.mubai.common.base.Return;
import com.mubai.common.enums.BusinessType;
import com.mubai.common.page.TableDataInfo;
import com.mubai.web.core.base.BaseController;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.ActivitiException;
import org.activiti.engine.impl.persistence.entity.ModelEntity;
import org.activiti.engine.repository.Model;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import static org.activiti.editor.constants.ModelDataJsonConstants.MODEL_DESCRIPTION;
import static org.activiti.editor.constants.ModelDataJsonConstants.MODEL_NAME;

/**
 * 模型管理 操作处理
 * @author baichuanping
 * @create 2018-12-13
 */
@RequestMapping("/activiti/model")
@Controller
public class ActModelController extends BaseController {
	protected static final Logger LOGGER = LoggerFactory.getLogger(ActModelController.class);

	private String prefix = "activiti/model";

	@Autowired
	ActModelService actModelService;

	@Autowired
	private ObjectMapper objectMapper;

	@RequestMapping(value = "/editor/stencilset", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
	@ResponseBody
	public String getStencilset() {
		InputStream stencilsetStream = this.getClass().getClassLoader().getResourceAsStream("stencilset.json");
		try {
			return IOUtils.toString(stencilsetStream, "utf-8");
		} catch (Exception e) {
			throw new ActivitiException("Error while loading stencil set", e);
		}
	}
	/**
	 * 进入模型列表页
	 *
	 * @return
	 */
	@RequiresPermissions("activiti:model:view")
	@GetMapping
	public String model()
	{
		return prefix + "/model";
	}


	@RequiresPermissions("activiti:model:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(ModelEntityDto modelEntityDto)
	{
		return actModelService.selectModelList(modelEntityDto);
	}


	@GetMapping("/add")
	public String add() throws UnsupportedEncodingException
	{
		ModelEntity model = new ModelEntity();
		String name = "new-process";
		String description = "新的流程";
		int revision = 1;
		String key = "process";
		ObjectNode modelNode = objectMapper.createObjectNode();
		modelNode.put(ModelDataJsonConstants.MODEL_NAME, name);
		modelNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, description);
		modelNode.put(ModelDataJsonConstants.MODEL_REVISION, revision);
		model.setName(name);
		model.setKey(key);
		model.setMetaInfo(modelNode.toString());
		String modelId = actModelService.createModel(model);
		return redirect("/modeler.html?modelId=" + modelId);
	}

	@GetMapping("/{modelId}/json")
	@ResponseBody
	public ObjectNode getEditorJson(@PathVariable String modelId)
	{
		ObjectNode modelNode = actModelService.selectWrapModelById(modelId);
		return modelNode;
	}


	@GetMapping("/edit/{modelId}")
	public String edit(@PathVariable("modelId") String modelId)
	{
		return redirect("/modeler.html?modelId=" + modelId);
	}


	@Log(title = "删除模型", businessType = BusinessType.DELETE)
	@RequiresPermissions("activiti:model:remove")
	@PostMapping("/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(actModelService.deleteModelIds(ids));
	}

	/**
	 * 发布流程
	 *
	 * @param modelId
	 * @return
	 * @throws Exception
	 */
	@Log(title = "发布流程", businessType = BusinessType.UPDATE)
	@RequiresPermissions("activiti:model:deploy")
	@GetMapping("/deploy/{modelId}")
	@ResponseBody
	public Return deploy(@PathVariable("modelId") String modelId) throws Exception
	{
		return actModelService.deployProcess(modelId);
	}


	@Log(title = "模型管理", businessType = BusinessType.UPDATE)
	@PutMapping(value = "/{modelId}/save")
	@ResponseBody
	public void save(@PathVariable String modelId, String name, String description, String json_xml,
					 String svg_xml) throws IOException, TranscoderException {
		Model model = actModelService.selectModelById(modelId);
		ObjectNode modelJson = (ObjectNode) objectMapper.readTree(model.getMetaInfo());
		modelJson.put(MODEL_NAME, name);
		modelJson.put(MODEL_DESCRIPTION, description);
		model.setMetaInfo(modelJson.toString());
		model.setName(name);
		actModelService.update(model, json_xml, svg_xml);

	}

	@Log(title = "导出指定模型", businessType = BusinessType.EXPORT)
	@RequiresPermissions("activiti:model:export")
	@RequestMapping("/export/{id}")
	public void exportToXml(@PathVariable("id") String id, HttpServletResponse response) {
		try {
			Model modelData = actModelService.selectModelById(id);
			BpmnJsonConverter jsonConverter = new BpmnJsonConverter();
			JsonNode editorNode = new ObjectMapper().readTree(actModelService.getModelEditorSource(modelData.getId()));
			BpmnModel bpmnModel = jsonConverter.convertToBpmnModel(editorNode);
			BpmnXMLConverter xmlConverter = new BpmnXMLConverter();
			byte[] bpmnBytes = xmlConverter.convertToXML(bpmnModel);

			ByteArrayInputStream in = new ByteArrayInputStream(bpmnBytes);
			IOUtils.copy(in, response.getOutputStream());
			String filename = bpmnModel.getMainProcess().getId() + ".bpmn20.xml";
			response.setHeader("Content-Disposition", "attachment; filename=" + filename);
			response.flushBuffer();
		} catch (Exception e) {
			throw new ActivitiException("导出model的xml文件失败，模型ID=" + id, e);
		}
	}

	// @Log(title = "模型导出", businessType = BusinessType.EXPORT)
	// @RequiresPermissions("activiti:model:export")
	// @PostMapping("/model/export")
	// @ResponseBody
	// public Return export() {
	//     List<Model> list = repositoryService.createModelQuery().list();
	//     ExcelUtil<CustomData> util = new ExcelUtil<>(CustomData.class);
	//     ArrayList<CustomData> lists = new ArrayList<>();
	//     for (Model model : list) {
	//         CustomData customData = new CustomData();
	//         customData.setId(model.getId());
	//         customData.setName(model.getName());
	//         lists.add(customData);
	//     }
	//     return util.exportExcel(lists, "modelData");
	// }

}
