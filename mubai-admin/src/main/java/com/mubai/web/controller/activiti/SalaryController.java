package com.mubai.web.controller.activiti;

import java.util.List;

import com.mubai.activiti.dto.SalaryDto;
import com.mubai.common.base.Return;
import com.mubai.system.service.SysPostService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mubai.common.annotation.Log;
import com.mubai.common.enums.BusinessType;
import com.mubai.activiti.domain.Salary;
import com.mubai.activiti.service.SalaryService;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.web.core.base.BaseController;

/**
 * 薪水调整 信息操作处理
 *
 * @author baichuanping
 * @date 2018-12-29
 */
@Controller
@RequestMapping("/activiti/salary")
public class SalaryController extends BaseController
{
	private String prefix = "activiti/salary";

	@Autowired
	private SalaryService salaryService;

	@Autowired
	private SysPostService sysPostService;

	@RequiresPermissions("activiti:salary:view")
	@GetMapping()
	public String salary()
	{
		return prefix + "/salary";
	}

	/**
	 * 查询薪水调整列表
	 */
	@RequiresPermissions("activiti:salary:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Salary salary)
	{
		startPage();
		List<Salary> list = salaryService.selectSalaryList(salary);
		return getDataTable(list);
	}


	/**
	 * 导出薪水调整列表
	 */
	@RequiresPermissions("activiti:salary:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(Salary salary)
	{
		List<Salary> list = salaryService.selectSalaryList(salary);
		ExcelUtil<Salary> util = new ExcelUtil<Salary>(Salary.class);
		return util.exportExcel(list, "salary");
	}

	/**
	 * 新增薪水调整
	 */
	@GetMapping("/form")
	public String add(ModelMap mmap)
	{
		// 获取所有岗位
		mmap.put("posts", sysPostService.selectPostAll());
		return prefix + "/add";
	}

	/**
	 * 新增保存薪水调整
	 */
	@RequiresPermissions("activiti:salary:add")
	@Log(title = "薪水调整", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(SalaryDto salary)
	{
		return toReturn(salaryService.saveSalary(getUserId(),salary));
	}

	/**修改薪水调整
	 *
	 */
	@GetMapping("/form/{taskId}")
	public String edit(@PathVariable("taskId") String taskId, ModelMap mmap)
	{
		Salary salary = salaryService.selectSalaryByTaskId(taskId);
		mmap.put("salary", salary);
		return prefix + "/edit";
	}

	/**
	 * 修改保存薪水调整
	 */
	@RequiresPermissions("activiti:salary:edit")
	@Log(title = "薪水调整", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(SalaryDto salaryDto)
	{
		return toReturn(salaryService.updateSalaryInfo(salaryDto));
	}

	/**
	 * 删除薪水调整
	 */
	@RequiresPermissions("activiti:salary:remove")
	@Log(title = "薪水调整", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(salaryService.deleteSalaryByIds(ids));
	}

}