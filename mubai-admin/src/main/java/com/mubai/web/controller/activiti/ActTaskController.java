package com.mubai.web.controller.activiti;

import com.mubai.activiti.dto.ProcessDefinitionDto;
import com.mubai.activiti.dto.TaskEntityDto;
import com.mubai.activiti.service.ActProcessService;
import com.mubai.activiti.service.ActTaskService;
import com.mubai.common.annotation.Log;
import com.mubai.common.base.Return;
import com.mubai.common.enums.BusinessType;
import com.mubai.common.page.TableDataInfo;
import com.mubai.framework.util.ShiroUtils;
import com.mubai.web.core.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * 代办任务
 * @author baichuanping
 * @create 2018-12-13
 */
@RequestMapping("activiti/task")
@Controller
public class ActTaskController extends BaseController {

	@Autowired
	ActTaskService actTaskService;

	@Autowired
	private ActProcessService actProcessService;

	private String prefix = "activiti/task";
	/**
	 * 进入待办任务页
	 * @return
	 */
	@RequiresPermissions("activiti:task:view")
	@GetMapping
	public String task() {
		return prefix + "/tasks";
	}

	@RequiresPermissions("activiti:task:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(TaskEntityDto taskEntityDto) {
		if (!"admin".equalsIgnoreCase(ShiroUtils.getLoginName())){
			taskEntityDto.setAssignee(String.valueOf(ShiroUtils.getUserId()));
		}
		return actTaskService.selectTaskEntityList(taskEntityDto);
	}

	/**
	 * 进入任务申请列表
	 * @return
	 */
	@RequiresPermissions("activiti:task:goto")
	@GetMapping("goto")
	public String gotoTask(){
		return prefix + "/goto";
	}

	/**
	 * 可申请的流程
	 * @param processDefinitionDto
	 * @return
	 */
	@RequiresPermissions("activiti:task:gotoList")
	@PostMapping("/gotoList")
	@ResponseBody
	public TableDataInfo list(ProcessDefinitionDto processDefinitionDto) {
		return actProcessService.selectProcessDefinitionList(processDefinitionDto);
	}

	/**
	 * 跳转到新增
	 * @return
	 */
	@RequiresPermissions("activiti:task:add")
	@GetMapping("/add")
	public String add() {
		return prefix + "/add";
	}

	/**
	 * 编辑
	 * @param taskId
	 * @param map
	 * @return
	 */
	@RequiresPermissions("activiti:task:edit")
	@PostMapping("/edit/{taskId}")
	@ResponseBody
	public String edit(@PathVariable("taskId") String taskId, ModelMap map) {
		TaskEntityDto taskEntityDto = actTaskService.selectOneTask(taskId);
		map.put("task", taskEntityDto);
		return prefix + "/edit";
	}

	/**
	 * 更新任务
	 * @param taskEntityDto
	 * @param map
	 * @return
	 */
	@Log(title = "更新任务", businessType = BusinessType.OTHER)
	@RequiresPermissions("activiti:task:edit")
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(TaskEntityDto taskEntityDto, ModelMap map) {
		actTaskService.completeTask(taskEntityDto.getId(), map);
		return toReturn(1);
	}

	/**
	 * 代办任务
	 * @param taskEntityDto
	 * @param map
	 * @return
	 */
	@Log(title = "待办任务", businessType = BusinessType.OTHER)
	@RequiresPermissions("activiti:task:view")
	@PostMapping("/todo")
	@ResponseBody
	public TableDataInfo todo(TaskEntityDto taskEntityDto,ModelMap map) {
		taskEntityDto.setAssignee(ShiroUtils.getLoginName());
		return actTaskService.selectTodoTask(taskEntityDto);
	}

	/**
	 * 受邀任务
	 * @param taskEntityDto
	 * @param map
	 * @return
	 */
	@Log(title = "受邀任务", businessType = BusinessType.OTHER)
	@RequiresPermissions("activiti:task:view")
	@PostMapping("/involved")
	@ResponseBody
	public TableDataInfo selectInvolvedTask(TaskEntityDto taskEntityDto,ModelMap map) {
		taskEntityDto.setInvolvedUser(ShiroUtils.getLoginName());
		return actTaskService.selectInvolvedTask(taskEntityDto);
	}

	/**
	 * 归档任务
	 * @param taskEntityDto
	 * @param map
	 * @return
	 */
	@Log(title = "归档任务", businessType = BusinessType.OTHER)
	@RequiresPermissions("activiti:task:view")
	@PostMapping("/archived")
	@ResponseBody
	public TableDataInfo selectArchivedTask(TaskEntityDto taskEntityDto,ModelMap map) {
		taskEntityDto.setOwner(ShiroUtils.getLoginName());
		return actTaskService.selectArchivedTask(taskEntityDto);
	}

	/**
	 * 查询完成的任务
	 * @param taskEntityDto
	 * @param map
	 * @return
	 */
	@Log(title = "查询完成的任务", businessType = BusinessType.OTHER)
	@RequiresPermissions("activiti:task:view")
	@PostMapping("/finishedTask")
	public TableDataInfo finishedTask(TaskEntityDto taskEntityDto, ModelMap map) {
		taskEntityDto.setOwner(ShiroUtils.getLoginName());
		return actTaskService.selectFinishedTask(taskEntityDto);
	}


	@GetMapping("/form/{processId}")
	public String startForm(@PathVariable("processId") String processId, HttpServletResponse response) throws IOException {
		String formKey = actTaskService.getFormKey(processId, null);
		return redirect(formKey);
	}

	/**
	 * 获取流程XML上的表单KEY
	 * @param processId
	 * @param taskId
	 * @throws IOException
	 */
	@GetMapping("/form/{processId}/{taskId}")
	public String form(@PathVariable("processId") String processId, @PathVariable("taskId") String taskId) throws IOException {
		// 获取流程XML上的表单KEY
		String formKey = actTaskService.getFormKey(processId, taskId);
		return redirect(formKey + "/" + taskId);
	}


	/**
	 * 读取带跟踪的图片
	 */
	@RequestMapping(value = "/trace/photo/{processId}/{execId}")
	public void traceTaskPhoto(@PathVariable("processId") String processId, @PathVariable("execId") String execId, HttpServletResponse response) throws Exception {
		InputStream imageStream = actTaskService.traceTaskPhoto(processId, execId);
		// 输出资源内容到相应对象
		byte[] b = new byte[1024];
		int len;
		while ((len = imageStream.read(b, 0, 1024)) != -1) {
			response.getOutputStream().write(b, 0, len);
		}
	}

}
