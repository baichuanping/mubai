package com.mubai.web.controller.api;

import com.github.pagehelper.PageHelper;
import com.mubai.common.base.Return;
import com.mubai.common.enums.CODE;
import com.mubai.common.page.TableDataInfo;
import com.mubai.system.domain.SysUser;
import com.mubai.system.service.SysUserService;
import com.mubai.web.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.WebAsyncTask;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * 对外提供接口的测试
 * @author baichuanping
 * @create 2019-01-09
 */
@RestController
@RequestMapping("/api")
@Api(value = "JWT示范接口", description = "JWT示范接口")
public class ApiTestController extends BaseController {
	@Autowired
	private SysUserService sysUserService;

	@GetMapping(value = "/test")
	@ApiOperation(value = "测试Callable", notes = "测试 Callable; Callable类型 swagger 不能使用try,故实际开发建议使用 WebAsyncTask ,后者也是要强大的多")
	public Callable test() {
		System.out.println(new Date());
		return () -> {
			PageHelper.startPage(1,10);
			List<SysUser> sysUserList = sysUserService.selectUserList(new SysUser());
			TableDataInfo dataTable = getDataTable(sysUserList);
			return Return.SUCCESS(CODE.success).add("data",dataTable);
		};
	}

	@PostMapping(value = "/test2")
	@ApiOperation(value="测试WebAsyncTask", notes="测试WebAsyncTask")
	public WebAsyncTask test2(HttpServletRequest request) {
		WebAsyncTask test2 = new WebAsyncTask(() -> {
			String test = request.getParameter("test");
			System.out.println("接收到参数信息:"+test);
			return "1151616";
		});
		return test2;
	}
	@PostMapping(value = "/test3")
	@ApiOperation(value="测试body", notes="测试body")
	public WebAsyncTask test3(HttpServletRequest request, @RequestBody String bodyData) {
		WebAsyncTask test2 = new WebAsyncTask(() -> {
			String test = request.getParameter("test");
			System.out.println("接收到参数:"+test);
			return bodyData;
		});
		return test2;
	}

	@PostMapping(value = "/test4")
	@ApiOperation(value="测试beanBody", notes="测试beanBody")
	public WebAsyncTask test4(HttpServletRequest request,@RequestBody SysUser SysUser){
		WebAsyncTask test2 = new WebAsyncTask( () -> {
			return SysUser;
		});
		return test2;
	}

	@PostMapping(value = "/test5")
	@ApiOperation(value="测试bean", notes="测试bean")
	public WebAsyncTask test5(HttpServletRequest request, SysUser SysUser){
		WebAsyncTask test2 = new WebAsyncTask( () -> {
			return SysUser;
		});
		return test2;
	}
}
