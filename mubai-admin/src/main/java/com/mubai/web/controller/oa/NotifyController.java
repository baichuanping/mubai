package com.mubai.web.controller.oa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mubai.common.annotation.Log;
import com.mubai.common.base.Return;
import com.mubai.common.constant.OAConstants;
import com.mubai.common.enums.BusinessType;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.ExcelUtil;
import com.mubai.framework.manager.AsyncManager;
import com.mubai.framework.manager.factory.AsyncFactory;
import com.mubai.framework.util.ShiroUtils;
import com.mubai.oa.domain.Notify;
import com.mubai.oa.domain.NotifyDto;
import com.mubai.oa.service.NotifyRecordService;
import com.mubai.oa.service.NotifyService;
import com.mubai.web.core.base.BaseController;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 通知通告 信息操作处理
 *
 * @author baichuanping
 * @date 2018-12-18
 */
@Controller
@RequestMapping("/oa/notify")
public class NotifyController extends BaseController
{
	private String prefix = "oa/notify";

	@Autowired
	private NotifyService notifyService;
	@Autowired
	private NotifyRecordService notifyRecordService;

	@RequiresPermissions("oa:notify:view")
	@GetMapping()
	public String notifyView()
	{
		return prefix + "/notify";
	}

	/**
	 * 查询通知通告列表
	 */
	@RequiresPermissions("oa:notify:list")
	@PostMapping("/list")
	@ResponseBody
	public TableDataInfo list(Notify notify)
	{
		startPage();
		List<Notify> list = notifyService.selectNotifyList(notify);
		return getDataTable(list);
	}

	/**
	 * 导出通知通告列表
	 */
	@RequiresPermissions("oa:notify:export")
	@PostMapping("/export")
	@ResponseBody
	public Return export(Notify notify)
	{
		List<Notify> list = notifyService.selectNotifyList(notify);
		ExcelUtil<Notify> util = new ExcelUtil<Notify>(Notify.class);
		return util.exportExcel(list, "notify");
	}

	/**
	 * 新增通知通告
	 */
	@GetMapping("/add")
	public String add()
	{
		return prefix + "/add";
	}

	/**
	 * 新增保存通知通告
	 */
	@RequiresPermissions("oa:notify:add")
	@Log(title = "通知通告", businessType = BusinessType.INSERT)
	@PostMapping("/add")
	@ResponseBody
	public Return addSave(Notify notify)
	{
		notify.setCreateBy(ShiroUtils.getLoginName());

		int r=notifyService.insertNotify(notify);
		// 发送消息给在线用户
		AsyncManager.me().execute(AsyncFactory.sendNoticeOnLine(notify.getLoginNames(),notify.getTitle()));
		return toReturn(r);
	}

	/**
	 * 修改通知通告
	 */
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, ModelMap mmap)
	{
		Notify notify = notifyService.selectNotifyById(id);
		mmap.put("notify", notify);
		return prefix + "/edit";
	}

	/**
	 * 修改保存通知通告
	 */
	@RequiresPermissions("oa:notify:edit")
	@Log(title = "通知通告", businessType = BusinessType.UPDATE)
	@PostMapping("/edit")
	@ResponseBody
	public Return editSave(Notify notify)
	{
		notify.setUpdateBy(ShiroUtils.getLoginName());
		return toReturn(notifyService.updateNotify(notify));
	}

	/**
	 * 删除通知通告
	 */
	@RequiresPermissions("oa:notify:remove")
	@Log(title = "通知通告", businessType = BusinessType.DELETE)
	@PostMapping( "/remove")
	@ResponseBody
	public Return remove(String ids)
	{
		return toReturn(notifyService.deleteNotifyByIds(ids));
	}

	/**
	 * 读取通知信息
	 * @return
	 */
	@GetMapping("/message")
	@ResponseBody
	public TableDataInfo message() {
		Map<String, Object> params = new HashMap<>(16);
//		params.put("offset", 0);
//		params.put("limit", 3);
		params.put("userId", getUserId());
		params.put("isRead", OAConstants.NOTIFY_READ_NO);
		startPage();
		List<NotifyDto> list = notifyService.selfList(params);
		return getDataTable(list);
	}

	@GetMapping("/selfnotify")
	public String selfNotify() { return prefix + "/selfnotify";}

	@GetMapping("/selfList")
	@ResponseBody
	public TableDataInfo selfList() {
		Map<String, Object> params = new HashMap<>(16);
//		params.put("offset", 0);
//		params.put("limit", 3);
		params.put("userId", getUserId());
		params.put("isRead", OAConstants.NOTIFY_READ_NO);
		startPage();
		List<NotifyDto> list = notifyService.selfList(params);
		return getDataTable(list);
	}

	/**
	 * 已读
	 * @param id
	 * @param mmap
	 * @return
	 */
	@GetMapping("/read/{id}")
	public String read(@PathVariable("id") Long id, ModelMap mmap) {
		Notify notify = notifyService.selectNotifyById(id);
		// 更改阅读状态
		notifyRecordService.changeRead(ShiroUtils.getUserId(),id);
		mmap.addAttribute("notify",notify);
		return prefix+"/read";
	}
}