package com.mubai.web.controller.system;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mubai.common.base.Return;
import com.mubai.common.config.Global;
import com.mubai.common.enums.CODE;
import com.mubai.common.tool.ToolJackson;
import com.mubai.framework.util.JwtUtils;
import com.mubai.framework.util.ShiroUtils;
import com.mubai.system.domain.SysUser;
import com.mubai.web.core.base.BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.mubai.common.utils.StringUtils;
import com.mubai.common.utils.ServletUtils;

import java.util.Map;

/**
 * 登录验证
 * 
 * @author baichuanping
 */
@Controller
@Api(value = "登录接口",description = "登录接口登录和刷新token")
public class SysLoginController extends BaseController
{
    private static final Logger logger = LoggerFactory.getLogger(SysLoginController.class);
    @GetMapping("/login")
    public String login(HttpServletRequest request, HttpServletResponse response)
    {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request))
        {
            return ServletUtils.renderString(response, "{\"note\":\"1\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }

//        return "login";
        return "login10";
    }

    @PostMapping("/login")
    @ResponseBody
    @ApiOperation(value = "登录", notes = "jwt登录参数jwt=true,返回token位于header")
    public Return ajaxLogin(String username, String password, @RequestParam(defaultValue = "false") Boolean rememberMe,
                            @RequestParam(defaultValue = "false") Boolean jwt, HttpServletResponse response)
    {
        UsernamePasswordToken token = new UsernamePasswordToken(username, password, rememberMe);
        Subject subject = SecurityUtils.getSubject();
        try
        {
            subject.login(token);
            if (jwt) {
                // jwt登录
                Map<String, Object> jwtUserInfo = ShiroUtils.getJwtUserInfo();
                String jwtUserInfoToken = ShiroUtils.getJwtUserInfoToken();
                response.setHeader("token",jwtUserInfoToken);
                return Return.SUCCESS(CODE.success).add("data",jwtUserInfo);
            }
            return success();
        }
        catch (AuthenticationException e)
        {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage()))
            {
                msg = e.getMessage();
            }
            return error(msg);
        }
    }

    @GetMapping("/unauth")
    public String unauth()
    {
        return "/error/unauth";
    }

    /**
     * 刷新jwt token
     * @param response
     * @return
     */
    @PostMapping("/api/refreshToken")
    @ResponseBody
    @ApiOperation(value = "刷新token", notes = "header带上参数有效token")
    public Return refreshToken(HttpServletResponse response) {
        String token = ServletUtils.getRequest().getHeader("token");
        SysUser sysUser = ShiroUtils.getSysUserByJwtToken(token);
        if (sysUser != null) {
            try {
                String newToken = JwtUtils.createToken(ToolJackson.toJson(sysUser), Global.getJwtOutTime());
                response.setHeader("token",newToken);
                return success();
            } catch (Exception e) {
                logger.error("刷新jwt token");
                return error("系统错误");
            }
        }else{
            return Return.FAIL(CODE.token_invalid);
        }
    }
}
