package com.mubai.web.controller.system;

import java.util.List;

import com.mubai.web.core.base.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import com.mubai.common.config.Global;
import com.mubai.system.domain.SysMenu;
import com.mubai.system.domain.SysUser;
import com.mubai.system.service.SysMenuService;

/**
 * 首页 业务处理
 * 
 * @author baichuanping
 */
@Controller
public class SysIndexController extends BaseController
{
    @Autowired
    private SysMenuService menuService;

    // 系统首页
    @GetMapping("/index")
    public String index(ModelMap mmap)
    {
        // 取身份信息
        SysUser user = getSysUser();
        // 根据用户id取出菜单
        List<SysMenu> menus = menuService.selectMenusByUser(user);
        mmap.put("menus", menus);
        mmap.put("user", user);
        mmap.put("copyrightYear", Global.getCopyrightYear());
//        return "index";
        return "win10";
    }

    // 系统介绍
    @GetMapping("/system/main")
    public String main(ModelMap mmap)
    {
        mmap.put("version", Global.getVersion());
        return "main";
    }
}
