package com.mubai.web.core.base;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.mubai.common.base.Return;
import com.mubai.common.base.BaseEntity;
import com.mubai.common.page.PageDomain;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.DateUtils;
import com.mubai.common.utils.StringUtils;
import com.mubai.framework.util.ShiroUtils;
import com.mubai.common.page.TableSupport;
import com.mubai.system.domain.SysUser;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import java.beans.PropertyEditorSupport;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * web层通用数据处理
 * 
 * @author baichuanping
 */
public class BaseController
{
    /**
     * 将前台传递过来的日期格式的字符串，自动转化为Date类型
     */
    @InitBinder
    public void initBinder(WebDataBinder binder)
    {
        // Date 类型转换
        binder.registerCustomEditor(Date.class, new PropertyEditorSupport()
        {
            @Override
            public void setAsText(String text)
            {
                setValue(DateUtils.parseDate(text));
            }
        });
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage()
    {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize))
        {
            String orderBy = pageDomain.getOrderBy();
            PageHelper.startPage(pageNum, pageSize, orderBy);
        }
    }

    /**
     * 设置请求分页数据
     */
    protected void startPage(BaseEntity baseEntity) {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        if (StringUtils.isNotNull(pageNum) && StringUtils.isNotNull(pageSize)) {
            Map<String, Object> params = baseEntity.getParams();
            if (params == null) {
                params = new HashMap<>();
            }
            params.put("pageNum", pageNum.toString());
            params.put("pageSize", pageSize.toString());
            baseEntity.setParams(params);
        }
    }
    /**
     * 响应请求分页数据
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected TableDataInfo getDataTable(List<?> list)
    {
        TableDataInfo rspData = new TableDataInfo();
        rspData.setCode(200);
        rspData.setRows(list);
        rspData.setTotal(new PageInfo(list).getTotal());
        return rspData;
    }

    /**
     * 响应返回结果
     * 
     * @param rows 影响行数
     * @return 操作结果
     */
    protected Return toReturn(int rows)
    {
        return rows > 0 ? success() : error();
    }
    /**
     * 响应返回结果
     *
     * @param result 结果
     * @return 操作结果
     */
    protected Return toReturn(boolean result)
    {
        return result ? success() : error();
    }

    /**
     * 返回成功
     */
    public Return success()
    {
        return Return.SUCCESS(200,"操作成功");
    }

    /**
     * 返回失败消息
     */
    public Return error()
    {
        return Return.FAIL(500,"操作失败");
    }

    /**
     * 返回成功消息
     */
    public Return success(String message)
    {
        return Return.SUCCESS(200,message);
    }

    /**
     * 返回失败消息
     */
    public Return error(String message)
    {
        return Return.FAIL(500,message);
    }

    /**
     * 返回错误码消息
     */
    public Return error(int code, String message)
    {
        return Return.FAIL(code, message);
    }

    /**
     * 页面跳转
     */
    public String redirect(String url)
    {
        return StringUtils.format("redirect:{}", url);
    }

    public SysUser getSysUser()
    {
        return ShiroUtils.getSysUser();
    }

    public void setSysUser(SysUser user)
    {
        ShiroUtils.setSysUser(user);
    }

    public Long getUserId()
    {
        return getSysUser().getUserId();
    }

    public String getLoginName()
    {
        return getSysUser().getLoginName();
    }
}
