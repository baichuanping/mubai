package com.mubai.activiti.dto;

import com.mubai.common.base.BaseActDto;
import lombok.Getter;
import lombok.Setter;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;

/**
 * 流程实体数据对象
 * @author baichuanping
 * @create 2018-12-27
 */
@Setter
@Getter
public class ProcessDefinitionDto extends BaseActDto
{
	private static final long serialVersionUID = 1L;

	/**
	 * 流程编号
	 */
	private String processId;

	/**
	 * 流程名称
	 */
	private String name;

	/**
	 * 部署编号
	 */
	private String deploymentId;

	/**
	 * 流程命名空间（该编号就是流程文件targetNamespace的属性值）
	 */
	private String category;

	/**
	 * 流程编号（该编号就是流程文件process元素的id属性值）
	 */
	private String key;

	/**
	 * 资源文件名称
	 */
	private String resourceName;

	/**
	 * 图片资源文件名称
	 */
	private String diagramResourceName;

	/**
	 * 版本号
	 */
	private int version;

	private Boolean suspended;
	private Boolean latest;
	private String tenantId;
	private String startableByUser;

	/**
	 * 描述信息
	 */
	private String description;

	public ProcessDefinitionDto(Deployment processDefinition)
	{
		this.setProcessId(processDefinition.getId());
		this.name = processDefinition.getName();
	}

	public ProcessDefinitionDto(ProcessDefinition processDefinition)
	{
		this.setProcessId(processDefinition.getId());
		this.name = processDefinition.getName();
		this.deploymentId = processDefinition.getDeploymentId();
		this.category = processDefinition.getCategory();
		this.key = processDefinition.getKey();
		this.resourceName = processDefinition.getResourceName();
		this.diagramResourceName = processDefinition.getDiagramResourceName();
		this.version = processDefinition.getVersion();
		this.description = processDefinition.getDescription();
		this.suspended = processDefinition.isSuspended();
		this.tenantId = processDefinition.getTenantId();
		this.description = processDefinition.getDescription();
	}

	public ProcessDefinitionDto()
	{

	}
}
