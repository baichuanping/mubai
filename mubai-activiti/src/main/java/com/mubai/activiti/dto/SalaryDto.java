package com.mubai.activiti.dto;

import com.mubai.activiti.domain.Salary;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

/**
 * 调薪流程Dto
 * @author baichuanping
 * @create 2018-12-29
 */
@Setter
@Getter
public class SalaryDto extends Salary {
	private String taskId;
	private String taskComment;
	private String taskPass;
	private Map<String,Object> vars;
}
