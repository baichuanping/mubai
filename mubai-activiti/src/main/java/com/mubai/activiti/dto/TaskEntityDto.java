package com.mubai.activiti.dto;

import com.mubai.common.base.BaseActDto;
import lombok.Getter;
import lombok.Setter;
import org.activiti.engine.task.Task;

import java.util.Date;
import java.util.List;

/**
 * 任务实体数据对象
 * @author baichuanping
 * @create 2018-12-28
 */
@Setter
@Getter
public class TaskEntityDto extends BaseActDto {
	public static final long serialVersionUID = 1L;

	/**
	 * 任务编号
	 */
	private String id;
	/**
	 * 任务名称
	 */
	private String name;

	private Boolean active;

	private String key;

	private String description;

	private String formKey;

	/**
	 * 任务的办理人
	 */
	private String assignee;

	private String processId;

	private String processDefinitionId;

	private String executionId;

	private Integer priority;

	/**
	 * 任务的拥有人
	 */
	private String owner;

	private Boolean unassigned;

	/**
	 * 委派状态
	 */
	private String delegationState;

	/**
	 * 任务的候选人
	 */
	private String candidateUser;

	/**
	 * 任务的候选组
	 */
	private String candidateGroup;

	private List<String> candidateGroupIn;

	/**
	 * 受邀人
	 */
	private String involvedUser;

	private String processInstanceId;

	private String processInstanceBusinessKey;

	private List<String> processInstanceIdIn;

	private String processDefinitionKey;

	private Date createdOn;
	private Date createdBefore;
	private Date createdAfter;

	private String taskDefinitionKey;

	public TaskEntityDto() {

	}

	public TaskEntityDto(Task task) {
		this.setId(task.getId());
		this.setKey(task.getTaskDefinitionKey());
		this.setName(task.getName());
		this.setDescription(task.getDescription());
		this.setAssignee(task.getAssignee());
		this.setFormKey(task.getFormKey());
		this.setProcessId(task.getProcessInstanceId());
		this.setProcessDefinitionId(task.getProcessDefinitionId());
		this.setExecutionId(task.getExecutionId());
	}
}
