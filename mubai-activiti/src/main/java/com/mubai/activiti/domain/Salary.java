package com.mubai.activiti.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.mubai.common.base.BaseEntity;
import java.util.Date;

/**
 * 薪水调整表 oa_salary
 * 
 * @author baichuanping
 * @date 2018-12-29
 */
public class Salary extends BaseEntity
{
	private static final long serialVersionUID = 1L;
	
	/** 薪水id */
	private Integer salaryId;
	/** 流程编号 */
	private String processId;
	/** 变动用户 */
	private String userId;
	/** 变动用户名 */
	private String userName;
	/** 归属部门Id */
	private String deptId;
	/** 归属部门 */
	private String deptName;
	/** 岗位 */
	private String post;
	/** 用户性别（0男 1女 2未知） */
	private String sex;
	/** 学历 */
	private String edu;
	/** 调整原因 */
	private String content;
	/** 现行标准 薪酬档级 */
	private String olda;
	/** 现行标准 月工资额 */
	private String oldb;
	/** 现行标准 年薪总额 */
	private String oldc;
	/** 调整后标准 薪酬档级 */
	private String newa;
	/** 调整后标准 月工资额 */
	private String newb;
	/** 调整后标准 年薪总额 */
	private String newc;
	/** 月增资 */
	private String addNum;
	/** 执行时间 */
	private Date exeDate;
	/** 人力资源部门意见 */
	private String hrText;
	/** 分管领导意见 */
	private String leadText;
	/** 集团主要领导意见 */
	private String mainLeadText;

	public void setSalaryId(Integer salaryId) 
	{
		this.salaryId = salaryId;
	}

	public Integer getSalaryId() 
	{
		return salaryId;
	}
	public void setProcessId(String processId) 
	{
		this.processId = processId;
	}

	public String getProcessId() 
	{
		return processId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setPost(String post)
	{
		this.post = post;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getPost()
	{
		return post;
	}
	public void setSex(String sex) 
	{
		this.sex = sex;
	}

	public String getSex() 
	{
		return sex;
	}
	public void setEdu(String edu) 
	{
		this.edu = edu;
	}

	public String getEdu() 
	{
		return edu;
	}
	public void setContent(String content) 
	{
		this.content = content;
	}

	public String getContent() 
	{
		return content;
	}
	public void setOlda(String olda) 
	{
		this.olda = olda;
	}

	public String getOlda() 
	{
		return olda;
	}
	public void setOldb(String oldb) 
	{
		this.oldb = oldb;
	}

	public String getOldb() 
	{
		return oldb;
	}
	public void setOldc(String oldc) 
	{
		this.oldc = oldc;
	}

	public String getOldc() 
	{
		return oldc;
	}
	public void setNewa(String newa) 
	{
		this.newa = newa;
	}

	public String getNewa() 
	{
		return newa;
	}
	public void setNewb(String newb) 
	{
		this.newb = newb;
	}

	public String getNewb() 
	{
		return newb;
	}
	public void setNewc(String newc) 
	{
		this.newc = newc;
	}

	public String getNewc() 
	{
		return newc;
	}
	public void setAddNum(String addNum) 
	{
		this.addNum = addNum;
	}

	public String getAddNum() 
	{
		return addNum;
	}
	public void setExeDate(Date exeDate) 
	{
		this.exeDate = exeDate;
	}

	public Date getExeDate() 
	{
		return exeDate;
	}
	public void setHrText(String hrText) 
	{
		this.hrText = hrText;
	}

	public String getHrText() 
	{
		return hrText;
	}
	public void setLeadText(String leadText) 
	{
		this.leadText = leadText;
	}

	public String getLeadText() 
	{
		return leadText;
	}
	public void setMainLeadText(String mainLeadText) 
	{
		this.mainLeadText = mainLeadText;
	}

	public String getMainLeadText() 
	{
		return mainLeadText;
	}
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("salaryId", getSalaryId())
            .append("processId", getProcessId())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("deptId", getDeptId())
            .append("deptName", getDeptName())
            .append("post", getPost())
            .append("sex", getSex())
            .append("edu", getEdu())
            .append("content", getContent())
            .append("olda", getOlda())
            .append("oldb", getOldb())
            .append("oldc", getOldc())
            .append("newa", getNewa())
            .append("newb", getNewb())
            .append("newc", getNewc())
            .append("addNum", getAddNum())
            .append("exeDate", getExeDate())
            .append("hrText", getHrText())
            .append("leadText", getLeadText())
            .append("mainLeadText", getMainLeadText())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
