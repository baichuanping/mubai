package com.mubai.activiti.mapper;

import com.mubai.activiti.domain.Salary;
import java.util.List;	

/**
 * 薪水调整 数据层
 * 
 * @author baichuanping
 * @date 2018-12-29
 */
public interface SalaryMapper 
{
	/**
     * 查询薪水调整信息
     * 
     * @param salaryId 薪水调整ID
     * @return 薪水调整信息
     */
	public Salary selectSalaryById(Integer salaryId);
	
	/**
     * 查询薪水调整列表
     * 
     * @param salary 薪水调整信息
     * @return 薪水调整集合
     */
	public List<Salary> selectSalaryList(Salary salary);
	
	/**
     * 新增薪水调整
     * 
     * @param salary 薪水调整信息
     * @return 结果
     */
	public int insertSalary(Salary salary);
	
	/**
     * 修改薪水调整
     * 
     * @param salary 薪水调整信息
     * @return 结果
     */
	public int updateSalary(Salary salary);
	
	/**
     * 删除薪水调整
     * 
     * @param salaryId 薪水调整ID
     * @return 结果
     */
	public int deleteSalaryById(Integer salaryId);
	
	/**
     * 批量删除薪水调整
     * 
     * @param salaryIds 需要删除的数据ID
     * @return 结果
     */
	public int deleteSalaryByIds(String[] salaryIds);
	
}