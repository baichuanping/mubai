package com.mubai.activiti.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mubai.activiti.config.ActivitiConstant;
import com.mubai.activiti.dto.SalaryDto;
import com.mubai.activiti.dto.TaskEntityDto;
import com.mubai.activiti.service.ActTaskService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.activiti.mapper.SalaryMapper;
import com.mubai.activiti.domain.Salary;
import com.mubai.activiti.service.SalaryService;
import com.mubai.common.support.Convert;

/**
 * 薪水调整 服务层实现
 *
 * @author baichuanping
 * @date 2018-12-29
 */
@Service
public class SalaryServiceImpl implements SalaryService
{
	@Autowired
	private SalaryMapper salaryMapper;

	@Autowired
	private ActTaskService actTaskService;

	@Autowired
	private RuntimeService runtimeService;
	/**
	 * 查询薪水调整信息
	 *
	 * @param salaryId 薪水调整ID
	 * @return 薪水调整信息
	 */
	@Override
	public Salary selectSalaryById(Integer salaryId)
	{
		return salaryMapper.selectSalaryById(salaryId);
	}

	/**
	 * 查询薪水调整列表
	 *
	 * @param salary 薪水调整信息
	 * @return 薪水调整集合
	 */
	@Override
	public List<Salary> selectSalaryList(Salary salary)
	{
		return salaryMapper.selectSalaryList(salary);
	}

	/**
	 * 新增薪水调整
	 *
	 * @param salary 薪水调整信息
	 * @return 结果
	 */
	@Override
	public int insertSalary(Salary salary)
	{
		return salaryMapper.insertSalary(salary);
	}

	/**
	 * 修改薪水调整
	 *
	 * @param salary 薪水调整信息
	 * @return 结果
	 */
	@Override
	public int updateSalary(Salary salary)
	{
		return salaryMapper.updateSalary(salary);
	}

	/**
	 * 删除薪水调整对象
	 *
	 * @param ids 需要删除的数据ID
	 * @return 结果
	 */
	@Override
	public int deleteSalaryByIds(String ids)
	{
		return salaryMapper.deleteSalaryByIds(Convert.toStrArray(ids));
	}

	/**
	 * 根据taskId查询薪水调整信息
	 *
	 * @param taskId
	 * @return
		*/
	@Override
	public Salary selectSalaryByTaskId(String taskId) {
		TaskEntityDto taskEntityDto = actTaskService.selectOneTask(taskId);
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(taskEntityDto.getProcessId()).singleResult();
		Salary salary = salaryMapper.selectSalaryById(Integer.valueOf(processInstance.getBusinessKey()));
		return salary;
	}
	/**
	 * 保存一个业务流程
	 *
	 * @param userId
	 * @param salary
	 * @return
	 */
	@Override
	public int saveSalary(Long userId, Salary salary) {
		int r=salaryMapper.insertSalary(salary);
		actTaskService.startProcess(String.valueOf(userId),ActivitiConstant.ACTIVITI_SALARY[0],ActivitiConstant.ACTIVITI_SALARY[1],String.valueOf(salary.getSalaryId()),salary.getContent(),new HashMap<>());
		return r;
	}

	/**
	 * 修改调薪流程
	 *
	 * @param salaryDto
	 */
	@Override
	public int updateSalaryInfo(SalaryDto salaryDto) {
		String taskDefinitionKey = actTaskService.selectOneTask(salaryDto.getTaskId()).getTaskDefinitionKey();
		String taskComment = salaryDto.getTaskComment();
		switch (taskDefinitionKey) {
			case "audit2":
				salaryDto.setHrText(taskComment);
				break;
			case "audit3":
				salaryDto.setLeadText(taskComment);
				break;
			case "audit4":
				salaryDto.setMainLeadText(taskComment);
				break;
			case "apply_end":
				// 流程完成，兑现
				break;
		}
		Map<String,Object> vars = new HashMap<>();
		vars.put("pass", salaryDto.getTaskPass());
		vars.put("title","");
		actTaskService.completeTask(salaryDto.getTaskId(),vars);
		return this.updateSalary(salaryDto);
	}
}