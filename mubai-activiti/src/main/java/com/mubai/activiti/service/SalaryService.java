package com.mubai.activiti.service;

import com.mubai.activiti.domain.Salary;
import com.mubai.activiti.dto.SalaryDto;

import java.util.List;

/**
 * 薪水调整 服务层
 * 
 * @author baichuanping
 * @date 2018-12-29
 */
public interface SalaryService
{
	/**
     * 查询薪水调整信息
     * 
     * @param salaryId 薪水调整ID
     * @return 薪水调整信息
     */
	public Salary selectSalaryById(Integer salaryId);
	
	/**
     * 查询薪水调整列表
     * 
     * @param salary 薪水调整信息
     * @return 薪水调整集合
     */
	public List<Salary> selectSalaryList(Salary salary);
	
	/**
     * 新增薪水调整
     * 
     * @param salary 薪水调整信息
     * @return 结果
     */
	public int insertSalary(Salary salary);
	
	/**
     * 修改薪水调整
     * 
     * @param salary 薪水调整信息
     * @return 结果
     */
	public int updateSalary(Salary salary);
		
	/**
     * 删除薪水调整信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
	public int deleteSalaryByIds(String ids);

	/**
	 * 根据taskId查询薪水调整信息
	 * @param taskId
	 * @return
	 */
	public Salary selectSalaryByTaskId(String taskId);

	/**
	 * 保存一个调薪流程
	 * @param userId
	 * @param salary
	 * @return
	 */
	public int saveSalary(Long userId,Salary salary);

	/**
	 * 修改调薪流程
	 * @param salaryDto
	 */
	public int updateSalaryInfo(SalaryDto salaryDto);
}
