package com.mubai.activiti.service.impl;

import com.mubai.activiti.service.ActIdentityService;
import org.activiti.engine.IdentityService;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 身份角色
 * @author baichuanping
 * @create 2018-12-12
 */
@Service
public class ActIdentityServiceImpl implements ActIdentityService {

	@Autowired
	private IdentityService identityService;

	@Override
	public List<User> selectUserList() {
		return null;
	}

	@Override
	public List<Group> selectGroupList() {
		return null;
	}
}
