package com.mubai.activiti.service;

import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.User;

import java.util.List;

/**
 * @author baichuanping
 * @create 2018-12-12
 */
public interface ActIdentityService {
	List<User> selectUserList();
	List<Group> selectGroupList();
}
