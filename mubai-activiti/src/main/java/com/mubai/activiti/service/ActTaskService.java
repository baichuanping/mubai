package com.mubai.activiti.service;

import com.mubai.activiti.dto.TaskEntityDto;
import com.mubai.common.page.TableDataInfo;

import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * @author baichuanping
 * @create 2018-12-12
 */
public interface ActTaskService {

	/**
	 * 查询任务列表
	 * @param taskEntityDto
	 * @return
	 */
	public TableDataInfo selectTaskEntityList(TaskEntityDto taskEntityDto);

	/**
	 * 根据taskId获取任务
	 * @param taskId
	 * @return
	 */
	public TaskEntityDto selectOneTask(String taskId);

	/**
	 * 修改任务
	 * @param taskId
	 * @param variables
	 */
	public void completeTask(String taskId, Map<String, Object> variables);

	/**
	 * 代办任务
	 * @param taskEntityDto
	 * @return
	 */
	public TableDataInfo selectTodoTask(TaskEntityDto taskEntityDto);

	/**
	 * 受邀任务
	 * @param taskEntityDto
	 * @return
	 */
	public TableDataInfo  selectInvolvedTask(TaskEntityDto taskEntityDto);
	/**
	 * 归档任务
	 * @param taskEntityDto
	 * @return
	 */
	public TableDataInfo selectArchivedTask(TaskEntityDto taskEntityDto);

	/**
	 * 查询完成的任务
	 * @param taskEntityDto
	 * @return
	 */
	public TableDataInfo selectFinishedTask(TaskEntityDto taskEntityDto);

	/**
	 * 获取流程XML上的表单KEY
	 * @param processId
	 * @param taskDefKey
	 * @return
	 */
	public String getFormKey(String processId, String taskDefKey);

	/**
	 * 读取带跟踪的图片
	 * @param processDefinitionId
	 * @param executionId
	 * @return
	 */
	public InputStream traceTaskPhoto(String processDefinitionId, String executionId);

	/**
	 *  key 组名  value 代办任务
	 * @param taskEntityDto
	 * @return
	 */
	public Map<String, List<TaskEntityDto>> selectGroupQueueTask(TaskEntityDto taskEntityDto);

	/**
	 * 提交任务, 并保存意见
	 *
	 * @param taskId    任务ID
	 * @param procInsId 流程实例ID，如果为空，则不保存任务提交意见
	 * @param comment   任务提交意见的内容
	 * @param title     流程标题，显示在待办任务标题
	 * @param vars      任务变量
	 */
	public void complete(String taskId, String procInsId, String comment, String title, Map<String, Object> vars);

	/**
	 * 启动流程
	 *
	 * @param procDefKey    流程定义KEY
	 * @param businessTable 业务表表名
	 * @param businessId    业务表编号
	 * @param title         流程标题，显示在待办任务标题
	 * @param vars          流程变量
	 * @return 流程实例ID
	 */
	public String startProcess(String procDefKey, String businessTable, String businessId, String title, String userId, Map<String, Object> vars);


}
