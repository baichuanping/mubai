package com.mubai.activiti.service.impl;

import com.mubai.activiti.dto.TaskEntityDto;
import com.mubai.activiti.service.ActTaskService;
import com.mubai.common.enums.CODE;
import com.mubai.common.page.TableDataInfo;
import com.mubai.common.utils.StringUtils;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.identity.Group;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.DelegationState;
import org.activiti.engine.task.TaskQuery;
import org.activiti.image.ProcessDiagramGenerator;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author baichuanping
 * @create 2018-12-12
 */
@Service
public class ActTaskServiceImpl implements ActTaskService {

	@Autowired
	TaskService taskService;
	@Autowired
	IdentityService identityService;
	@Autowired
	RuntimeService runtimeService;

	@Autowired
	FormService formService;
	@Autowired
	RepositoryService repositoryService;

	@Autowired
	private ProcessEngineFactoryBean processEngineFactory;

	@Autowired
	private ProcessEngine processEngine;

	@Autowired
	private HistoryService historyService;

	/**
	 * 查询任务列表
	 * @param taskEntityDto
	 * @return
	 */
	@Override
	public TableDataInfo selectTaskEntityList(TaskEntityDto taskEntityDto) {
		TableDataInfo data = new TableDataInfo();
		TaskQuery taskQuery = taskService.createTaskQuery();
		if (StringUtils.isNotEmpty(taskEntityDto.getAssignee())) {
			taskQuery.taskAssignee(taskEntityDto.getAssignee());
		}
		if (StringUtils.isNotEmpty(taskEntityDto.getName())) {
			taskQuery.taskName(taskEntityDto.getName());
		}
		if (StringUtils.isNotEmpty(taskEntityDto.getOwner())) {
			taskQuery.taskOwner(taskEntityDto.getOwner());
		}
//		if (StringUtils.isNotEmpty(taskEntityDto.getUnassigned())) {
//			taskQuery.taskUnassigned();
//		}
		if (StringUtils.isNotEmpty(taskEntityDto.getDelegationState())) {
			DelegationState state = getDelegationState(taskEntityDto.getDelegationState());
			if (state != null) {
				taskQuery.taskDelegationState(state);
			}
		}
		if (StringUtils.isNotEmpty(taskEntityDto.getTaskDefinitionKey())) {
			taskQuery.taskDefinitionKey(taskEntityDto.getTaskDefinitionKey());
		}
//		if (StringUtils.isNotEmpty(taskEntityDto.getActive())) {
//			if (taskEntityDto.getActive().booleanValue()) {
//				taskQuery.active();
//			} else {
//				taskQuery.suspended();
//			}
//		}
		data.setCode(CODE.success.code);
		data.setTotal(taskQuery.count());
		data.setRows(taskQuery.orderByTaskId().desc().listPage(taskEntityDto.getPageNum(),taskEntityDto.getPageSize())
		.stream().map(TaskEntityDto::new).collect(Collectors.toList()));
		return data;
	}

	/**
	 * 根据taskId获取任务
	 * @param taskId
	 * @return
	 */
	@Override
	public TaskEntityDto selectOneTask(String taskId) {
		return new TaskEntityDto(taskService.createTaskQuery().taskId(taskId).singleResult());
	}

	/**
	 * 修改任务
	 * @param taskId
	 * @param variables
	 */
	@Override
	public void completeTask(String taskId, Map<String, Object> variables) {
		if (StringUtils.isNotEmpty(variables)) {
			taskService.complete(taskId, variables);
		}else{
			taskService.complete(taskId);
		}

	}

	/**
	 * 代办任务
	 * @param taskEntityDto
	 * @return
	 */
	@Override
	public TableDataInfo selectTodoTask(TaskEntityDto taskEntityDto) {
		TableDataInfo data = new TableDataInfo();
		TaskQuery taskQuery = taskService.createTaskQuery();
		data.setTotal(taskQuery.count());
		data.setRows(taskQuery.taskAssignee(taskEntityDto.getAssignee())
				.active()
				.orderByTaskId()
				.desc()
				.listPage(taskEntityDto.getPageNum(), taskEntityDto.getPageSize())
				.stream()
				.map(TaskEntityDto::new)
				.collect(Collectors.toList()));
		return data;
	}

	/**
	 * 受邀任务
	 * @param taskEntityDto
	 * @return
	 */
	@Override
	public TableDataInfo selectInvolvedTask(TaskEntityDto taskEntityDto) {
		TableDataInfo data = new TableDataInfo();
		TaskQuery taskQuery = taskService.createTaskQuery();
		data.setCode(CODE.success.code);
		data.setTotal(taskQuery.count());
		data.setRows(taskQuery.taskInvolvedUser(taskEntityDto.getInvolvedUser())
				.active()
				.orderByTaskId()
				.desc()
				.listPage(taskEntityDto.getPageNum(), taskEntityDto.getPageSize())
				.stream()
				.map(TaskEntityDto::new)
				.collect(Collectors.toList()));
		return data;
	}

	/**
	 * 归档任务
	 * @param taskEntityDto
	 * @return
	 */
	@Override
	public TableDataInfo selectArchivedTask(TaskEntityDto taskEntityDto) {
		TableDataInfo data = new TableDataInfo();
		List<TaskEntityDto> taskVOS = new ArrayList<TaskEntityDto>();
		HistoricTaskInstanceQuery instanceQuery = historyService.createHistoricTaskInstanceQuery();
		instanceQuery.taskOwner(taskEntityDto.getOwner()).finished();
		List<HistoricTaskInstance> historicTaskInstances = instanceQuery
				.listPage(taskEntityDto.getPageNum(), taskEntityDto.getPageSize());
		for (HistoricTaskInstance instance : historicTaskInstances) {
			TaskEntityDto task = new TaskEntityDto();
			task.setId(instance.getId());
			task.setName(instance.getName());
			task.setProcessInstanceId(instance.getProcessInstanceId());
			task.setTaskDefinitionKey(instance.getTaskDefinitionKey());
			task.setProcessDefinitionId(instance.getProcessDefinitionId());
			task.setProcessId(instance.getProcessInstanceId());
			task.setFormKey(instance.getFormKey());
			task.setKey(instance.getTaskDefinitionKey());
			task.setAssignee(instance.getAssignee());

			taskVOS.add(task);
		}
		data.setCode(CODE.success.code);
		data.setTotal(instanceQuery.count());
		data.setRows(taskVOS);
		return data;
	}

	/**
	 * 查询完成的任务
	 * @param taskEntityDto
	 * @return
	 */
	@Override
	public TableDataInfo selectFinishedTask(TaskEntityDto taskEntityDto) {
		TableDataInfo data = new TableDataInfo();
		List<TaskEntityDto> taskVOS = new ArrayList<TaskEntityDto>();
		HistoricTaskInstanceQuery taskInstanceQuery = historyService.createHistoricTaskInstanceQuery();
		taskInstanceQuery.taskOwner(taskEntityDto.getOwner()).finished();
		List<HistoricTaskInstance> historicTaskInstances = taskInstanceQuery
				.listPage(taskEntityDto.getPageNum(), taskEntityDto.getPageSize());
		for (HistoricTaskInstance instance : historicTaskInstances) {
			TaskEntityDto entityDto = new TaskEntityDto();
			entityDto.setId(instance.getId());
			entityDto.setName(instance.getName());
			entityDto.setProcessInstanceId(instance.getProcessInstanceId());
			entityDto.setTaskDefinitionKey(instance.getTaskDefinitionKey());
			entityDto.setProcessDefinitionId(instance.getProcessDefinitionId());
			entityDto.setProcessId(instance.getProcessInstanceId());
			entityDto.setFormKey(instance.getFormKey());
			entityDto.setKey(instance.getTaskDefinitionKey());
			entityDto.setAssignee(instance.getAssignee());
			taskVOS.add(entityDto);
		}
		data.setCode(CODE.success.code);
		data.setTotal(taskInstanceQuery.count());
		data.setRows(taskVOS);
		return data;
	}

	/**
	 * 获取流程表单（首先获取任务节点表单KEY，如果没有则取流程开始节点表单KEY）
	 * @param processId
	 * @param taskDefKey
	 * @return
	 */
	@Override
	public String getFormKey(String processId, String taskDefKey) {
		String formKey = "";
		if (StringUtils.isNotBlank(processId)) {
			if (StringUtils.isNotBlank(taskDefKey)) {
				try {
					formKey = formService.getTaskFormKey(processId, taskDefKey);
				} catch (Exception e) {
					formKey = "";
				}
			}
			if (StringUtils.isBlank(formKey)) {
				formKey = formService.getStartFormKey(processId);
			}
			if (StringUtils.isBlank(formKey)) {
				formKey = "/404";
			}
		}
		return formKey;
	}
	/**
	 * 读取带跟踪的图片
	 * @param processDefinitionId
	 * @param pProcessInstanceId
	 * @return
	 */
	@Override
	public InputStream traceTaskPhoto(String processDefinitionId, String pProcessInstanceId) {

		//  获取历史流程实例
		HistoricProcessInstance historicProcessInstance = historyService.createHistoricProcessInstanceQuery()
				.processInstanceId(pProcessInstanceId).singleResult();

		if (historicProcessInstance != null) {
			// 获取流程定义
			ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
					.getDeployedProcessDefinition(historicProcessInstance.getProcessDefinitionId());

			// 获取流程历史中已执行节点，并按照节点在流程中执行先后顺序排序
			List<HistoricActivityInstance> historicActivityInstanceList = historyService.createHistoricActivityInstanceQuery()
					.processInstanceId(pProcessInstanceId).orderByHistoricActivityInstanceId().asc().list();

			// 已执行的节点ID集合
			List<String> executedActivityIdList = new ArrayList<String>();
			int index = 1;
			//获取已经执行的节点ID
			for (HistoricActivityInstance activityInstance : historicActivityInstanceList) {
				executedActivityIdList.add(activityInstance.getActivityId());
				index++;
			}

			// 已执行的线集合
			List<String> flowIds = new ArrayList<String>();
			// 获取流程走过的线
			flowIds = getHighLightedFlows(processDefinition, historicActivityInstanceList);

			BpmnModel bpmnModel = repositoryService
					.getBpmnModel(historicProcessInstance.getProcessDefinitionId());
			// 获取流程图图像字符流
			ProcessDiagramGenerator pec = processEngine.getProcessEngineConfiguration().getProcessDiagramGenerator();
			//配置字体

			InputStream imageStream = pec.generateDiagram(bpmnModel, "png", executedActivityIdList, flowIds, "宋体", "微软雅黑", "黑体", null, 2.0);
			return imageStream;
		}
		return null;
	}


	/**
	 * 获取需要高亮的线
	 *
	 * @param processDefinitionEntity
	 * @param historicActivityInstances
	 * @return
	 */
	private List<String> getHighLightedFlows(
			ProcessDefinitionEntity processDefinitionEntity,
			List<HistoricActivityInstance> historicActivityInstances) {
		List<String> highFlows = new ArrayList<String>();// 用以保存高亮的线flowId
		for (int i = 0; i < historicActivityInstances.size() - 1; i++) {// 对历史流程节点进行遍历
			ActivityImpl activityImpl = processDefinitionEntity
					.findActivity(historicActivityInstances.get(i)
							.getActivityId());// 得到节点定义的详细信息
			List<ActivityImpl> sameStartTimeNodes = new ArrayList<ActivityImpl>();// 用以保存后需开始时间相同的节点
			ActivityImpl sameActivityImpl1 = processDefinitionEntity
					.findActivity(historicActivityInstances.get(i + 1)
							.getActivityId());
			// 将后面第一个节点放在时间相同节点的集合里
			sameStartTimeNodes.add(sameActivityImpl1);
			for (int j = i + 1; j < historicActivityInstances.size() - 1; j++) {
				HistoricActivityInstance activityImpl1 = historicActivityInstances
						.get(j);// 后续第一个节点
				HistoricActivityInstance activityImpl2 = historicActivityInstances
						.get(j + 1);// 后续第二个节点
				if (activityImpl1.getStartTime().equals(
						activityImpl2.getStartTime())) {
					// 如果第一个节点和第二个节点开始时间相同保存
					ActivityImpl sameActivityImpl2 = processDefinitionEntity
							.findActivity(activityImpl2.getActivityId());
					sameStartTimeNodes.add(sameActivityImpl2);
				} else {
					// 有不相同跳出循环
					break;
				}
			}
			List<PvmTransition> pvmTransitions = activityImpl
					.getOutgoingTransitions();// 取出节点的所有出去的线
			for (PvmTransition pvmTransition : pvmTransitions) {
				// 对所有的线进行遍历
				ActivityImpl pvmActivityImpl = (ActivityImpl) pvmTransition
						.getDestination();
				// 如果取出的线的目标节点存在时间相同的节点里，保存该线的id，进行高亮显示
				if (sameStartTimeNodes.contains(pvmActivityImpl)) {
					highFlows.add(pvmTransition.getId());
				}
			}
		}
		return highFlows;
	}

	protected DelegationState getDelegationState(String delegationState) {
		DelegationState state = null;
		if (delegationState != null) {
			if (DelegationState.RESOLVED.name().toLowerCase().equals(delegationState)) {
				return DelegationState.RESOLVED;
			} else if (DelegationState.PENDING.name().toLowerCase().equals(delegationState)) {
				return DelegationState.PENDING;
			} else {
				throw new ActivitiIllegalArgumentException("Illegal value for delegationState: " + delegationState);
			}
		}
		return state;
	}

	/**
	 *  key 组名  value 代办任务
	 * @param taskEntityDto
	 * @return
	 */
	@Override
	public Map<String, List<TaskEntityDto>> selectGroupQueueTask(TaskEntityDto taskEntityDto) {
		List<String> groups = identityService.createGroupQuery().list().stream().map(Group::getId).collect(Collectors.toList());
		TaskQuery taskQuery = taskService.createTaskQuery();
		HashMap<String, List<TaskEntityDto>> map = new HashMap<>();
		for (String group : groups) {
			map.put(group,taskQuery.taskCandidateGroup(group).taskUnassigned().orderByTaskId().asc()
					.list()
					.stream()
					.map(TaskEntityDto::new)
					.collect(Collectors.toList()));
		}
		return map;
	}
	/**
	 * 提交任务, 并保存意见
	 *
	 * @param taskId    任务ID
	 * @param procInsId 流程实例ID，如果为空，则不保存任务提交意见
	 * @param comment   任务提交意见的内容
	 * @param title     流程标题，显示在待办任务标题
	 * @param vars      任务变量
	 */
	@Override
	public void complete(String taskId, String procInsId, String comment, String title, Map<String, Object> vars) {
		// 添加意见
		if (StringUtils.isNotBlank(procInsId) && StringUtils.isNotBlank(comment)) {
			taskService.addComment(taskId, procInsId, comment);
		}
		// 设置流程变量
		if (vars == null) {
			vars = new HashMap<>();
		}
		// 设置流程标题
		if (StringUtils.isNotBlank(title)) {
			vars.put("title", title);
		}
		// 提交任务
		taskService.complete(taskId, vars);
	}

	/**
	 * 启动流程
	 *
	 * @param userId		用户Id
	 * @param procDefKey    流程定义KEY
	 * @param businessTable 业务表表名
	 * @param businessId    业务表编号
	 * @param title         流程标题，显示在待办任务标题
	 * @param vars          流程变量
	 * @return 流程实例ID
	 */
	@Override
	public String startProcess(String userId,String procDefKey, String businessTable, String businessId, String title, Map<String, Object> vars) {
		//String userId = ShiroUtils.getUser().getUsername();//ObjectUtils.toString(UserUtils.getUser().getId())
		// 用来设置启动流程的人员ID，引擎会自动把用户ID保存到activiti:initiator中
		identityService.setAuthenticatedUserId(userId);
		// 设置流程变量
		if (vars == null) {
			vars = new HashMap();
		}
		// 设置流程标题
		if (StringUtils.isNotBlank(title)) {
			vars.put("title", title);
		}
		// 启动流程
		ProcessInstance procIns = runtimeService.startProcessInstanceByKey(procDefKey, businessId, vars);
		return null;
	}

}
