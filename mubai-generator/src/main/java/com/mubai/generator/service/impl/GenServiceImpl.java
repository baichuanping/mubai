package com.mubai.generator.service.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mubai.common.config.Global;
import com.mubai.common.constant.Constants;
import com.mubai.common.utils.StringUtils;
import com.mubai.generator.domain.ColumnInfo;
import com.mubai.generator.domain.TableInfo;
import com.mubai.generator.mapper.GenMapper;
import com.mubai.generator.service.GenService;
import com.mubai.generator.util.GenUtils;
import com.mubai.generator.util.VelocityInitializer;

/**
 * 代码生成 服务层处理
 * 
 * @author baichuanping
 */
@Service
public class GenServiceImpl implements GenService
{
    private static final Logger log = LoggerFactory.getLogger(GenServiceImpl.class);

    @Autowired
    private GenMapper genMapper;

    /**
     * 查询ry数据库表信息
     * 
     * @param tableInfo 表信息
     * @return 数据库表列表
     */
    @Override
    public List<TableInfo> selectTableList(TableInfo tableInfo)
    {
        return genMapper.selectTableList(tableInfo);
    }

    /**
     * 生成代码
     * 
     * @param tableName 表名称
     * @return 数据
     */
    @Override
    public byte[] generatorCode(String tableName) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        // 查询表信息
        TableInfo table = genMapper.selectTableByName(tableName);
        // 查询列信息
        List<ColumnInfo> columns = genMapper.selectTableColumnsByName(tableName);
        // 生成代码
        GenUtils.generatorCode(table, columns, zip);
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 批量生成代码
     * 
     * @param tableNames 表数组
     * @return 数据
     */
    @Override
    public byte[] generatorCode(String[] tableNames)
    {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        for (String tableName : tableNames)
        {
            // 查询表信息
            TableInfo table = genMapper.selectTableByName(tableName);
            // 查询列信息
            List<ColumnInfo> columns = genMapper.selectTableColumnsByName(tableName);
            // 生成代码
            GenUtils.generatorCode(table, columns, zip);
        }
        IOUtils.closeQuietly(zip);
        return outputStream.toByteArray();
    }

    /**
     * 把参数保存到yaml文件中
     *
     * @param map
     */
    @Override
    public void modifySave(Map<String, Object> map) {
        String autoRemovePre = (String) map.get("autoRemovePre");
        String tablePrefix = (String) map.get("tablePrefix");
        Global.setConfig("gen.author",(String) map.get("author"));
        Global.setConfig("gen.email",(String) map.get("email"));
        Global.setConfig("gen.packageName",(String) map.get("package"));
        Global.setConfig("gen.autoRemovePre",(String) map.get("autoRemovePre"));
        Global.setConfig("gen.tablePrefix",(String) map.get("tablePrefix"));
    }
}
