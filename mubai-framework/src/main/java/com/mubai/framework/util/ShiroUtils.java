package com.mubai.framework.util;

import com.mubai.common.config.Global;
import com.mubai.common.tool.ToolJackson;
import com.mubai.common.utils.StringUtils;
import com.mubai.common.utils.bean.BeanUtils;
import com.mubai.framework.shiro.realm.UserRealm;
import com.mubai.system.domain.SysUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.mgt.RealmSecurityManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * shiro 工具类
 * 
 * @author baichuanping
 */
public class ShiroUtils
{
    public static Subject getSubject()
    {
        return SecurityUtils.getSubject();
    }

    public static Session getSession()
    {
        return SecurityUtils.getSubject().getSession();
    }

    public static void logout()
    {
        getSubject().logout();
    }

    public static SysUser getSysUser()
    {
        SysUser user = null;
        Object obj = getSession().getAttribute("sysUser");
        if (StringUtils.isNotNull(obj))
        {
            user = new SysUser();
            BeanUtils.copyBeanProp(user, obj);
        }
        return user;
    }

    public static void setSysUser(SysUser user)
    {
        ShiroUtils.getSession().setAttribute("sysUser", user);
    }

    public static void clearCachedAuthorizationInfo()
    {
        RealmSecurityManager rsm = (RealmSecurityManager) SecurityUtils.getSecurityManager();
        UserRealm realm = (UserRealm) rsm.getRealms().iterator().next();
        realm.clearCachedAuthorizationInfo();
    }

    public static Long getUserId()
    {
        return getSysUser().getUserId().longValue();
    }

    public static String getLoginName()
    {
        return getSysUser().getLoginName();
    }

    public static String getIp()
    {
        return getSubject().getSession().getHost();
    }

    public static String getSessionId()
    {
        return String.valueOf(getSubject().getSession().getId());
    }

    /**
     * 生成随机盐
     */
    public static String randomSalt()
    {
        // 一个Byte占两个字节，此处生成的3字节，字符串长度为6
        SecureRandomNumberGenerator secureRandom = new SecureRandomNumberGenerator();
        String hex = secureRandom.nextBytes(3).toHex();
        return hex;
    }

    /**
     * JWT 的用户信息
     * @return
     */
    public static SysUser getSysUserByJwtToken(String token)
    {
        SysUser user = null;
        Object obj = EHCacheUtil.getValue("jwt",token);
        if (StringUtils.isNotNull(obj))
        {
            user = new SysUser();
            BeanUtils.copyBeanProp(user, obj);
        }
        return user;
    }

    /**
     * jwt登录成功后返回给前端的用户信息
     */
    public static Map<String,Object> getJwtUserInfo() {
        SysUser sysUser = getSysUser();
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("loginName",sysUser.getLoginName());
        map.put("userName",sysUser.getUserName());
        map.put("sex",sysUser.getSex());
        map.put("phonenumber",sysUser.getPhonenumber());
//        map.put("avatarUrl",Global.getAvatarPath()+sysUser.getAvatar());
        map.put("email",sysUser.getEmail());
        return map;
    }

    /**
     * jwt登录成功后返回给前端的用户信息token
     * @return
     */
    public static String getJwtUserInfoToken() {
        try {
            SysUser sysUser = getSysUser();
            sysUser.setPassword(null);//安全一点
            return JwtUtils.createToken(ToolJackson.toJson(sysUser), Global.getJwtOutTime());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
