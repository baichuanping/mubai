package com.mubai.framework.manager.factory;

import java.util.List;
import java.util.TimerTask;
import com.mubai.common.constant.Constants;
import com.mubai.common.tool.ToolAddress;
import com.mubai.common.utils.ServletUtils;
import com.mubai.common.utils.spring.SpringUtils;
import com.mubai.framework.shiro.session.OnlineSession;
import com.mubai.framework.shiro.session.SessionDao;
import com.mubai.framework.util.*;
import com.mubai.system.domain.SysLogininfor;
import com.mubai.system.domain.SysOperLog;
import com.mubai.system.domain.SysUserOnline;
import com.mubai.system.service.SysOperLogService;
import com.mubai.system.service.impl.SysLogininforServiceImpl;
import com.mubai.system.service.impl.SysUserOnlineServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.messaging.simp.SimpMessagingTemplate;

/**
 * 异步工厂（产生任务用）
 *
 * @author baichuanping
 *
 */
public class AsyncFactory
{
    private static final Logger sys_user_logger = LoggerFactory.getLogger("sys-user");

    /**
     * 同步session到数据库
     *
     * @param session 在线用户会话
     * @return 任务task
     */
    public static TimerTask syncSessionToDb(final OnlineSession session)
    {
        return new TimerTask()
        {
            @Override
            public void run()
            {
                SysUserOnline online = new SysUserOnline();
                online.setSessionId(String.valueOf(session.getId()));
                online.setDeptName(session.getDeptName());
                online.setLoginName(session.getLoginName());
                online.setStartTimestamp(session.getStartTimestamp());
                online.setLastAccessTime(session.getLastAccessTime());
                online.setExpireTime(session.getTimeout());
                online.setIpaddr(session.getHost());
                online.setLoginLocation(ToolAddress.getRealAddressByIP(session.getHost()));
                online.setBrowser(session.getBrowser());
                online.setOs(session.getOs());
                online.setStatus(session.getStatus());
                SpringUtils.getBean(SysUserOnlineServiceImpl.class).saveOnline(online);
            }
        };
    }

    /**
     * 操作日志记录
     *
     * @param operLog 操作日志信息
     * @return 任务task
     */
    public static TimerTask recordOper(final SysOperLog operLog)
    {
        return new TimerTask()
        {
            @Override
            public void run()
            {
                // 远程查询操作地点
                operLog.setOperLocation(ToolAddress.getRealAddressByIP(operLog.getOperIp()));
                SpringUtils.getBean(SysOperLogService.class).insertOperlog(operLog);
            }
        };
    }

    /**
     * 向在线用户发送消息
     * @param loginNames 接收人列表
     * @param title
     * @return
     */
    public static TimerTask sendNoticeOnLine(String[] loginNames,String title) {
        return new TimerTask(){
            @Override
            public void run() {
//                List<SysUser> sysUserOnlines = SpringUtils.getBean(OnlineSessionDAO.class).listOnlineUser();
                List<String> loginNameOnlines = SpringUtils.getBean(SessionDao.class).listOnlineUser();
                SimpMessagingTemplate template = SpringUtils.getBean(SimpMessagingTemplate.class);
                for (String loginName : loginNames) {
                    if(loginNameOnlines.contains(loginName)){
                        template.convertAndSendToUser(loginName,"/queue/notifications","新消息：" + title);
                    }
                }
                /*for (SysUser sysUser: sysUserOnlines) {
                    for (Long userId:userIdList) {
                        if (userId.equals(sysUser.getUserId())) {
                            template.convertAndSendToUser(sysUser.toString(),"/queue/notifications","新消息：" + title);
                        }
                    }
                }*/
            }
        };

    }

    /**
     * 记录登陆信息
     *
     * @param username 用户名
     * @param status 状态
     * @param message 消息
     * @param args 列表
     * @return 任务task
     */
    public static TimerTask recordLogininfor(final String username, final String status, final String message, final Object... args)
    {
        final UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        final String ip = ShiroUtils.getIp();
        return new TimerTask()
        {
            @Override
            public void run()
            {
                StringBuilder s = new StringBuilder();
                s.append(LogUtils.getBlock(ip));
                s.append(ToolAddress.getRealAddressByIP(ip));
                s.append(LogUtils.getBlock(username));
                s.append(LogUtils.getBlock(status));
                s.append(LogUtils.getBlock(message));
                // 打印信息到日志
                sys_user_logger.info(s.toString(), args);
                // 获取客户端操作系统
                String os = userAgent.getOperatingSystem().getName();
                // 获取客户端浏览器
                String browser = userAgent.getBrowser().getName();
                // 封装对象
                SysLogininfor logininfor = new SysLogininfor();
                logininfor.setLoginName(username);
                logininfor.setIpaddr(ip);
                logininfor.setLoginLocation(ToolAddress.getRealAddressByIP(ip));
                logininfor.setBrowser(browser);
                logininfor.setOs(os);
                logininfor.setMsg(message);
                // 日志状态
                if (Constants.LOGIN_SUCCESS.equals(status) || Constants.LOGOUT.equals(status))
                {
                    logininfor.setStatus(Constants.SUCCESS);
                }
                else if (Constants.LOGIN_FAIL.equals(status))
                {
                    logininfor.setStatus(Constants.FAIL);
                }
                // 插入数据
                SpringUtils.getBean(SysLogininforServiceImpl.class).insertLogininfor(logininfor);
            }
        };
    }
}
