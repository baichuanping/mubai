package com.mubai.framework.web.exception;

import com.mubai.common.base.Return;
import com.mubai.common.exception.BusinessException;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.mubai.common.exception.DemoModeException;
import com.mubai.framework.util.PermissionUtils;

/**
 * 自定义异常处理器
 * 
 * @author baichuanping
 */
@RestControllerAdvice
public class DefaultExceptionHandler
{
    private static final Logger log = LoggerFactory.getLogger(DefaultExceptionHandler.class);
    
    /**
     * 权限校验失败
     */
    @ExceptionHandler(AuthorizationException.class)
    public Return handleAuthorizationException(AuthorizationException e)
    {
        log.error(e.getMessage(), e);
        return Return.FAIL("500",PermissionUtils.getMsg(e.getMessage()));
    }

    /**
     * 请求方式不支持
     */
    @ExceptionHandler({ HttpRequestMethodNotSupportedException.class })
    public Return handleException(HttpRequestMethodNotSupportedException e)
    {
        log.error(e.getMessage(), e);
        return Return.FAIL("500","不支持' " + e.getMethod() + "'请求");
    }

    /**
     * 拦截未知的运行时异常
     */
    @ExceptionHandler(RuntimeException.class)
    public Return notFount(RuntimeException e)
    {
        log.error("运行时异常:", e);
        return Return.FAIL("500","运行时异常:" + e.getMessage());
    }

    /**
     * 系统异常
     */
    @ExceptionHandler(Exception.class)
    public Return handleException(Exception e)
    {
        log.error(e.getMessage(), e);
        return Return.FAIL("500","服务器错误，请联系管理员");
    }
    /**
     * 业务异常
     */
    @ExceptionHandler(BusinessException.class)
    public Return businessException(BusinessException e)
    {
        log.error(e.getMessage(), e);
        return Return.FAIL("500",e.getMessage());
    }
    /**
     * 演示模式异常
     */
    @ExceptionHandler(DemoModeException.class)
    public Return demoModeException(DemoModeException e)
    {
        return Return.FAIL("500","演示模式，不允许操作");
    }
}
