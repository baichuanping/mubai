package com.mubai.framework.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * web 配置api 接口访问，对手持端提供接口
 * @author baichuanping
 * @create 2019-01-09
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

	/**
	 * 异步线程池
	 * @return
	 */
	@Bean
	public ThreadPoolTaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
		taskExecutor.setCorePoolSize(5);
		taskExecutor.setMaxPoolSize(20);
		taskExecutor.setQueueCapacity(20);
		taskExecutor.setThreadNamePrefix("webAsyncTask-");
		return taskExecutor;
	}
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new JwtApiInterceptor()).addPathPatterns("/api/**");
	}
}
