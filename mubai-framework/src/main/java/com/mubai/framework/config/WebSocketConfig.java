package com.mubai.framework.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import java.util.List;

/**
 * 开启WebSocket支持
 *  启用STOMP协议来传输基于代理（message broker)的消息
 * @author baichuanping
 * @create 2018-12-14
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		//注册websocket，客户端用ws://host:port/项目名/mubai-socket 访问
		registry.addEndpoint("mubai-socket")
				.setAllowedOrigins("*") // 添加允许跨域访问
				.withSockJS();
	}

	/**
	 * 应用程序以/app为前缀，代理目的地以/topic、/queue为前缀
	 * @param registry
	 */
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		// 设置服务器广播消息的基础路径
		registry.enableSimpleBroker("/queue", "/topic");
		// 设置客户端订阅消息的基础路径
//		registry.setApplicationDestinationPrefixes("/app"); // 表示客户单向服务器端发送时的主题上面需要加"/app"作为前缀。
//		registry.setUserDestinationPrefix("/user"); // 表示服务端给客户端指定用户发送一对一的主题，前缀是"/user"
	}
}
