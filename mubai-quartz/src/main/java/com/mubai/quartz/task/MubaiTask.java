package com.mubai.quartz.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

/**
 * 定时任务调度测试
 * 
 * @author baichuanping
 */
@Component("mubaiTask")
public class MubaiTask
{
    @Autowired
    private SimpMessagingTemplate template;

    public void mubaiParams(String params)
    {
        System.out.println("执行有参方法：" + params);
    }
    public void mubaiNoParams() {
        System.out.println("执行无参方法:");
        template.convertAndSend("/topic/getResponse", "欢迎体验mubai,这是一个任务计划，使用了websocket和quzrtz技术，可以在计划列表(mubaiTask)中取消");
    }
}
