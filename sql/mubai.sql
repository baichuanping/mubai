-- ----------------------------
-- 1、部门表
-- ----------------------------
drop table if exists sys_dept;
create table sys_dept (
                        dept_id 			int(11) 		not null auto_increment    comment '部门id',
                        parent_id 		int(11) 		default 0 			       comment '父部门id',
                        ancestors 		varchar(50)     default '' 			       comment '祖级列表',
                        dept_name 		varchar(30) 	default '' 				   comment '部门名称',
                        order_num 		int(4) 			default 0 			       comment '显示顺序',
                        leader            varchar(20)     default ''                 comment '负责人',
                        phone             varchar(11)     default ''                 comment '联系电话',
                        email             varchar(50)     default ''                 comment '邮箱',
                        status 			char(1) 		default '0' 			   comment '部门状态（0正常 1停用）',
                        del_flag			char(1) 		default '0' 			   comment '删除标志（0代表存在 2代表删除）',
                        create_by         varchar(64)     default ''                 comment '创建者',
                        create_time 	    datetime                                   comment '创建时间',
                        update_by         varchar(64)     default ''                 comment '更新者',
                        update_time       datetime                                   comment '更新时间',
                        primary key (dept_id)
) engine=innodb auto_increment=200 default charset=utf8 comment = '部门表';

-- ----------------------------
-- 初始化-部门表数据
-- ----------------------------
insert into sys_dept values(100,  0,   '0',          '木白科技',   0, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(101,  100, '0,100',      '上海总公司', 1, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(102,  100, '0,100',      '无锡分公司', 2, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(103,  101, '0,100,101',  '研发部门',   1, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(104,  101, '0,100,101',  '市场部门',   2, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(105,  101, '0,100,101',  '测试部门',   3, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(106,  101, '0,100,101',  '财务部门',   4, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(107,  101, '0,100,101',  '运维部门',   5, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(108,  102, '0,100,102',  '市场部门',   1, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');
insert into sys_dept values(109,  102, '0,100,102',  '财务部门',   2, '木白', '15888888888', '276058171@qq.com', '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00');

-- ----------------------------
-- 2、用户信息表
-- ----------------------------
drop table if exists sys_user;
create table sys_user (
                        user_id 			int(11) 		not null auto_increment    comment '用户ID',
                        dept_id 			int(11) 		default null			   comment '部门ID',
                        login_name 		varchar(30) 	not null 				   comment '登录账号',
                        user_name 		varchar(30) 	not null 				   comment '用户昵称',
                        user_type 		varchar(2) 	    default '00' 		       comment '用户类型（00系统用户）',
                        email  			varchar(50) 	default '' 				   comment '用户邮箱',
                        phonenumber  		varchar(11) 	default '' 				   comment '手机号码',
                        sex  		        char(1) 	    default '0' 			   comment '用户性别（0男 1女 2未知）',
                        avatar            varchar(100) 	default '' 				   comment '头像路径',
                        password 			varchar(50) 	default '' 				   comment '密码',
                        salt 				varchar(20) 	default '' 				   comment '盐加密',
                        status 			char(1) 		default '0' 			   comment '帐号状态（0正常 1停用）',
                        del_flag			char(1) 		default '0' 			   comment '删除标志（0代表存在 2代表删除）',
                        login_ip          varchar(50)     default ''                 comment '最后登陆IP',
                        login_date        datetime                                   comment '最后登陆时间',
                        create_by         varchar(64)     default ''                 comment '创建者',
                        create_time 	    datetime                                   comment '创建时间',
                        update_by         varchar(64)     default ''                 comment '更新者',
                        update_time       datetime                                   comment '更新时间',
                        remark 		    varchar(500) 	default '' 				   comment '备注',
                        primary key (user_id)
) engine=innodb auto_increment=100 default charset=utf8 comment = '用户信息表';

-- ----------------------------
-- 初始化-用户信息表数据
-- ----------------------------
insert into sys_user values(1,  103, 'admin', '超级管理员', '00', 'mubai@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2018-12-01 11-33-00', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '管理员');
insert into sys_user values(2,  105, 'mubai',    '木白', '00', '276058171@qq.com',  '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2018-12-01 11-33-00', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '测试员');


-- ----------------------------
-- 3、岗位信息表
-- ----------------------------
drop table if exists sys_post;
create table sys_post
(
  post_id       int(11)         not null auto_increment    comment '岗位ID',
  post_code     varchar(64)     not null                   comment '岗位编码',
  post_name     varchar(50)     not null                   comment '岗位名称',
  post_sort     int(4)          not null                   comment '显示顺序',
  status        char(1)         not null                   comment '状态（0正常 1停用）',
  create_by     varchar(64)     default ''                 comment '创建者',
  create_time   datetime                                   comment '创建时间',
  update_by     varchar(64) 	  default ''			     comment '更新者',
  update_time   datetime                                   comment '更新时间',
  remark 		  varchar(500) 	  default '' 				 comment '备注',
  primary key (post_id)
) engine=innodb default charset=utf8 comment = '岗位信息表';

-- ----------------------------
-- 初始化-岗位信息表数据
-- ----------------------------
insert into sys_post values(1, 'ceo',  '董事长',    1, '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_post values(2, 'se',   '项目经理',  2, '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_post values(3, 'hr',   '人力资源',  3, '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_post values(4, 'user', '普通员工',  4, '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');


-- ----------------------------
-- 4、角色信息表
-- ----------------------------
drop table if exists sys_role;
create table sys_role (
                        role_id 			int(11) 		not null auto_increment    comment '角色ID',
                        role_name 		varchar(30) 	not null 				   comment '角色名称',
                        role_key 		    varchar(100) 	not null 				   comment '角色权限字符串',
                        role_sort         int(4)          not null                   comment '显示顺序',
                        data_scope        char(1) 	    default '1'				   comment '数据范围（1：全部数据权限 2：自定数据权限）',
                        status 			char(1) 		not null 			       comment '角色状态（0正常 1停用）',
                        del_flag			char(1) 		default '0' 			   comment '删除标志（0代表存在 2代表删除）',
                        create_by         varchar(64)     default ''                 comment '创建者',
                        create_time 		datetime                                   comment '创建时间',
                        update_by 		varchar(64) 	default ''			       comment '更新者',
                        update_time 		datetime                                   comment '更新时间',
                        remark 			varchar(500) 	default '' 				   comment '备注',
                        primary key (role_id)
) engine=innodb auto_increment=100 default charset=utf8 comment = '角色信息表';

-- ----------------------------
-- 初始化-角色信息表数据
-- ----------------------------
insert into sys_role values('1', '管理员',   'admin',  1, 1, '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '管理员');
insert into sys_role values('2', '普通角色', 'common', 2, 2, '0', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '普通角色');


-- ----------------------------
-- 5、菜单权限表
-- ----------------------------
drop table if exists sys_menu;
create table sys_menu (
                        menu_id 			int(11) 		not null auto_increment    comment '菜单ID',
                        menu_name 		varchar(50) 	not null 				   comment '菜单名称',
                        parent_id 		int(11) 		default 0 			       comment '父菜单ID',
                        order_num 		int(4) 			default 0 			       comment '显示顺序',
                        url 				varchar(200) 	default '#'				   comment '请求地址',
                        menu_type 		char(1) 		default '' 			       comment '菜单类型（M目录 C菜单 F按钮）',
                        visible 			char(1) 		default 0 				   comment '菜单状态（0显示 1隐藏）',
                        perms 			varchar(100) 	default '' 				   comment '权限标识',
                        icon 				varchar(100) 	default '#' 			   comment '菜单图标',
                        create_by         varchar(64)     default ''                 comment '创建者',
                        create_time 		datetime                                   comment '创建时间',
                        update_by 		varchar(64) 	default ''			       comment '更新者',
                        update_time 		datetime                                   comment '更新时间',
                        remark 			varchar(500) 	default '' 				   comment '备注',
                        primary key (menu_id)
) engine=innodb auto_increment=2000 default charset=utf8 comment = '菜单权限表';

-- ----------------------------
-- 初始化-菜单信息表数据
-- ----------------------------
-- 一级菜单
insert into sys_menu values('1', '系统管理', '0', '10', '#', 'M', '0', '', 'fa fa-gear',         'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '系统管理目录');
insert into sys_menu values('2', '系统监控', '0', '11', '#', 'M', '0', '', 'fa fa-video-camera', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '系统监控目录');
insert into sys_menu values('3', '系统工具', '0', '12', '#', 'M', '0', '', 'fa fa-bars',         'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '系统工具目录');
INSERT INTO sys_menu values('4', '在线办公', '0', '7', '#', 'M', '0', '', 'fa fa-laptop', 'admin', '2018-12-18 16:12:43', 'mubai', '2018-12-01 11-33-00','在线办公目录');
INSERT INTO sys_menu values('5', '工作流程', '0', '9', '#', 'M', '0', '', 'fa fa-wrench', 'admin', '2018-12-05 07:48:35', 'mubai', '2018-12-01 11-33-00', '工作流程目录');
INSERT INTO sys_menu values('6', '资产管理', '0', '8', '#', 'M', '0', '', 'fa fa-newspaper-o', 'admin', '2018-12-05 07:48:35', 'mubai', '2018-12-01 11-33-00', '资产管理目录');

-- 二级菜单
insert into sys_menu values('100',  '用户管理', '1', '1', '/system/user',        'C', '0', 'system:user:view',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '用户管理菜单');
insert into sys_menu values('101',  '角色管理', '1', '2', '/system/role',        'C', '0', 'system:role:view',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '角色管理菜单');
insert into sys_menu values('102',  '菜单管理', '1', '3', '/system/menu',        'C', '0', 'system:menu:view',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '菜单管理菜单');
insert into sys_menu values('103',  '部门管理', '1', '4', '/system/dept',        'C', '0', 'system:dept:view',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '部门管理菜单');
insert into sys_menu values('104',  '岗位管理', '1', '5', '/system/post',        'C', '0', 'system:post:view',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '岗位管理菜单');
insert into sys_menu values('105',  '字典管理', '1', '6', '/system/dict',        'C', '0', 'system:dict:view',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '字典管理菜单');
insert into sys_menu values('106',  '参数设置', '1', '7', '/system/config',      'C', '0', 'system:config:view',       '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '参数设置菜单');
insert into sys_menu values('107',  '通知公告', '1', '8', '/system/notice',      'C', '0', 'system:notice:view',       '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '通知公告菜单');
insert into sys_menu values('108',  '文件管理', '1', '9', '#',                   'M', '0', '',                         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '文件管理菜单');
insert into sys_menu values('109',  '日志管理', '2', '5', '#',                   'M', '0', '',                         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '日志管理菜单');
insert into sys_menu values('110',  '在线用户', '2', '1', '/monitor/online',     'C', '0', 'monitor:online:view',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '在线用户菜单');
insert into sys_menu values('111',  '定时任务', '2', '2', '/monitor/job',        'C', '0', 'monitor:job:view',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '定时任务菜单');
insert into sys_menu values('112',  '数据监控', '2', '3', '/monitor/data',       'C', '0', 'monitor:data:view',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '数据监控菜单');
insert into sys_menu values('113',  '服务监控', '2', '4', '/monitor/server',     'C', '0', 'monitor:server:view',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '数据监控菜单');
insert into sys_menu values('114',  '表单构建', '3', '1', '/tool/build',         'C', '0', 'tool:build:view',          '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '表单构建菜单');
insert into sys_menu values('115',  '代码生成', '3', '2', '/tool/gen',           'C', '0', 'tool:gen:view',            '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '代码生成菜单');
insert into sys_menu values('116',  '系统接口', '3', '3', '/tool/swagger',       'C', '0', 'tool:swagger:view',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '系统接口菜单');

-- 在线办公
INSERT INTO sys_menu values ('201', '通知通告', '4', '1', '/oa/notify', 'C', '0', 'oa:notify:view', '#', 'admin', '2018-11-11 00:00:00', 'mubai', '2018-12-01 11-33-00', '通知通告菜单');
INSERT INTO sys_menu values ('202', '我的通知', '4', '2', 'oa/notify/selfnotify', 'C', '0', 'oa:notify:selfnotify', '#', 'admin', '2018-12-18 16:19:36', 'mubai', '2018-12-01 11-33-00', '我的通知菜单');
INSERT INTO sys_menu values ('203', '我的任务', '4', '3', '/activiti/task', 'C', '0', 'activiti:task:view', '#', 'admin', '2018-12-05 07:49:51', 'mubai', '2018-12-01 11-33-00', '我的任务菜单');
INSERT INTO sys_menu values ('204', '发起申请', '4', '4', '/activiti/task/goto', 'C', '0', 'activiti:task:goto', '#', 'admin', '2018-12-05 07:49:51', 'mubai', '2018-12-01 11-33-00', '发起申请菜单');
INSERT INTO sys_menu values ('205', '我的日程', '4', '5', '/oa/daymanage', 'C', '0', 'oa:daymanage:view', '#', 'admin', '2018-12-05 07:49:51', 'mubai', '2018-12-01 11-33-00', '我的日程菜单');
INSERT INTO sys_menu values ('206', '我的邮箱', '4', '6', '/oa/mail', 'C', '0', 'oa:mail:view', '#', 'admin', '2018-12-05 07:49:51', 'mubai', '2018-12-01 11-33-00', '我的邮箱菜单');

-- 工作流程
INSERT INTO sys_menu values ('301', '模型管理', '5', '1', '/activiti/model', 'C', '0', 'activiti:model:view', '#', 'admin', '2018-12-05 07:49:10', 'mubai', '2018-12-01 11-33-00', '模型管理菜单');
INSERT INTO sys_menu values ('302', '流程管理', '5', '3', '/activiti/process', 'C', '0', 'activiti:process:view', '#', 'admin', '2018-12-05 07:50:16', 'mubai', '2018-12-01 11-33-00', '流程管理菜单');

-- 资产管理
insert into sys_menu values ('401', '物品管理', '6', '1', '/capital/asset', 'C', '0', 'capital:asset:view', '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '资产菜单');
insert into sys_menu values ('402', '货架管理', '6', '2', '/capital/shelf', 'C', '0', 'capital:shelf:view', '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '货架菜单');
insert into sys_menu values ('403', '货区管理', '6', '3', '/capital/zone', 'C', '0', 'capital:zone:view', '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '货区菜单');
insert into sys_menu values ('404', '供应商管理', '6', '4', '/capital/supplier', 'C', '0', 'capital:supplier:view', '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '供应商菜单');
insert into sys_menu values ('405', '读写器管理', '6', '5', '/capital/reader', 'C', '0', 'capital:reader:view', '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '读写器菜单');


-- 三级菜单
insert into sys_menu values('500',  '文件上传', '108', '1', '/system/files', 'C', '0', 'system:files:view', '#', 'admin', '2018-12-01 11-33-00', 'admin', '2018-12-01 11-33-00', '文件上传菜单');
insert into sys_menu values('501',  '通道设置', '108', '2', '/system/fileChannel', 'C', '0', 'system:fileChannel:view', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '文件通道菜单');
insert into sys_menu values('502',  '操作日志', '109', '1', '/monitor/operlog',    'C', '0', 'monitor:operlog:view',     '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '操作日志菜单');
insert into sys_menu values('503',  '登录日志', '109', '2', '/monitor/logininfor', 'C', '0', 'monitor:logininfor:view',  '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '登录日志菜单');
-- 用户管理按钮
insert into sys_menu values('1000', '用户查询', '100', '1',  '#',  'F', '0', 'system:user:list',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1001', '用户新增', '100', '2',  '#',  'F', '0', 'system:user:add',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1002', '用户修改', '100', '3',  '#',  'F', '0', 'system:user:edit',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1003', '用户删除', '100', '4',  '#',  'F', '0', 'system:user:remove',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1004', '用户导出', '100', '5',  '#',  'F', '0', 'system:user:export',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1005', '用户导入', '100', '6',  '#',  'F', '0', 'system:user:import',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1006', '重置密码', '100', '6',  '#',  'F', '0', 'system:user:resetPwd',    '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 角色管理按钮
insert into sys_menu values('1007', '角色查询', '101', '1',  '#',  'F', '0', 'system:role:list',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1008', '角色新增', '101', '2',  '#',  'F', '0', 'system:role:add',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1009', '角色修改', '101', '3',  '#',  'F', '0', 'system:role:edit',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1010', '角色删除', '101', '4',  '#',  'F', '0', 'system:role:remove',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1011', '角色导出', '101', '5',  '#',  'F', '0', 'system:role:export',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 菜单管理按钮
insert into sys_menu values('1012', '菜单查询', '102', '1',  '#',  'F', '0', 'system:menu:list',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1013', '菜单新增', '102', '2',  '#',  'F', '0', 'system:menu:add',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1014', '菜单修改', '102', '3',  '#',  'F', '0', 'system:menu:edit',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1015', '菜单删除', '102', '4',  '#',  'F', '0', 'system:menu:remove',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 部门管理按钮
insert into sys_menu values('1016', '部门查询', '103', '1',  '#',  'F', '0', 'system:dept:list',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1017', '部门新增', '103', '2',  '#',  'F', '0', 'system:dept:add',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1018', '部门修改', '103', '3',  '#',  'F', '0', 'system:dept:edit',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1019', '部门删除', '103', '4',  '#',  'F', '0', 'system:dept:remove',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 岗位管理按钮
insert into sys_menu values('1020', '岗位查询', '104', '1',  '#',  'F', '0', 'system:post:list',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1021', '岗位新增', '104', '2',  '#',  'F', '0', 'system:post:add',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1022', '岗位修改', '104', '3',  '#',  'F', '0', 'system:post:edit',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1023', '岗位删除', '104', '4',  '#',  'F', '0', 'system:post:remove',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1024', '岗位导出', '104', '5',  '#',  'F', '0', 'system:post:export',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 字典管理按钮
insert into sys_menu values('1025', '字典查询', '105', '1', '#',  'F', '0', 'system:dict:list',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1026', '字典新增', '105', '2', '#',  'F', '0', 'system:dict:add',          '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1027', '字典修改', '105', '3', '#',  'F', '0', 'system:dict:edit',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1028', '字典删除', '105', '4', '#',  'F', '0', 'system:dict:remove',       '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1029', '字典导出', '105', '5', '#',  'F', '0', 'system:dict:export',       '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 参数设置按钮
insert into sys_menu values('1030', '参数查询', '106', '1', '#',  'F', '0', 'system:config:list',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1031', '参数新增', '106', '2', '#',  'F', '0', 'system:config:add',       '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1032', '参数修改', '106', '3', '#',  'F', '0', 'system:config:edit',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1033', '参数删除', '106', '4', '#',  'F', '0', 'system:config:remove',    '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1034', '参数导出', '106', '5', '#',  'F', '0', 'system:config:export',    '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 通知公告按钮
insert into sys_menu values('1035', '公告查询', '107', '1', '#',  'F', '0', 'system:notice:list',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1036', '公告新增', '107', '2', '#',  'F', '0', 'system:notice:add',       '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1037', '公告修改', '107', '3', '#',  'F', '0', 'system:notice:edit',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1038', '公告删除', '107', '4', '#',  'F', '0', 'system:notice:remove',    '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 文件管理按钮
insert into sys_menu values('1039', '查询', '500', '1', '#', 'f', '0', 'system:files:list', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1040', '上传', '500', '2', '#', 'f', '0', 'system:files:upload', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1041', '修改', '500', '3', '#', 'f', '0', 'system:files:update', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1042', '删除', '500', '4', '#', 'f', '0', 'system:files:remove', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1043', '导出', '500', '5', '#', 'f', '0', 'system:files:export', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');

insert into sys_menu values('1044', '新增', '501', '1', '#', 'f', '0', 'system:fileChannel:add', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1045', '修改', '502', '2', '#', 'f', '0', 'system:fileChannel:update', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 操作日志按钮
insert into sys_menu values('1046', '操作查询', '502', '1', '#',  'F', '0', 'monitor:operlog:list',    '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1047', '操作删除', '502', '2', '#',  'F', '0', 'monitor:operlog:remove',  '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1048', '详细信息', '502', '3', '#',  'F', '0', 'monitor:operlog:detail',  '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1049', '日志导出', '502', '4', '#',  'F', '0', 'monitor:operlog:export',  '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 登录日志按钮
insert into sys_menu values('1050', '登录查询', '503', '1', '#',  'F', '0', 'monitor:logininfor:list',         '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1051', '登录删除', '503', '2', '#',  'F', '0', 'monitor:logininfor:remove',       '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1052', '日志导出', '503', '3', '#',  'F', '0', 'monitor:logininfor:export',       '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 在线用户按钮
insert into sys_menu values('1053', '在线查询', '110', '1', '#',  'F', '0', 'monitor:online:list',             '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1054', '批量强退', '110', '2', '#',  'F', '0', 'monitor:online:batchForceLogout', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1055', '单条强退', '110', '3', '#',  'F', '0', 'monitor:online:forceLogout',      '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 定时任务按钮
insert into sys_menu values('1056', '任务查询', '111', '1', '#',  'F', '0', 'monitor:job:list',                '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1057', '任务新增', '111', '2', '#',  'F', '0', 'monitor:job:add',                 '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1058', '任务修改', '111', '3', '#',  'F', '0', 'monitor:job:edit',                '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1059', '任务删除', '111', '4', '#',  'F', '0', 'monitor:job:remove',              '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1060', '状态修改', '111', '5', '#',  'F', '0', 'monitor:job:changeStatus',        '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1061', '任务详细', '111', '6', '#',  'F', '0', 'monitor:job:detail',              '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1062', '任务导出', '111', '7', '#',  'F', '0', 'monitor:job:export',              '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 代码生成按钮
insert into sys_menu values('1063', '生成查询', '115', '1', '#',  'F', '0', 'tool:gen:list',  '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_menu values('1064', '生成代码', '115', '2', '#',  'F', '0', 'tool:gen:code',  '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
INSERT INTO sys_menu values('1065', '生成修改', '115', '3', '#',  'F', '0', 'tool:gen:modify', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 通知通告
INSERT INTO sys_menu values('1066', '通知通告查询', '201', '1', '#', 'F', '0', 'oa:notify:list', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
INSERT INTO sys_menu values('1067', '通知通告新增', '201', '2', '#', 'F', '0', 'oa:notify:add', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
INSERT INTO sys_menu values('1068', '通知通告修改', '201', '3', '#', 'F', '0', 'oa:notify:edit', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
INSERT INTO sys_menu values('1069', '通知通告删除', '201', '4', '#', 'F', '0', 'oa:notify:remove', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 工作流任务
INSERT INTO sys_menu values('1070', '任务查询', '203', '1', '#', 'F', '0', 'activiti:task:list', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
INSERT INTO sys_menu values('1071', '任务申请', '203', '2', '#', 'F', '0', 'activiti:task:apply', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
INSERT INTO sys_menu values('1072', '任务审批', '203', '3', '#', 'F', '0', 'activiti:task:assignee', '#', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
-- 日程管理
insert into sys_menu values('1081','日程查询','205','1',  '#',  'F', '0', 'oa:daymanage:list',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('1082','日程新增','205','2',  '#',  'F', '0', 'oa:daymanage:add',          '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('1083','日程修改','205','3',  '#',  'F', '0', 'oa:daymanage:edit',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('1084','日程删除','205','4',  '#',  'F', '0', 'oa:daymanage:remove',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
-- 模型管理按钮
insert into sys_menu values('2011', '模型查询', '301', '1', '#',  'F', '0', 'activiti:model:list',                '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
insert into sys_menu values('2012', '模型新增', '301', '2', '#',  'F', '0', 'activiti:model:add',                 '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
insert into sys_menu values('2013', '模型修改', '301', '3', '#',  'F', '0', 'activiti:model:edit',                '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
insert into sys_menu values('2014', '模型删除', '301', '4', '#',  'F', '0', 'activiti:model:remove',              '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
insert into sys_menu values('2015', '模型部署', '301', '5', '#',  'F', '0', 'activiti:model:deploy',              '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
insert into sys_menu values('2016', '模型导出', '301', '6', '#',  'F', '0', 'activiti:model:export',              '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
-- 流程管理按钮
insert into sys_menu values('2021', '流程查询', '302', '1', '#',  'F', '0', 'activiti:process:list',              '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
insert into sys_menu values('2022', '流程新增', '302', '2', '#',  'F', '0', 'activiti:process:add',               '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
insert into sys_menu values('2023', '流程删除', '302', '3', '#',  'F', '0', 'activiti:process:remove',            '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
insert into sys_menu values('2024', '转为模型', '302', '4', '#',  'F', '0', 'activiti:process:model',             '#', 'admin', '2018-03-16 11-33-00', 'mubai', '2018-03-16 11-33-00', '');
-- 物品管理按钮
insert into sys_menu values ('3001','物品查询', '401', '1',  '#',  'F', '0', 'capital:asset:list',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values ('3002','物品新增', '401', '2',  '#',  'F', '0', 'capital:asset:add',          '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values ('3003','物品修改', '401', '3',  '#',  'F', '0', 'capital:asset:edit',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values ('3004','物品删除', '401', '4',  '#',  'F', '0', 'capital:asset:remove',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values ('3005','物品导出', '401', '5',  '#',  'F', '0', 'capital:asset:export',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
-- 货架管理按钮
insert into sys_menu values('3101','货架查询', '402', '1',  '#',  'F', '0', 'capital:shelf:list',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3102','货架新增', '402', '2',  '#',  'F', '0', 'capital:shelf:add',          '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3103','货架修改', '402', '3',  '#',  'F', '0', 'capital:shelf:edit',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3104','货架删除', '402', '4',  '#',  'F', '0', 'capital:shelf:remove',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3105','货架导出', '402', '5',  '#',  'F', '0', 'capital:shelf:export',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
-- 货区管理按钮
insert into sys_menu values('3201','货区查询', '403', '1',  '#',  'F', '0', 'capital:zone:list',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3202','货区新增', '403', '2',  '#',  'F', '0', 'capital:zone:add',          '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3203','货区修改', '403', '3',  '#',  'F', '0', 'capital:zone:edit',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3204','货区删除', '403', '4',  '#',  'F', '0', 'capital:zone:remove',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3205','货区导出', '403', '5',  '#',  'F', '0', 'capital:zone:export',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
-- 供应商管理按钮
insert into sys_menu values('3301','供应商查询', '404', '1',  '#',  'F', '0', 'capital:supplier:list',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3302','供应商新增', '404', '2',  '#',  'F', '0', 'capital:supplier:add',          '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3303','供应商修改', '404', '3',  '#',  'F', '0', 'capital:supplier:edit',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3304','供应商删除', '404', '4',  '#',  'F', '0', 'capital:supplier:remove',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3305','供应商导出', '404', '5',  '#',  'F', '0', 'capital:supplier:export',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
-- 读写器管理按钮
insert into sys_menu values('3401','读写器查询', '405', '1',  '#',  'F', '0', 'capital:reader:list',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3402','读写器新增', '405', '2',  '#',  'F', '0', 'capital:reader:add',          '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3403','读写器修改', '405', '3',  '#',  'F', '0', 'capital:reader:edit',         '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3404','读写器删除', '405', '4',  '#',  'F', '0', 'capital:reader:remove',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');
insert into sys_menu values('3405','读写器导出', '405', '4',  '#',  'F', '0', 'capital:reader:export',       '#', 'admin', '2018-11-11', 'mubai', '2018-11-11', '');

-- ----------------------------
-- 6、用户和角色关联表  用户N-1角色
-- ----------------------------
drop table if exists sys_user_role;
create table sys_user_role (
                             user_id 	int(11) not null comment '用户ID',
                             role_id 	int(11) not null comment '角色ID',
                             primary key(user_id, role_id)
) engine=innodb default charset=utf8 comment = '用户和角色关联表';

-- ----------------------------
-- 初始化-用户和角色关联表数据
-- ----------------------------
insert into sys_user_role values ('1', '1');
insert into sys_user_role values ('2', '2');


-- ----------------------------
-- 7、角色和菜单关联表  角色1-N菜单
-- ----------------------------
drop table if exists sys_role_menu;
create table sys_role_menu (
                             role_id 	int(11) not null comment '角色ID',
                             menu_id 	int(11) not null comment '菜单ID',
                             primary key(role_id, menu_id)
) engine=innodb default charset=utf8 comment = '角色和菜单关联表';

-- ----------------------------
-- 初始化-角色和菜单关联表数据
-- ----------------------------
insert into sys_role_menu values ('2', '1');
insert into sys_role_menu values ('2', '2');
insert into sys_role_menu values ('2', '3');
insert into sys_role_menu values ('2', '100');
insert into sys_role_menu values ('2', '101');
insert into sys_role_menu values ('2', '102');
insert into sys_role_menu values ('2', '103');
insert into sys_role_menu values ('2', '104');
insert into sys_role_menu values ('2', '105');
insert into sys_role_menu values ('2', '106');
insert into sys_role_menu values ('2', '107');
insert into sys_role_menu values ('2', '108');
insert into sys_role_menu values ('2', '109');
insert into sys_role_menu values ('2', '110');
insert into sys_role_menu values ('2', '111');
insert into sys_role_menu values ('2', '112');
insert into sys_role_menu values ('2', '113');
insert into sys_role_menu values ('2', '114');
insert into sys_role_menu values ('2', '115');
insert into sys_role_menu values ('2', '116');
insert into sys_role_menu values ('2', '500');
insert into sys_role_menu values ('2', '501');
insert into sys_role_menu values ('2', '502');
insert into sys_role_menu values ('2', '503');
insert into sys_role_menu values ('2', '1000');
insert into sys_role_menu values ('2', '1001');
insert into sys_role_menu values ('2', '1002');
insert into sys_role_menu values ('2', '1003');
insert into sys_role_menu values ('2', '1004');
insert into sys_role_menu values ('2', '1005');
insert into sys_role_menu values ('2', '1006');
insert into sys_role_menu values ('2', '1007');
insert into sys_role_menu values ('2', '1008');
insert into sys_role_menu values ('2', '1009');
insert into sys_role_menu values ('2', '1010');
insert into sys_role_menu values ('2', '1011');
insert into sys_role_menu values ('2', '1012');
insert into sys_role_menu values ('2', '1013');
insert into sys_role_menu values ('2', '1014');
insert into sys_role_menu values ('2', '1015');
insert into sys_role_menu values ('2', '1016');
insert into sys_role_menu values ('2', '1017');
insert into sys_role_menu values ('2', '1018');
insert into sys_role_menu values ('2', '1019');
insert into sys_role_menu values ('2', '1020');
insert into sys_role_menu values ('2', '1021');
insert into sys_role_menu values ('2', '1022');
insert into sys_role_menu values ('2', '1023');
insert into sys_role_menu values ('2', '1024');
insert into sys_role_menu values ('2', '1025');
insert into sys_role_menu values ('2', '1026');
insert into sys_role_menu values ('2', '1027');
insert into sys_role_menu values ('2', '1028');
insert into sys_role_menu values ('2', '1029');
insert into sys_role_menu values ('2', '1030');
insert into sys_role_menu values ('2', '1031');
insert into sys_role_menu values ('2', '1032');
insert into sys_role_menu values ('2', '1033');
insert into sys_role_menu values ('2', '1034');
insert into sys_role_menu values ('2', '1035');
insert into sys_role_menu values ('2', '1036');
insert into sys_role_menu values ('2', '1037');
insert into sys_role_menu values ('2', '1038');
insert into sys_role_menu values ('2', '1039');
insert into sys_role_menu values ('2', '1040');
insert into sys_role_menu values ('2', '1041');
insert into sys_role_menu values ('2', '1042');
insert into sys_role_menu values ('2', '1043');
insert into sys_role_menu values ('2', '1044');
insert into sys_role_menu values ('2', '1045');
insert into sys_role_menu values ('2', '1046');
insert into sys_role_menu values ('2', '1047');
insert into sys_role_menu values ('2', '1048');
insert into sys_role_menu values ('2', '1049');
insert into sys_role_menu values ('2', '1050');
insert into sys_role_menu values ('2', '1051');
insert into sys_role_menu values ('2', '1052');
insert into sys_role_menu values ('2', '1053');
insert into sys_role_menu values ('2', '1054');
insert into sys_role_menu values ('2', '1055');
insert into sys_role_menu values ('2', '1056');
insert into sys_role_menu values ('2', '1057');
insert into sys_role_menu values ('2', '1058');

-- ----------------------------
-- 8、角色和部门关联表  角色1-N部门
-- ----------------------------
drop table if exists sys_role_dept;
create table sys_role_dept (
                             role_id 	int(11) not null comment '角色ID',
                             dept_id 	int(11) not null comment '部门ID',
                             primary key(role_id, dept_id)
) engine=innodb default charset=utf8 comment = '角色和部门关联表';

-- ----------------------------
-- 初始化-角色和部门关联表数据
-- ----------------------------
insert into sys_role_dept values ('2', '100');
insert into sys_role_dept values ('2', '101');
insert into sys_role_dept values ('2', '105');

-- ----------------------------
-- 9、用户与岗位关联表  用户1-N岗位
-- ----------------------------
drop table if exists sys_user_post;
create table sys_user_post
(
  user_id int(11) not null comment '用户ID',
  post_id int(11) not null comment '岗位ID',
  primary key (user_id, post_id)
) engine=innodb default charset=utf8 comment = '用户与岗位关联表';

-- ----------------------------
-- 初始化-用户与岗位关联表数据
-- ----------------------------
insert into sys_user_post values ('1', '1');
insert into sys_user_post values ('2', '2');


-- ----------------------------
-- 10、操作日志记录
-- ----------------------------
drop table if exists sys_oper_log;
create table sys_oper_log (
                            oper_id 			int(11) 		not null auto_increment    comment '日志主键',
                            title             varchar(50)     default ''                 comment '模块标题',
                            business_type     int(2)          default 0                  comment '业务类型（0其它 1新增 2修改 3删除）',
                            method            varchar(100)    default ''                 comment '方法名称',
                            operator_type     int(1)          default 0                  comment '操作类别（0其它 1后台用户 2手机端用户）',
                            oper_name 	    varchar(50)     default '' 		 	 	   comment '操作人员',
                            dept_name 		varchar(50)     default '' 		 	 	   comment '部门名称',
                            oper_url 		    varchar(255) 	default '' 				   comment '请求URL',
                            oper_ip 			varchar(50) 	default '' 				   comment '主机地址',
                            oper_location     varchar(255)    default ''                 comment '操作地点',
                            oper_param 		varchar(255) 	default '' 				   comment '请求参数',
                            status 			int(1) 		    default 0				   comment '操作状态（0正常 1异常）',
                            error_msg 		varchar(2000) 	default '' 				   comment '错误消息',
                            oper_time 		datetime                                   comment '操作时间',
                            primary key (oper_id)
) engine=innodb auto_increment=100 default charset=utf8 comment = '操作日志记录';


-- ----------------------------
-- 11、字典类型表
-- ----------------------------
drop table if exists sys_dict_type;
create table sys_dict_type
(
  dict_id          int(11) 		 not null auto_increment    comment '字典主键',
  dict_name        varchar(100)    default ''                 comment '字典名称',
  dict_type        varchar(100)    default ''                 comment '字典类型',
  status 			 char(1) 		 default '0'			    comment '状态（0正常 1停用）',
  create_by        varchar(64)     default ''                 comment '创建者',
  create_time      datetime                                   comment '创建时间',
  update_by        varchar(64) 	 default ''			        comment '更新者',
  update_time      datetime                                   comment '更新时间',
  remark 	         varchar(500) 	 default '' 				comment '备注',
  primary key (dict_id),
  unique (dict_type)
) engine=innodb auto_increment=100 default charset=utf8 comment = '字典类型表';

insert into sys_dict_type values(1,  '用户性别', 'sys_user_sex',        '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '用户性别列表');
insert into sys_dict_type values(2,  '菜单状态', 'sys_show_hide',       '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '菜单状态列表');
insert into sys_dict_type values(3,  '系统开关', 'sys_normal_disable',  '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '系统开关列表');
insert into sys_dict_type values(4,  '任务状态', 'sys_job_status',      '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '任务状态列表');
insert into sys_dict_type values(5,  '系统是否', 'sys_yes_no',          '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '系统是否列表');
insert into sys_dict_type values(6,  '通知类型', 'sys_notice_type',     '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '通知类型列表');
insert into sys_dict_type values(7,  '通知状态', 'sys_notice_status',   '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '通知状态列表');
insert into sys_dict_type values(8,  '操作类型', 'sys_oper_type',       '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '操作类型列表');
insert into sys_dict_type values(9,  '系统状态', 'sys_common_status',   '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '登录状态列表');
INSERT INTO sys_dict_type values(10, '通告读标识', 'oa_notify_read',    '0', 'admin', '2018-12-20 10:33:15', 'mubai','2018-12-01 11-33-00', '通告读标识列表');
INSERT INTO sys_dict_type values(11, '日程类型', 'oa_daymanage_type',    '0', 'admin', '2018-12-20 10:33:15', 'mubai','2018-12-01 11-33-00', '日程类型列表');
INSERT INTO sys_dict_type values(12, '日程状态', 'oa_daymanage_status',    '0', 'admin', '2018-12-20 10:33:15', 'mubai','2018-12-01 11-33-00', '日程状态列表');
INSERT INTO sys_dict_type values(13, '文件状态', 'sys_file_type',       '0', 'admin', '2018-12-20 10:25:56', 'mubai', '2018-12-20 10:26:01', '文件类型表');
INSERT INTO sys_dict_type values(14, '连接状态', 'ca_reader_status',    '0', 'admin', '2018-12-20 10:33:15', 'mubai','2018-12-01 11-33-00', '连接状态列表');
INSERT INTO sys_dict_type values(15, '读写器类型', 'ca_reader_type',    '0', 'admin', '2018-12-20 10:33:15', 'mubai','2018-12-01 11-33-00', '读写器类型列表');
INSERT INTO sys_dict_type values(16, '货架状态', 'ca_shelf_status',    '0', 'admin', '2018-12-20 10:33:15', 'mubai','2018-12-01 11-33-00', '货架状态列表');
INSERT INTO sys_dict_type values(17, '库存状态', 'ca_asset_status',    '0', 'admin', '2018-12-20 10:33:15', 'mubai','2018-12-01 11-33-00', '库存状态列表');
INSERT INTO sys_dict_type values(18, '库存类型', 'ca_asset_type',    '0', 'admin', '2018-12-20 10:33:15', 'mubai','2018-12-01 11-33-00', '库存类型列表');

-- ----------------------------
-- 12、字典数据表
-- ----------------------------
drop table if exists sys_dict_data;
create table sys_dict_data
(
  dict_code        int(11) 		 not null auto_increment    comment '字典编码',
  dict_sort        int(4)          default 0                  comment '字典排序',
  dict_label       varchar(100)    default ''                 comment '字典标签',
  dict_value       varchar(100)    default ''                 comment '字典键值',
  dict_type        varchar(100)    default ''                 comment '字典类型',
  css_class        varchar(100)    default ''                 comment '样式属性（其他样式扩展）',
  list_class       varchar(100)    default ''                 comment '表格回显样式',
  is_default       char(1)         default 'N'                comment '是否默认（Y是 N否）',
  status 			 char(1) 		 default '0'			    comment '状态（0正常 1停用）',
  create_by        varchar(64)     default ''                 comment '创建者',
  create_time      datetime                                   comment '创建时间',
  update_by        varchar(64) 	 default ''			        comment '更新者',
  update_time      datetime                                   comment '更新时间',
  remark 	         varchar(500) 	 default '' 				comment '备注',
  primary key (dict_code)
) engine=innodb auto_increment=100 default charset=utf8 comment = '字典数据表';


insert into sys_dict_data values(1,  1, '男',       '0',  'sys_user_sex',        '',   '',        'Y', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '性别男');
insert into sys_dict_data values(2,  2, '女',       '1',  'sys_user_sex',        '',   '',        'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '性别女');
insert into sys_dict_data values(3,  3, '未知',     '2',  'sys_user_sex',        '',   '',        'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '性别未知');
insert into sys_dict_data values(4,  1, '显示',     '0',  'sys_show_hide',       '',   'primary', 'Y', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '显示菜单');
insert into sys_dict_data values(5,  2, '隐藏',     '1',  'sys_show_hide',       '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '隐藏菜单');
insert into sys_dict_data values(6,  1, '正常',     '0',  'sys_normal_disable',  '',   'primary', 'Y', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '正常状态');
insert into sys_dict_data values(7,  2, '停用',     '1',  'sys_normal_disable',  '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '停用状态');
insert into sys_dict_data values(8,  1, '正常',     '0',  'sys_job_status',      '',   'primary', 'Y', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '正常状态');
insert into sys_dict_data values(9,  2, '暂停',     '1',  'sys_job_status',      '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '停用状态');
insert into sys_dict_data values(10, 1, '是',       'Y',  'sys_yes_no',          '',   'primary', 'Y', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '系统默认是');
insert into sys_dict_data values(11, 2, '否',       'N',  'sys_yes_no',          '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '系统默认否');
insert into sys_dict_data values(12, 1, '会议通告',     '1',  'sys_notice_type',     '',   'warning', 'Y', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '通知');
insert into sys_dict_data values(13, 2, '奖惩通告',     '2',  'sys_notice_type',     '',   'success', 'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '公告');
insert into sys_dict_data values(14, 3, '活动通告',     '3',  'sys_notice_type',     '',   'danger', 'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '公告');
insert into sys_dict_data values(15, 1, '草稿',     '0',  'sys_notice_status',   '',   'primary', 'Y', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '正常状态');
insert into sys_dict_data values(16, 2, '发布',     '1',  'sys_notice_status',   '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '关闭状态');
insert into sys_dict_data values(17, 1, '新增',     '1',  'sys_oper_type',       '',   'info',    'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '新增操作');
insert into sys_dict_data values(18, 2, '修改',     '2',  'sys_oper_type',       '',   'info',    'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '修改操作');
insert into sys_dict_data values(19, 3, '删除',     '3',  'sys_oper_type',       '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '删除操作');
insert into sys_dict_data values(20, 4, '授权',     '4',  'sys_oper_type',       '',   'primary', 'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '授权操作');
insert into sys_dict_data values(21, 5, '导出',     '5',  'sys_oper_type',       '',   'warning', 'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '导出操作');
insert into sys_dict_data values(22, 6, '导入',     '6',  'sys_oper_type',       '',   'warning', 'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '导入操作');
insert into sys_dict_data values(23, 7, '强退',     '7',  'sys_oper_type',       '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '强退操作');
insert into sys_dict_data values(24, 8, '生成代码', '8',  'sys_oper_type',       '',   'warning', 'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '生成操作');
insert into sys_dict_data values(25, 8, '清空数据', '9',  'sys_oper_type',       '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '清空操作');
insert into sys_dict_data values(26, 1, '成功',     '0',  'sys_common_status',   '',   'primary', 'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '正常状态');
insert into sys_dict_data values(27, 2, '失败',     '1',  'sys_common_status',   '',   'danger',  'N', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '停用状态');
INSERT INTO sys_dict_data VALUES(28, 1, '图片', '0', 'sys_file_type', '', 'danger', 'N', '0', 'admin', '2018-12-20 10:26:53', 'mubai', '2018-12-20 10:26:58', '图片');
INSERT INTO sys_dict_data VALUES(29, 2, '视频', '1', 'sys_file_type', '', 'primary', 'N', '0', 'admin', '2018-12-20 10:26:53', 'mubai', '2018-12-20 10:26:58', '视频');
INSERT INTO sys_dict_data VALUES(30, 3, '文字', '2', 'sys_file_type', '', 'info', 'N', '0', 'admin', '2018-12-20 10:26:53', 'mubai', '2018-12-20 10:26:58', '文字');
INSERT INTO sys_dict_data values(31, 1, '未读', '0', 'oa_notify_read', '', 'primary', 'Y', '0', 'admin', '2018-12-20 10:33:52', 'mubai', '2018-12-01 11-33-00', '未读');
INSERT INTO sys_dict_data values(32, 2, '已读', '1', 'oa_notify_read', '', 'success', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '已读');
INSERT INTO sys_dict_data values(33, 1, '日程安排', '1', 'oa_daymanage_type', '', 'primary', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '日程安排');
INSERT INTO sys_dict_data values(34, 2, '假日提醒', '2', 'oa_daymanage_type', '', 'success', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '假日提醒');
INSERT INTO sys_dict_data values(35, 1, '一般', '1', 'oa_daymanage_status', '', 'primary', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '一般');
INSERT INTO sys_dict_data values(36, 2, '重要', '2', 'oa_daymanage_status', '', 'success', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '重要');
INSERT INTO sys_dict_data values(37, 3, '紧急', '3', 'oa_daymanage_status', '', 'danger', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '紧急');
INSERT INTO sys_dict_data values(38, 1, '连接', '0', 'ca_reader_status', '', 'success', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '连接状态');
INSERT INTO sys_dict_data values(39, 2, '断开', '1', 'ca_reader_status', '', 'danger', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '断开状态');
INSERT INTO sys_dict_data values(40, 1, '4通道', '4', 'ca_reader_type', '', 'primary', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '4通道');
INSERT INTO sys_dict_data values(41, 2, '8通道', '8', 'ca_reader_type', '', 'warning', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '8通道');
INSERT INTO sys_dict_data values(42, 1, '在用', '0', 'ca_shelf_status', '', 'primary', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '在用状态');
INSERT INTO sys_dict_data values(43, 2, '空闲', '1', 'ca_shelf_status', '', 'warning', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '空闲状态');
INSERT INTO sys_dict_data values(44, 3, '报废', '2', 'ca_shelf_status', '', 'info', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '报废状态');
INSERT INTO sys_dict_data values(45, 4, '停用', '3', 'ca_shelf_status', '', 'danger', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '停用状态');
INSERT INTO sys_dict_data values(46, 1, '入库', '0', 'ca_asset_status', '', 'success', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '入库状态');
INSERT INTO sys_dict_data values(47, 2, '出库', '1', 'ca_asset_status', '', 'primary', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '出库状态');
INSERT INTO sys_dict_data values(48, 3, '报废', '2', 'ca_asset_status', '', 'danger', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '报废状态');
INSERT INTO sys_dict_data values(49, 1, '书画', '10', 'ca_asset_type', '', 'success', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '报废状态');
INSERT INTO sys_dict_data values(50, 2, '石器', '20', 'ca_asset_type', '', 'primary', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '报废状态');
INSERT INTO sys_dict_data values(51, 3, '铜器', '30', 'ca_asset_type', '', 'danger', 'Y', '0', 'admin', '2018-12-20 10:34:21', 'mubai', '2018-12-01 11-33-00', '报废状态');

-- ----------------------------
-- 13、参数配置表
-- ----------------------------
drop table if exists sys_config;
create table sys_config (
                          config_id 		   int(5) 	     not null auto_increment    comment '参数主键',
                          config_name        varchar(100)  default ''                 comment '参数名称',
                          config_key         varchar(100)  default ''                 comment '参数键名',
                          config_value       varchar(100)  default ''                 comment '参数键值',
                          config_type        char(1)       default 'N'                comment '系统内置（Y是 N否）',
                          create_by          varchar(64)   default ''                 comment '创建者',
                          create_time 	   datetime                                 comment '创建时间',
                          update_by          varchar(64)   default ''                 comment '更新者',
                          update_time        datetime                                 comment '更新时间',
                          remark              varchar(500)  default ''                 comment '备注信息',
                          primary key (config_id)
) engine=innodb auto_increment=100 default charset=utf8 comment = '参数配置表';

insert into sys_config values(1, '主框架页-默认皮肤样式名称', 'sys.index.skinName',     'skin-blue',     'Y', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow' );
insert into sys_config values(2, '用户管理-账号初始密码',     'sys.user.initPassword',  '123456',        'Y', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '初始化密码 123456' );


-- ----------------------------
-- 14、系统访问记录
-- ----------------------------
drop table if exists sys_logininfor;
create table sys_logininfor (
                              info_id 		 int(11) 	   not null auto_increment   comment '访问ID',
                              login_name 	 varchar(50)   default '' 			     comment '登录账号',
                              ipaddr 		 varchar(50)   default '' 			     comment '登录IP地址',
                              login_location varchar(255)  default ''                comment '登录地点',
                              browser  		 varchar(50)   default '' 			     comment '浏览器类型',
                              os      		 varchar(50)   default '' 			     comment '操作系统',
                              status 		 char(1) 	   default '0' 			     comment '登录状态（0成功 1失败）',
                              msg      		 varchar(255)  default '' 			     comment '提示消息',
                              login_time 	 datetime                                comment '访问时间',
                              primary key (info_id)
) engine=innodb auto_increment=100 default charset=utf8 comment = '系统访问记录';


-- ----------------------------
-- 15、在线用户记录
-- ----------------------------
drop table if exists sys_user_online;
create table sys_user_online (
                               sessionId 	    varchar(50)  default ''              	comment '用户会话id',
                               login_name 	    varchar(50)  default '' 		 	 	comment '登录账号',
                               dept_name 		varchar(50)  default '' 		 	 	comment '部门名称',
                               ipaddr 		    varchar(50)  default '' 			 	comment '登录IP地址',
                               login_location    varchar(255) default ''                 comment '登录地点',
                               browser  		    varchar(50)  default '' 			 	comment '浏览器类型',
                               os      		    varchar(50)  default '' 			 	comment '操作系统',
                               status      	    varchar(10)  default '' 			 	comment '在线状态on_line在线off_line离线',
                               start_timestamp 	datetime                                comment 'session创建时间',
                               last_access_time  datetime                                comment 'session最后访问时间',
                               expire_time 	    int(5) 		 default 0 			 	    comment '超时时间，单位为分钟',
                               primary key (sessionId)
) engine=innodb default charset=utf8 comment = '在线用户记录';


-- ----------------------------
-- 16、定时任务调度表
-- ----------------------------
drop table if exists sys_job;
create table sys_job (
                       job_id 		      int(11) 	    not null auto_increment    comment '任务ID',
                       job_name            varchar(64)   default ''                 comment '任务名称',
                       job_group           varchar(64)   default ''                 comment '任务组名',
                       method_name         varchar(500)  default ''                 comment '任务方法',
                       method_params       varchar(200)  default ''                 comment '方法参数',
                       cron_expression     varchar(255)  default ''                 comment 'cron执行表达式',
                       misfire_policy      varchar(20)   default '3'                comment '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
                       status              char(1)       default '0'                comment '状态（0正常 1暂停）',
                       create_by           varchar(64)   default ''                 comment '创建者',
                       create_time         datetime                                 comment '创建时间',
                       update_by           varchar(64)   default ''                 comment '更新者',
                       update_time         datetime                                 comment '更新时间',
                       remark              varchar(500)  default ''                 comment '备注信息',
                       primary key (job_id, job_name, job_group)
) engine=innodb auto_increment=100 default charset=utf8 comment = '定时任务调度表';

insert into sys_job values(1, 'mubaiTask', '系统默认（无参）', 'mubaiNoParams',  '',   '0 */3 * * * ?', '3', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');
insert into sys_job values(2, 'mubaiTask', '系统默认（有参）', 'mubaiParams',    'mubai', '0/20 * * * * ?', '3', '1', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '');


-- ----------------------------
-- 17、定时任务调度日志表
-- ----------------------------
drop table if exists sys_job_log;
create table sys_job_log (
                           job_log_id          int(11) 	    not null auto_increment    comment '任务日志ID',
                           job_name            varchar(64)   not null                   comment '任务名称',
                           job_group           varchar(64)   not null                   comment '任务组名',
                           method_name         varchar(500)                             comment '任务方法',
                           method_params       varchar(50)  default null                comment '方法参数',
                           job_message         varchar(500)                             comment '日志信息',
                           status              char(1)       default '0'                comment '执行状态（0正常 1失败）',
                           exception_info      varchar(2000) default ''                 comment '异常信息',
                           create_time         datetime                                 comment '创建时间',
                           primary key (job_log_id)
) engine=innodb default charset=utf8 comment = '定时任务调度日志表';


-- ----------------------------
-- 18、通知公告表
-- ----------------------------
drop table if exists sys_notice;
create table sys_notice (
                          notice_id 		int(4) 		    not null auto_increment    comment '公告ID',
                          notice_title 		varchar(50) 	not null 				   comment '公告标题',
                          notice_type 		char(2) 	    not null 			       comment '公告类型（1通知 2公告）',
                          notice_content    varchar(500)    not null                   comment '公告内容',
                          status 			char(1) 		default '0' 			   comment '公告状态（0正常 1关闭）',
                          create_by         varchar(64)     default ''                 comment '创建者',
                          create_time 		datetime                                   comment '创建时间',
                          update_by 		varchar(64) 	default ''			       comment '更新者',
                          update_time 		datetime                                   comment '更新时间',
                          remark 			varchar(255) 	default '' 				   comment '备注',
                          primary key (notice_id)
) engine=innodb auto_increment=10 default charset=utf8 comment = '通知公告表';

-- ----------------------------
-- 初始化-公告信息表数据
-- ----------------------------
insert into sys_notice values('1', '温馨提醒：2018-07-01 木白新版本发布啦', '2', '新版本内容', '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '管理员');
insert into sys_notice values('2', '维护通知：2018-07-01 木白系统凌晨维护', '1', '维护内容',   '0', 'admin', '2018-12-01 11-33-00', 'mubai', '2018-12-01 11-33-00', '管理员');


-- ----------------------------
-- 19.文件存储表
-- ----------------------------
drop table if exists sys_files;
create table sys_files  (
                      file_id                 int(11) 	    not null auto_increment    comment '文件存储ID',
                      file_name               varchar(255)  not null default '文件名称',
                      url                     varchar(255) default ''  comment 'url路径',
                      content                 varchar(5000)  default '' comment '文字内容',
                      suffix                  varchar(20)  default '' comment '后缀',
                      type                    varchar(10)  not null default '0' comment '类型（0代表图片 1代表视频）',
                      del_flag                char(1)  default '0' comment '删除标志（0代表存在 2代表删除）',
                      create_by_name          varchar(50)  default '' COMMENT '创建人姓名',
                      create_by               varchar(64)     default ''                 comment '创建者',
                      create_time 		        datetime                                   comment '创建时间',
                      update_by_name          varchar(50)  default '' COMMENT '修改人名称',
                      update_by 		          varchar(64) 	default ''			       comment '更新者',
                      update_time 		        datetime                                   comment '更新时间',
                      remark 			            varchar(255) 	default '' 				   comment '备注',
                      primary key (file_id),
                      index unique_key(file_name) comment '文件名称唯一'
) engine=innodb auto_increment=100 default charset=utf8 comment = '文件上传表';

-- ----------------------------
-- 初始化-文件存储表数据
-- ----------------------------
insert into sys_files values('10', '我是文字', '', '<h1>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 标题</h1><div>&nbsp; &nbsp; &nbsp; 我是内容<span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容。</span></div>', '', '2', '0', '管理员', 'admin', '2018-12-01 11-33-00', '木白', 'mubai', '2018-12-01 11-33-00', '管理员');
insert into sys_files values('11', '木白文件上传测试', 'd:/profile/木白文件上传测试.mp4', '', 'mp4', '1', '0', '管理员', 'admin', '2018-12-01 11-33-00', '木白', 'mubai', '2018-12-01 11-33-00', '管理员');
insert into sys_files values('12', '木白图片上传','d:/profile/木白图片上传.jpg',  '', 'jpg', '0', '0', '管理员', 'admin', '2018-12-01 11-33-00', '木白', 'mubai', '2018-12-01 11-33-00', '管理员');


-- ----------------------------
-- 20.文件通道表
-- ----------------------------
drop table if exists sys_file_channel;
create table sys_file_channel  (
                  channel_id int(11) 	    not null auto_increment    comment '文件通道ID',
                  channel_name varchar(50) default '' comment '通道名称',
                  channel_code varchar(20) default '' comment '通道编码',
                  show_file varchar(500) default '' comment '显示内容',
                  primary key (channel_id),
                  unique index unique_key(channel_code) comment '编码唯一'
) engine=innodb auto_increment=100 default charset=utf8 comment = '文件通道表';

-- ----------------------------
-- 初始化-文件通道表数据
-- ----------------------------
insert into sys_file_channel values ('1', '一号通道', '10', '');
insert into sys_file_channel values ('2', '二号通道', '20', '');
insert into sys_file_channel values ('3', '三号通道', '30', '');
insert into sys_file_channel values ('4', '四号通道', '40', '');

-- ----------------------------
-- 播放顺序表
-- ----------------------------
drop table if exists sys_file_play;
create table sys_file_play  (
      play_id       int(11) 	    not null auto_increment    comment '文件播放ID',
      channel_code    int(11)       not null default 0 comment '通道编码',
      file_id       int(11)       default 0 comment '文件id',
      file_name     varchar(50)   default '',
      types         char(1)       default 0 comment '0 图片 1 视频 2 文字',
      suffix        varchar(50)   default '' comment '后缀',
      url           varchar(50)   default '',
      content       varchar(5000) default '',
      order_num      int(11)       default 0 comment '播放顺序',
      remark        varchar(255) default '',
      primary key (play_id)
) engine=innodb auto_increment=100 default charset=utf8 comment = '文件播放顺序表';

-- ----------------------------
-- 初始化-播放顺序表数据
-- ----------------------------
insert into sys_file_play values ('17', '10', '0',  '图片1', '0', 'jpg','d:/profile/图片1.jpg', '', '1', '');
insert into sys_file_play values ('18', '10', '0','视频', '1', 'mp4',  'd:/profile/视频.mp4', '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 我是标题<div>&nbsp; &nbsp; &nbsp;内容<span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span><span style=\"color: inherit;\">内容</span></div>', '2', '');
insert into sys_file_play values ('19', '10', '0', '文件上传测试', '1', 'mp4', 'd:/profile/木白文件上传测试.mp4', 'zuidaima.com', '3', '');
insert into sys_file_play values ('20', '40', '0', '我是文字', '2', '',  '','<h1>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 标题</h1><div>&nbsp; &nbsp; &nbsp; 我是内容<span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容</span><span style=\"color: inherit;\">我是内容。</span></div>', '1', '');
insert into sys_file_play values ('21', '40', '0','木白文件上传测试', '1', 'mp4','d:/profile/木白文件上传测试.mp4', 'zuidaima.com', '2', '');
insert into sys_file_play values ('22', '40', '0','木白图片上传', '0', 'jpg', 'd:/profile/木白图片上传.jpg', '', '3', '');


-- ----------------------------
-- 20、oa 在线通知表
-- ----------------------------
drop table if exists oa_notify;
create table oa_notify (
  id bigint(20) not null auto_increment comment '编号',
  type char(1) collate utf8_bin default null comment '类型',
  title varchar(200) collate utf8_bin default null comment '标题',
  content varchar(2000) collate utf8_bin default null comment '内容',
  files varchar(2000) collate utf8_bin default null comment '附件',
  status char(1) character set utf8 default '0' comment '公告状态（0正常 1关闭）',
  create_by varchar(64) character set utf8 default '' comment '创建者',
  create_time datetime default null comment '创建时间',
  update_by varchar(64) character set utf8 default '' comment '更新者',
  update_time datetime default null comment '更新时间',
  remark varchar(255) character set utf8 default '' comment '备注',
  primary key (id)
) engine=innodb auto_increment=100 default charset=utf8 collate=utf8_bin comment='通知通告';

-- ----------------------------
-- records of t_notify
-- ----------------------------
insert into oa_notify values ('42', '3', '苹果发布新手机了', '有全面屏的iphonex', '', '0', 'admin', '2018-07-18 14:28:26', 'admin', '2018-07-18 14:28:26', '');
insert into oa_notify values ('55', '1', '发钱了', '<p>好的</p>', null, '1', 'admin', '2018-12-25 13:34:49', 'admin', '2018-12-25 13:34:49', 'again');
insert into oa_notify values ('72', '3', 'agaga', '<p>来一个测试</p>', null, '1', 'admin', '2018-12-25 16:51:22', 'admin', '2018-12-25 16:51:22', '');

-- ----------------------------
-- 21 oa 通知记录表
-- ----------------------------
drop table if exists oa_notify_record;
create table oa_notify_record (
  id bigint(20) not null auto_increment comment '编号',
  notify_id bigint(20) default null comment '通知通告id',
  user_id bigint(20) default null comment '接受人',
  is_read tinyint(1) default '0' comment '阅读标记',
  read_date datetime default null comment '阅读时间',
  primary key (id),
  key oa_notify_record_notify_id (notify_id),
  key oa_notify_record_user_id (user_id),
  key oa_notify_record_read_flag (is_read)
) engine=innodb auto_increment=100 default charset=utf8 collate=utf8_bin comment='通知通告发送记录';

-- ----------------------------
-- records of t_notify_record
-- ----------------------------
insert into oa_notify_record values ('19', '42', '1', '1', '2017-10-26 00:00:00');
insert into oa_notify_record values ('32', '55', '1', '0', '2018-12-25 13:34:49');
insert into oa_notify_record values ('33', '55', '2', '0', '2018-12-25 13:34:49');
insert into oa_notify_record values ('34', '55', '3', '0', '2018-12-25 13:34:49');
insert into oa_notify_record values ('81', '72', '1', '0', '2018-12-25 16:51:22');
insert into oa_notify_record values ('82', '72', '2', '0', '2018-12-25 16:51:22');
insert into oa_notify_record values ('83', '72', '3', '0', '2018-12-25 16:51:22');


-- ----------------------------
-- 22 我的日程表 oa_day_manage
-- ----------------------------
drop table if exists oa_day_manage;
create table oa_day_manage (
  day_id bigint(20) not null auto_increment comment '日程id',
  day_title varchar(255) default null comment '日程标题',
  start_time datetime default null comment '开始时间',
  end_time datetime default null comment '结束时间',
  day_type char(2) character set utf8 default null comment '日程类型（1日程安排 2假日提醒）',
  status char(1) character set utf8 default '0' comment '日程状态（0一般 1重要 2紧急）',
  remind char(1) character set utf8 default null comment '是否提醒(0否1是)',
  create_by varchar(64) character set utf8 default '' comment '创建者',
  create_time datetime default null comment '创建时间',
  update_by varchar(64) character set utf8 default '' comment '更新者',
  update_time datetime default null comment '更新时间',
  remark varchar(255) character set utf8 default '' comment '备注',
  primary key (day_id)
) engine=innodb auto_increment=18 default charset=utf8mb4 comment='日程信息表';

-- ----------------------------
-- records of oa_day_manage
-- ----------------------------
insert into oa_day_manage values ('17', '紧急', '2019-01-06 16:17:11', '2019-01-07 00:00:00', '2', '3', 'y', '', '2019-01-06 16:18:08', '', null, 'aga');

-- ----------------------------
-- 23 我的日程记录表 oa_day_record
-- ----------------------------
drop table if exists oa_day_record;
create table oa_day_record (
  day_id bigint(20) not null comment '日程id',
  user_id bigint(20) not null comment '用户id',
  primary key (day_id,user_id),
  key oa_day_record_user_id (user_id) using btree,
  key oa_day_record_day_id (day_id) using btree
) engine=innodb default charset=utf8 collate=utf8_bin comment='日程信息相关人员记录';

-- ----------------------------
-- records of oa_day_record
-- ----------------------------
insert into oa_day_record values ('17', '1');
insert into oa_day_record values ('17', '2');

-- ----------------------------
-- 24 资产 物品信息表
-- ----------------------------
drop table if exists ca_asset;
create table ca_asset (
  asset_id int(11) not null auto_increment comment '物品id',
  asset_no varchar(50) default null comment '物品编号',
  asset_name varchar(200) default null comment '物品名称',
  shelf_no varchar(50) default null comment '货架编号',
  location varchar(200) default null comment '货架位置 （组成结构：层号/列号）',
  rfid varchar(50) default null comment 'rfid',
  size varchar(100) default null comment '物品大小',
  weight varchar(100) default null comment '物品重量',
  imgid text comment '图片id多个图片以,分隔',
  type varchar(3) default null comment '物品类型 10书画 20石器 30铜器',
  status tinyint(1) default null comment '状态  0入库 1出库 2报废',
  create_by varchar(64) default '' comment '创建者',
  create_time datetime default null comment '创建时间',
  update_by varchar(64) default '' comment '更新者',
  update_time datetime default null comment '更新时间',
  remark varchar(255) default '' comment '备注',
  primary key (asset_id)
) engine=innodb default charset=utf8 comment='资产信息表';

-- ----------------------------
-- 25 资产 读写器信息表
-- ----------------------------
drop table if exists ca_reader;
create table ca_reader (
  reader_id varchar(50) not null comment '读写器id',
  reader_no varchar(255) default null comment '读写器no',
  type varchar(20) default null comment '读写器类型,4通道，8通道',
  ip varchar(50) default null comment '读写器ip',
  port int(10) default null comment '端口',
  power varchar(50) default null comment '发射功率',
  wire varchar(10) default null comment '读写器天线',
  status varchar(1) default null comment '连接状态 0断开，1连接',
  create_by varchar(64) default '' comment '创建者',
  create_time datetime default null comment '创建时间',
  update_by varchar(64) default '' comment '更新者',
  update_time datetime default null comment '更新时间',
  remark varchar(255) default '' comment '备注',
  primary key (reader_id)
) engine=innodb default charset=utf8 comment='读写器信息表';

-- ----------------------------
-- 26 资产 货架信息表
-- ----------------------------
drop table if exists ca_shelf;
create table ca_shelf (
  shelf_id int(11) not null auto_increment comment '货架id',
  shelf_name varchar(100) character set utf8 default null comment '货架名称',
  shelf_no varchar(50) character set utf8 default null comment '货架编号',
  zone_id int(11) default null comment '库区id',
  rfid varchar(50) character set utf8 default null comment 'rfid编号',
  layer int(2) default null comment '货架的层数',
  status char(1) character set utf8 default '0' comment '货架状态（0 在用 1空闲 2报废 3停用）',
  create_by varchar(64) character set utf8 default '' comment '创建者',
  create_time datetime default null comment '创建时间',
  update_by varchar(64) character set utf8 default '' comment '更新者',
  update_time datetime default null comment '更新时间',
  remark varchar(255) character set utf8 default '' comment '备注',
  primary key (shelf_id)
) engine=innodb default charset=utf8mb4 comment='货架信息表';

-- ----------------------------
-- 27 资产 供应商信息表
-- ----------------------------
drop table if exists ca_supplier;
create table ca_supplier (
  supplier_id int(11) not null auto_increment comment '供应商id',
  supplier_name varchar(200) character set utf8 default null comment '供应商名称',
  contact varchar(200) character set utf8 default null comment '联系人',
  tel varchar(15) character set utf8 default null comment '电话号码',
  phone varchar(15) character set utf8 default null comment '手机号码',
  mail varchar(50) character set utf8 default null comment '邮箱',
  address text character set utf8 comment '供应商地址',
  create_by varchar(64) character set utf8 default '' comment '创建者',
  create_time datetime default null comment '创建时间',
  update_by varchar(64) character set utf8 default '' comment '更新者',
  update_time datetime default null comment '更新时间',
  remark varchar(255) character set utf8 default '' comment '备注',
  primary key (supplier_id)
) engine=innodb default charset=utf8mb4 comment='供应商信息';

-- ----------------------------
-- 28 资产 货区信息表
-- ----------------------------
drop table if exists ca_zone;
create table ca_zone (
  zone_id int(11) not null auto_increment comment '库区id',
  zone_name varchar(200) default null comment '库区名称',
  order_num int(4) default '0' comment '显示顺序',
  create_by varchar(64) default '' comment '创建者',
  create_time datetime default null comment '创建时间',
  update_by varchar(64) default '' comment '更新者',
  update_time datetime default null comment '更新时间',
  remark varchar(255) default '' comment '备注',
  primary key (zone_id)
) engine=innodb default charset=utf8 comment='货区信息表';

