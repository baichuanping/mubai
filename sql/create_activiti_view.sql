-- 创建组与用户关联视图
DROP table IF EXISTS act_id_membership;
create or replace view act_id_membership
(user_id_, group_id_)
as
select user_id as user_id_,role_id as group_id_
from sys_user_role ;


-- 创建组视图
DROP table IF EXISTS act_id_group;
create or replace view act_id_group
(id_, rev_, name_, type_)
as
select t.role_id as id_ , 1 as rev_,t.role_name ,t.role_key as type_
from sys_role t;

-- 创建用户视图
DROP table IF EXISTS act_id_user;
CREATE OR REPLACE VIEW act_id_user
(id_, rev_, first_, last_, email_, pwd_, picture_id_)
AS
SELECT user_id as id_,1 as rev_ ,login_name as first_,user_name as last_,email as email_,`password` as pwd_ ,null as picture_id_
FROM sys_user;

-- 创建用户信息视图
-- DROP table IF EXISTS `act_id_info`;
-- CREATE OR REPLACE VIEW act_id_user
-- (ID_, REV_, USER_ID_, TYPE_,KEY_,VALUE_, PASSWORD_,PARENT_ID_)
-- AS
-- SELECT user_id as id_,1 as rev_ ,login_name as first_,user_name as last_,email as email_,`password` as pwd_ ,null as picture_id_
-- FROM sys_user;

-- ----------------------------
-- 22、薪水调整表
-- ----------------------------
drop table if exists oa_salary;
create table oa_salary (
  salary_id       int(11) 	    not null auto_increment    comment '薪水id',
  process_id    varchar(100)   default ''                 comment '流程编号',
  user_id         varchar(64) default ''               comment '变动用户',
  user_name         varchar(100) default ''               comment '变动用户名',
  dept_id           varchar(255) default ''             comment '归属部门',
  post              varchar(255) 	default '' 				   comment '岗位',
  sex  		          char(1) 	    default '0' 			   comment '用户性别（0男 1女 2未知）',
  edu               varchar(255) 	default '' 				   comment '学历',
  content           varchar(255) 	default '' 				   comment '调整原因',
  olda              varchar(255) 	default '' 				   comment '现行标准 薪酬档级',
  oldb              varchar(255) 	default '' 				   comment '现行标准 月工资额',
  oldc              varchar(255) 	default '' 				   comment '现行标准 年薪总额',
  newa              varchar(255) 	default '' 				   comment '调整后标准 薪酬档级',
  newb              varchar(255) 	default '' 				   comment '调整后标准 月工资额',
  newc              varchar(255) 	default '' 				   comment '调整后标准 年薪总额',
  add_num           varchar(255) 	default '' 				   comment '月增资',
  exe_date          datetime                           comment '执行时间',
  hr_text           varchar(255) 	default '' 				   comment '人力资源部门意见',
  lead_text         varchar(255) 	default '' 				   comment '分管领导意见',
  main_lead_text    varchar(255) 	default '' 				   comment '集团主要领导意见',
  create_by         varchar(64)    default ''          comment '创建者',
  create_time 		  datetime                           comment '创建时间',
  update_by 		    varchar(64) 	default ''			     comment '更新者',
  update_time 		  datetime                           comment '更新时间',
  remark 			      varchar(255) 	default '' 				   comment '备注',
  primary key (salary_id)
) engine=innodb auto_increment=100 default charset=utf8 comment='薪水调整表';

-- ----------------------------
-- 23、商户信息表 （工作流测试表）
-- ----------------------------
drop table if exists oa_merchant;
create table oa_merchant (
  merchant_id        int(11) 	    not null auto_increment    comment '商户ID',
  process_id 		 varchar(100)   default ''                 comment '流程编号',
  merchant_name      varchar(30) 	not null                   comment '商户名称',
  identity_no        varchar(20) 	default ''                 comment '身份证号码',
  phone              varchar(11)    default ''                 comment '联系电话',
  audit_state 		 char(1) 		default '0'                comment '审核状态（0初始 1审核中 3审核通过 4未通过）',
  audit_desc         varchar(255) 	default ''                 comment '审核结果描述',
  create_by          varchar(64)    default ''                 comment '创建者',
  create_time 		 datetime                                  comment '创建时间',
  update_by 		 varchar(64) 	default ''			       comment '更新者',
  update_time 		 datetime                                  comment '更新时间',
  remark 			 varchar(255) 	default '' 				   comment '备注',
  primary key (merchant_id)
) engine=innodb auto_increment=100 default charset=utf8 comment = '商户信息表';