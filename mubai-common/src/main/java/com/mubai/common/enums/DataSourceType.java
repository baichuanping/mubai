package com.mubai.common.enums;

/**
 * 数据源
 * 
 * @author baichuanping
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
