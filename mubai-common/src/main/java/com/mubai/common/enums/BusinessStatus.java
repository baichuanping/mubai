package com.mubai.common.enums;

/**
 * 操作状态
 * 
 * @author baichuanping
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
