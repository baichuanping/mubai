package com.mubai.common.enums;

/**
 * HTTP协议返回值封装
 * @author baichuanping
 * @create 2019-01-10
 */
public enum ReturnField {
	success,code, note
}
