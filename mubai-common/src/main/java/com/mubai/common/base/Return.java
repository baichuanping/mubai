package com.mubai.common.base;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mubai.common.enums.CODE;
import com.mubai.common.enums.ReturnField;
import com.mubai.common.json.JSONObject;
import com.mubai.common.tool.ToolJackson;
import com.mubai.common.tool.ToolThrowable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作消息提醒
 *
 * @author baichuanping
 */
public class Return extends JSONObject {
	private static final long serialVersionUID = 1L;
	public static final Logger log = LoggerFactory.getLogger(Return.class);

	//////////////////////////////// create//////////////////////////////////
	public static Return create() {
		return new Return();
	}

	public static Return create(Integer code, String data) {
		Return jo = new Return();
		jo.put(ReturnField.code.name(), code);
		jo.put(ReturnField.note.name(), data);
		return jo;
	}

	public static Return create(CODE code) {
		return create(code.code, code.note);
	}

	public static Return create(String key, Object value) {
		return new Return().add(key, value);
	}

	public static Return create(String json) {
		Return jo = new Return();
		try {
			Map<String, Object> fromJson = ToolJackson.toObject(json, new TypeReference<HashMap<String, Object>>() {
			});
			for (Entry<String, Object> entry : fromJson.entrySet()) {
				jo.put(entry.getKey(), entry.getValue());
			}
		} catch (IOException e) {
			log.error("Return.create 解析 JSON 失败");
			return Return.create(CODE.generate_return_error);
		}
		return jo;
	}

	public static Return SUCCESS(CODE code) {
		return SUCCESS(code.code, code.note);
	}

	public static Return SUCCESS(CODE code, Exception e) {
		return SUCCESS(code.code, ToolThrowable.stacktrace(e));
	}

	public static Return SUCCESS(String code, String note) {
		final Return jo = new Return();
		jo.put(ReturnField.success.name(), true);
		jo.put(ReturnField.note.name(), note);
		jo.put(ReturnField.code.name(), code);
		return jo;
	}

	public static Return SUCCESS(Integer code, String note) {
		final Return jo = new Return();
		jo.put(ReturnField.success.name(), true);
		jo.put(ReturnField.note.name(), note);
		jo.put(ReturnField.code.name(), code);
		return jo;
	}

	/**
	 * 返回失败
	 *
	 * @param code
	 * @return
	 */
	public static Return FAIL(CODE code) {
		return FAIL(code.code, code.note);
	}

	public static Return FAIL(CODE code, Exception e) {
		return FAIL(code.code, ToolThrowable.stacktrace(e));
	}

	public static Return FAIL(String code, String note) {
		final Return jo = new Return();
		jo.put(ReturnField.success.name(), false);
		jo.put(ReturnField.code.name(), code);
		jo.put(ReturnField.note.name(), note);
		return jo;
	}

	public static Return FAIL(Integer code, String note) {
		final Return jo = new Return();
		jo.put(ReturnField.success.name(), false);
		jo.put(ReturnField.code.name(), code);
		jo.put(ReturnField.note.name(), note);
		return jo;
	}
//////////////////////////////////// GETTER SETTER///////////////////////////

	public Integer getCode() {
		return (Integer) this.getOrDefault(ReturnField.code.name(), CODE.error.code);
	}

	public String getNote() {
		return (String) this.getOrDefault(ReturnField.note.name(), "");
	}

	//////////////////////// @Override/////////////////////////////////////
	@Override
	public Return put(String key, Object value) {
		super.put(key, value);
		return this;
	}

	public Return add(String key, Object value) {
		super.put(key, value);
		return this;
	}

	public String toJson() {
		try {
			return ToolJackson.toJson(this);
		} catch (Exception e) {
			log.error("json 解析失败:");
			return ToolJackson.toJson(FAIL(CODE.generate_return_error));
		}
	}
}
