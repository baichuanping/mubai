package com.mubai.common.config;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import com.mubai.common.utils.StringUtils;
import com.mubai.common.utils.YamlUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 全局配置类
 * 
 * @author baichuanping
 */
public class Global
{
    private static final Logger log = LoggerFactory.getLogger(Global.class);

    private static String NAME = "application.yml";

    /**
     * 当前对象实例
     */
    private static Global global = null;

    /**
     * 保存全局属性值
     */
    private static Map<String, String> map = new HashMap<String, String>();

    private Global()
    {
    }

    /**
     * 静态工厂方法 获取当前对象实例 多线程安全单例模式(使用双重同步锁)
     */

    public static synchronized Global getInstance()
    {
        if (global == null)
        {
            synchronized (Global.class)
            {
                if (global == null)
                    global = new Global();
            }
        }
        return global;
    }

    /**
     * 获取配置
     */
    public static String getConfig(String key)
    {
        String value = map.get(key);
        if (value == null)
        {
            Map<?, ?> yamlMap = null;
            try
            {
                yamlMap = YamlUtil.loadYaml(NAME);
                value = String.valueOf(YamlUtil.getProperty(yamlMap, key));
                map.put(key, value != null ? value : StringUtils.EMPTY);
            }
            catch (FileNotFoundException e)
            {
                log.error("获取全局配置异常 {}", key);
            }
        }
        return value;
    }

    /**
     * 设置配置
     * @param key
     * @param value
     * @return
     */
    public static void setConfig(String key,String value)
    {
        if (value != null) {
            Map<?, ?> yamlMap = null;
            try
            {
                yamlMap = YamlUtil.loadYaml(NAME);
                YamlUtil.setProperty(yamlMap, key,value);
                map.put(key, value != null ? value : StringUtils.EMPTY);
            }
            catch (FileNotFoundException e)
            {
                log.error("设置全局配置异常 {}", key);
            }
        }
    }

    /**
     * 获取项目名称
     */
    public static String getName()
    {
        return StringUtils.nvl(getConfig("mubai.name"), "mubai");
    }

    /**
     * 获取项目版本
     */
    public static String getVersion()
    {
        return StringUtils.nvl(getConfig("mubai.version"), "3.0.0");
    }

    /**
     * 获取版权年份
     */
    public static String getCopyrightYear()
    {
        return StringUtils.nvl(getConfig("mubai.copyrightYear"), "2018");
    }

    /**
     * 获取ip地址开关
     */
    public static Boolean isAddressEnabled()
    {
        return Boolean.valueOf(getConfig("mubai.addressEnabled"));
    }

    /**
     * 获取文件上传路径
     */
    public static String getProfile()
    {
        return getConfig("mubai.profile");
    }

    /**
     * 获取头像上传路径
     */
    public static String getAvatarPath()
    {
        return getConfig("mubai.profile") + "avatar/";
    }

    /**
     * 获取下载上传路径
     */
    public static String getDownloadPath()
    {
        return getConfig("mubai.profile") + "download/";
    }
    /**
     * 获取上传路径
     */
    public static String getUploadPath()
    {
        return getConfig("mubai.profile") + "upload/";
    }

    /**
     * 获取作者
     */
    public static String getAuthor()
    {
        return StringUtils.nvl(getConfig("gen.author"), "mubai");
    }

    /**
     * 获取Email
     * @return
     */
    public static String getEmail()
    {
        return StringUtils.nvl(getConfig("gen.email"), "276058171@qq.com");
    }

    /**
     * 生成包路径
     */
    public static String getPackageName()
    {
        return StringUtils.nvl(getConfig("gen.packageName"), "com.mubai.project.module");
    }

    /**
     * 是否自动去除表前缀
     */
    public static String getAutoRemovePre()
    {
        return StringUtils.nvl(getConfig("gen.autoRemovePre"), "true");
    }

    /**
     * 表前缀(类名不会包含表前缀)
     */
    public static String getTablePrefix()
    {
        return StringUtils.nvl(getConfig("gen.tablePrefix"), "sys_");
    }

    /**
     * 获取Jwt超时时间
     * @return
     */
    public static int getJwtOutTime() {
        return Integer.valueOf(getConfig("mubai.jwt.outTime"));
    }

    /**
     * 获取是否开启redis
     * @return
     */
    public static boolean hasRedis() {
        return Boolean.valueOf(getConfig("spring.redis.enable"));
    }
}
