package com.mubai.common.exception;

/**
 * 业务异常
 * @author baichuanping
 * @create 2018-12-18
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	protected final String message;

	public BusinessException(String message)
	{
		this.message = message;
	}
	/**
	 * Returns the detail message string of this throwable.
	 *
	 * @return the detail message string of this {@code Throwable} instance
	 * (which may be {@code null}).
	 */
	@Override
	public String getMessage() {
		return message;
	}
}
