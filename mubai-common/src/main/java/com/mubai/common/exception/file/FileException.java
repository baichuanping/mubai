package com.mubai.common.exception.file;

import com.mubai.common.exception.base.BaseException;

/**
 * 文件信息异常类
 * @author baichuanping
 * @create 2019-02-15
 */
public class FileException extends BaseException
{
	private static final long serialVersionUID = 1L;

	public FileException(String code, Object[] args)
	{
		super("file", code, args, null);
	}

}
