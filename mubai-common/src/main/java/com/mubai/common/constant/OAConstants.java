package com.mubai.common.constant;

/**
 * @author baichuanping
 * @create 2018-12-18
 */
public class OAConstants {
	//通知公告阅读状态-未读
	public static int NOTIFY_READ_NO = 0;
	//通知公告阅读状态-已读
	public static int NOTIFY_READ_YES = 1;
}
