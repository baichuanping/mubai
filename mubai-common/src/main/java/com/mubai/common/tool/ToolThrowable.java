package com.mubai.common.tool;

/**
 * 异常处理工具类
 * @author baichuanping
 * @create 2019-01-10
 */
public class ToolThrowable {
	/**
	 * 拼接打印exception 栈内容
	 * @param e 异常类
	 * @return
	 */
	public static String stacktrace(Throwable e) {
		StringBuilder stackTrace = new StringBuilder();
		while (e !=null){
			String message = e.getMessage();
			message = message == null ?"\r\n":message.concat("\r\n");
			stackTrace.append(message);
			stackTrace.append("<br>");
			for (StackTraceElement string : e.getStackTrace()) {
				stackTrace.append(string.toString());
				stackTrace.append("<br>");
			}
			e = e.getCause();
		}
		return stackTrace.toString();
	}
}
